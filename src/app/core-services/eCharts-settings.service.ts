import {Injectable, OnInit} from '@angular/core';
import {EChartsOption} from 'echarts';


@Injectable()
export class EChartsSettingsService {
    public donutChartOptions: EChartsOption = {};
    public pieChartOptions: EChartsOption = {};
    public pieWithinDonutChartOptions: EChartsOption = {};
    donutChartSetting(): EChartsOption {
        return this.donutChartOptions = {
            tooltip: {
                trigger: 'item',
                showDelay: 0,
                hideDelay: 50,
                transitionDuration: 0,
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: '#aaa',
                showContent: true,
                borderWidth: 1,
                padding: 10,
                position: [30, 50],
            },

            grid: {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
            },
            color: [
                '#68CCE3', '#33406C', '#FFBA53', '#37CE6A', '#054421'
            ],
            legend: {
                data: [],
            },
            series: [
                {
                    type: 'pie',
                    radius: ['50%', '70%'],
                    data: [],
                    label: {
                        position: 'outer',
                        alignTo: 'labelLine',
                        bleedMargin: 5
                    },
                    labelLine: {
                        smooth: 0.5,
                        length: 5,
                        length2: 5
                    }
                }
            ],
        };

    }
    pieChartSetting(): EChartsOption {
        return this.pieChartOptions = {
            tooltip: {
                trigger: 'item',
                showDelay: 0,
                hideDelay: 50,
                transitionDuration: 0,
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: '#aaa',
                showContent: true,
                borderWidth: 1,
                padding: 10,
            },
            grid: {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
            },
            color: [
                '#68CCE3', '#33406C', '#FFBA53', '#37CE6A', '#054421'
            ],
            legend: {
                left: 'left',
                data: [],
            },
            series: [
                {
                    type: 'pie',
                    radius: '55%',
                    center: ['57%', '53%'],
                    data: [],
                    label: {
                        position: 'outer',
                        alignTo: 'labelLine',
                        bleedMargin: 5
                    },
                    labelLine: {
                        smooth: 0.2,
                        length: 3,
                        length2: 3
                    },
                }
            ],
        };

    }
    pieWithinDonutChart(): EChartsOption {
        return this.pieWithinDonutChartOptions = {
            tooltip: {
                trigger: 'item',
                showDelay: 0,
                hideDelay: 50,
                transitionDuration: 0,
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: '#aaa',
                showContent: true,
                borderWidth: 1,
                padding: 10
            },
            legend: {
                orient: 'vertical',
                left: '75%',
                data: ['Hardware', 'Software', 'UnSpecified']
            },
            series: [
                {
                    name: 'Asset Types',
                    type: 'pie',
                    selectedMode: false,
                    radius: [],
                    label: {
                        position: 'inner'
                    },
                    labelLine: {
                        show: false,
                        smooth: 0.2,
                        length: 3,
                        length2: 3
                    },
                    data: [],
                },
                {
                    name: 'Asset Categories',
                    type: 'pie',
                    label: {
                        formatter: '{b|{b}：}{c}  {per|{d}%}  ',
                        padding: [0, 6],
                        backgroundColor: '#eee',
                        borderColor: '#aaa',
                        borderWidth: 1,
                        borderRadius: 4,
                        rich: {
                            a: {
                                color: '#000',
                                lineHeight: 30,
                                align: 'center'
                            },
                            hr: {
                                borderColor: '#aaa',
                                width: '100%',
                                borderWidth: 0.5,
                                height: 2
                            },
                            b: {
                                fontSize: 11,
                                lineHeight: 40
                            },
                            per: {
                                color: '#fff',
                                backgroundColor: '#000',
                                padding: [2, 4],
                                borderRadius: 2
                            }
                        }
                    },
                    labelLine: {
                        length: 10,
                    },
                    radius: ['55%', '70%'],
                    data: []
                }
            ]
        };

    }
}
