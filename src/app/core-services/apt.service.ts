import {Injectable, OnInit} from '@angular/core';
import {ConfigApiService} from './configApi.service';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class AptService {
    baseUrl: any;
    private storageDSub = new Subject<boolean>();
    private storageGSub = new Subject<boolean>();
    private storageASub = new Subject<boolean>();

    constructor(private http: HttpClient, private configApi: ConfigApiService) {
    }
    watchDStorage(): Observable<any> {
        return this.storageDSub.asObservable();
    }

    saveDitem(data): any {
        this.storageDSub.next(true);
    }
    watchGStorage(): Observable<any> {
        return this.storageGSub.asObservable();
    }

    saveGitem(data): any {
        this.storageGSub.next(true);
    }
    watchAStorage(): Observable<any> {
        return this.storageASub.asObservable();
    }

    saveAitem(data): any {
        this.storageASub.next(true);
    }
    uploadIoCFile(body): any {
        return this.http.post(this.configApi.uploadIoCFile(), body);
    }
    uploadGroupCV(id, body): any {
        return this.http.post(this.configApi.uploadGroupCV(id), body);
    }
    getSectors(): any {
        return this.http.get(this.configApi.getSectors());
    }
    downloadaptIOCs(id): any {
        const options = {responseType: 'blob' as 'json'};
        return this.http.get<Blob>(this.configApi.downloadaptIOCs(id), options);
    }
    downloadaptGroupProfile(body): any {
        const options = {responseType: 'blob' as 'json'};
        return this.http.post<Blob>(this.configApi.downloadaptGroupProfile(), body, options);
    }
    downloadaptCampaignProfile(body): any {
        const options = {responseType: 'blob' as 'json'};
        return this.http.post<Blob>(this.configApi.downloadaptCampaignProfile(), body, options);
    }
    getpreferences(): any {
        return this.http.get(this.configApi.preferences());
    }
    postpreferences(body): any {
        return this.http.post(this.configApi.preferences(), body);
    }
    putpreferences(body): any {
        return this.http.put(this.configApi.preferences(), body);
    }
    gettopGroups(): any {
        return this.http.get(this.configApi.gettopGroups());
    }
    gettopAttacks(): any {
        return this.http.get(this.configApi.gettopAttacks());
    }
    getSectorChart(): any {
        return this.http.get(this.configApi.getSectorChart());
    }
    getRegionChart(): any {
        return this.http.get(this.configApi.getRegionChart());
    }
    getCountriesChart(region, from, to): any {
        return this.http.get(this.configApi.getCountriesChart(region, from, to));
    }

    getRegions(): any {
        return this.http.get(this.configApi.getRegions());
    }

    getTechniques(): any {
        return this.http.get(this.configApi.getTechniques());
    }
    getTechniquesById(id): any {
        return this.http.get(this.configApi.getTechniquesById(id));
    }

    getCaseNames(): any {
        return this.http.get(this.configApi.getCaseNames());
    }

    getGroupNames(): any {
        return this.http.get(this.configApi.getGroupNames());
    }

    postGroups(data): any {
        return this.http.post(this.configApi.postGroups(), data);
    }

    postAttacksCampaign(data): any {
        return this.http.post(this.configApi.postAttackCampaign(), data);
    }

    updateAttacksCampaign(data): any {
        return this.http.put(this.configApi.postAttackCampaign(), data);
    }

    updateGroups(data): any {
        return this.http.put(this.configApi.postGroups(), data);
    }

    getGroups(id): any {
        return this.http.get(this.configApi.getGroups(id));
    }

    getGroupsList(body, defaultPreference): any {
        return this.http.post(this.configApi.getGroupsList(defaultPreference), body);
    }

    getAttackCampaignList(body, defaultPreference): any {
        return this.http.post(this.configApi.getAttackCampaignList(defaultPreference), body);
    }

    deleteGroups(id): any {
        return this.http.delete(this.configApi.deleteGroups(id));
    }

    deleteAttackCampaign(id): any {
        return this.http.delete(this.configApi.deleteAttackCampaign(id));
    }

    getAttackCampaign(id): any {
        return this.http.get(this.configApi.getAttackCampaign(id));
    }
    // dashboard
    getTotalGAI(): any {
        return this.http.get(this.configApi.getTotalGAI());
    }
    suggestionACListing(): any {
        return this.http.get(this.configApi.suggestionACListing());
    }
    suggestionGListing(): any {
        return this.http.get(this.configApi.suggestionGListing());
    }
    getTargetRegionAttack(from, to): any {
        return this.http.get(this.configApi.getTargetRegionAttack(from, to));
    }
    getTargetSectorCountriesByIOCR(region, from, to): any {
        return this.http.get(this.configApi.getTargetSectorCountriesByIOCR(region, from, to));
    }
    getTargetRegionAttackByIOC(from, to): any {
        return this.http.get(this.configApi.getTargetRegionAttackByIOC(from, to));
    }
    getTargetSectorAttack(from, to): any {
        return this.http.get(this.configApi.getTargetSectorAttack(from, to));
    }
    getTargetSectorAttackByIOC(from, to): any {
        return this.http.get(this.configApi.getTargetSectorAttackByIOC(from, to));
    }
    getTargetCountriesAttack(from, to): any {
        return this.http.get(this.configApi.getTargetCountriesAttack(from, to));
    }
    getTargetSectorCountriesByIOC(from, to): any {
        return this.http.get(this.configApi.getTargetSectorCountriesByIOC(from, to));
    }
}
