import {Injectable, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
@Injectable()
export class ConfigApiService {
    baseUrl: string;
    stixUrlFeeds: string;

    constructor() {
        this.baseUrl = environment.API_URL;
        this.stixUrlFeeds = environment.STIX_Url_Feeds;
    }

    /*-----------------------Adding to APT Services------------------------------------------*/
    getRegions(): string {
        const ip = this.baseUrl + 'apt/lookup/countries';
        return ip;
    }

    getCaseNames(): string {
        const ip = this.baseUrl + 'apt/lookup/casenames';
        return ip;
    }

    getGroupNames(): string {
        const ip = this.baseUrl + 'apt/lookup/group-names';
        return ip;
    }

    getTechniques(): string {
        const ip = this.baseUrl + 'apt/lookup/techniques';
        return ip;
    }

    getTechniquesById(id): string {
        const ip = this.baseUrl + 'apt/lookup/techniques?techniqueTypeId=' + id;
        return ip;
    }

    uploadIoCFile(): string {
        const ip = this.baseUrl + 'apt/attack-detail-file-upload';
        return ip;
    }

    uploadGroupCV(id): string {
        const ip = this.baseUrl + 'apt/technique-detail-file-upload?id=' + id;
        return ip;
    }

    getSectors(): string {
        const ip = this.baseUrl + 'apt/lookup/sectors';
        return ip;
    }

    downloadaptIOCs(id): string {
        const ip = this.baseUrl + 'apt/attack-campaign/download-indicators?id=' + id;
        return ip;
    }

    downloadaptGroupProfile(): string {
        const ip = this.baseUrl + 'reports/apt-group-report';
        return ip;
    }

    downloadaptCampaignProfile(): string {
        const ip = this.baseUrl + 'reports/apt-attack-campaign-report';
        return ip;
    }

    preferences(): string {
        const ip = this.baseUrl + 'apt/preferences';
        return ip;
    }

    gettopGroups(): string {
        const ip = this.baseUrl + 'apt/top-groups';
        return ip;
    }

    gettopAttacks(): string {
        const ip = this.baseUrl + 'apt/top-attack-campaigns';
        return ip;
    }

    getSectorChart(): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-sectors';
        return ip;
    }

    getCountriesChart(region, from, to): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-countries?region=' + region + '&from=' + from + '&to='
            + to;
        return ip;
    }

    getRegionChart(): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-regions';
        return ip;
    }

    getTotalGAI(): string {
        const ip = this.baseUrl + 'apt/total-counts';
        return ip;
    }

    getTargetRegionAttack(from, to): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-regions?from=' + from + '&to=' + to;
        return ip;
    }

    getTargetSectorCountriesByIOCR(region, from, to): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-countries-by-ioc?region=' + region + '&from=' + from + '&to=' + to;
        return ip;
    }

    getTargetRegionAttackByIOC(from, to): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-regions-by-ioc?from=' + from + '&to=' + to;
        return ip;
    }

    getTargetSectorAttack(from, to): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-sectors?from=' + from + '&to=' + to;
        return ip;
    }

    getTargetSectorAttackByIOC(from, to): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-sectors-by-ioc?from=' + from + '&to=' + to;
        return ip;
    }

    getTargetCountriesAttack(from, to): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-countries?from=' + from + '&to=' + to;
        return ip;
    }

    getTargetSectorCountriesByIOC(from, to): string {
        const ip = this.baseUrl + 'apt/attack-campaign/target-countries-by-ioc?from=' + from + '&to=' + to;
        return ip;
    }

    postGroups(): string {
        const ip = this.baseUrl + 'apt/group';
        return ip;
    }

    postAttackCampaign(): string {
        const ip = this.baseUrl + 'apt/attack-campaign';
        return ip;
    }

    getGroups(id): string {
        const ip = this.baseUrl + 'apt/group?id=' + id;
        return ip;
    }

    getGroupsList(defaultPreference): string {
        const role = sessionStorage.getItem('role');
        if (role === 'ROLE_SOC_ADMIN' || role === 'ROLE_L1_ANALYST' || role === 'ROLE_L2_ANALYST') {
            const ip = this.baseUrl + 'apt/group/group-list';
            return ip;

        } else {
            const ip = this.baseUrl + 'apt/group/group-list?defaultPreference=' + defaultPreference;
            return ip;
        }

    }

    getAttackCampaignList(defaultPreference): string {
        const role = sessionStorage.getItem('role');
        if (role === 'ROLE_SOC_ADMIN' || role === 'ROLE_L1_ANALYST' || role === 'ROLE_L2_ANALYST') {
            const ip = this.baseUrl + 'apt/attack-campaign/attack-campaign-list';
            return ip;
        } else {
            const ip = this.baseUrl + 'apt/attack-campaign/attack-campaign-list?defaultPreference=' + defaultPreference;
            return ip;
        }
    }

    suggestionACListing(): string {
        const ip = this.baseUrl + 'apt/lookup/suggestions?attackCampaign=true';
        return ip;
    }

    suggestionGListing(): string {
        const ip = this.baseUrl + 'apt/lookup/suggestions?attackCampaign=false';
        return ip;
    }

    deleteAttackCampaign(id): string {
        const ip = this.baseUrl + 'apt/attack-campaign?id=' + id;
        return ip;
    }

    deleteGroups(id): string {
        const ip = this.baseUrl + 'apt/group?id=' + id;
        return ip;
    }

    getAttackCampaign(id): string {
        const ip = this.baseUrl + 'apt/attack-campaign?id=' + id;
        return ip;
    }
}
