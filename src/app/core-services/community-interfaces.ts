export interface SummaryData {
    type: string;
    title: string;
    createdDate: Date;
    fetchedDate?: Date;
    count: number;
    riskFactor?: string;
    threatScore?: number;
    country?: string;
}

export interface GetExploreAllFeeds {
    summaryData: SummaryData[];
    size: number;
    pageNumber: number;
    totalPages: number;
    totalElements: number;
}

export interface HashIndicatorCounts {
    indicatorType: string;
    totalCount: number;
}
export interface SelectedDate {
    startDate: string;
    endDate: string;
}

export interface SelectedDateInterfaceForSetting {
    startDate: any;
    endDate: any;
}
