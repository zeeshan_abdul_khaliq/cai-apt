export class GlobalConstants {
    public static Hash: string = 'Hash';
    public static Domain: string = 'Domain';
    public static IPv4: string = 'IPv4';
    public static URL: string = 'URL';
    public static CVE: string = 'CVE';
    public static Mutex: string = 'Mutex';
    public static Process: string = 'Process';
    public static Email: string = 'Email';
    public static Path: string = 'Path';
    public static Registry: string = 'Registry';
    public static File: string = 'File';
    public static Hostname: string = 'Hostname';

    public static file: string = 'file';
    public static hashes: string = 'hashes';
    public static hashData: string = 'hashData';
    public static domains: string = 'domains';
    public static domainData: string = 'domainData';
    public static dateIoc: string = 'dateIoc';
    public static process: string = 'process';
    public static processData: string = 'processData';
    public static urls: string = 'urls';
    public static urlData: string = 'urlData';
    public static registryData: string = 'registryData';
    public static registeries: string = 'registeries';
    public static mutexData: string = 'mutexData';
    public static mutexes: string = 'mutexes';
    public static paths: string = 'paths';
    public static pathData: string = 'pathData';
    public static ipData: string = 'ipData';
    public static filePath: string = 'filePath';
    public static dns: string = 'dns';
    public static Files: string = 'Files';
    public static ips: string = 'ips';
    public static fileNameData: string = 'fileNameData';
    public static fileName: string = 'fileName';
    public static hostNameData: string = 'hostNameData';
    public static hostName: string = 'hostName';
    public static hostNames: string = 'hostNames';
    public static cveData: string = 'cveData';
    public static cve: string = 'cve';
    public static cves: string = 'cves';
    public static emailData: string = 'emailData';
    public static email: string = 'email';
    public static emails: string = 'emails';
    public static groups: string = 'groups';

}
