export interface SummaryData {
    type: string;
    title: string;
    createdDate: Date;
    fetchedDate?: Date;
    count: number;
    riskFactor?: string;
    threatScore?: number;
    country?: string;
}

export interface GetExploreAllFeeds {
    summaryData: SummaryData[];
    size: number;
    pageNumber: number;
    totalPages: number;
    totalElements: number;
}

export interface HashIndicatorCounts {
    indicatorType: string;
    totalCount: number;
}

// Vulnerability Prioritization
export interface VulnerabilityObj {
    name?: string;
    description?: string;
}
export interface Severity {
    count: number;
    name: string;
}
export interface Vulnerability {
    cvssScore?: number;
    publishedDate?: string;
    modifiedDate?: string;
    cve: string;
    vulnerability?: VulnerabilityObj;
    products?: Products[]

}
export interface Products {
    assetType?: string;
    assetName?: string;
    manufacturer?: string;
    versions?: string[];
}
export interface Vulnerabilities {
    statusCode?: number;
    message?: string;
    vulnerabilities?: Vulnerability[];
    total?: number;
    pages?: number;
}
export interface RecentCVES {
    cve: string;
    cvssScore: number;
    publishedDate: string;
    modifiedDate: string;
}
export interface CriticalCVSS {
    cvssScore?: number;
    publishedDate?: string;
    cve?: string;
}
export interface selectedDate {
    startDate: string;
    endDate: string;
}

export interface selectedDateInterfaceForSetting {
    startDate: any;
    endDate: any;
}

// Fetch User

export interface Role {
    id: number;
    name: string;
    reportsTo: string;
}

export interface Organization {
    organizationId: number;
    organizationName: string;
    organizationLocation: string;
    organizationWebAddress: string;
    organizationStatus: boolean;
    tip: boolean;
    community: boolean;
    drp: boolean;
    kaspersky: boolean;
    digitalShadow?: any;
    networkMon: boolean;
    apt: boolean;
}

export interface FetchUser {
    id: number;
    enabled: boolean;
    locked: boolean;
    firstTimeLogin: boolean;
    password?: any;
    name: string;
    emailId: string;
    displayName: string;
    createdBy: string;
    createdDate: Date;
    role: Role;
    organization: Organization;
}


// CVE Details

export interface Cwe {
    description?: string;
    name?: string;
}

export interface Advisory {
    name?: string;
    source?: string;
    tags?: string[];
    url?: string;
}

export interface InfectedProduct {
    category?: string;
    cpeUri?: string;
    depends?: Depend[];
    manufacturer?: string;
    name?: string;
    cpeStatus?: CpeStatus[];
    versions?: string[];
    versionEndExcluding?: string;
    versionEndIncluding?: string;
    versionStartExcluding?: string;
    versionStartIncluding?: string;
    vulnerable?: boolean;
}
export interface CpeStatus {
    cpe?: string;
    status?: string;
}
export interface Depend {

}

export interface Impact {
    attackComplexity?: string;
    attackVector?: string;
    availabilityImpact?: string;
    baseScore?: number;
    confidentialityImpact?: string;
    exploitabilityScore?: number;
    impactScore?: number;
    integrityImpact?: string;
    privilegesRequired?: string;
    scope?: string;
    severity?: string;
    userInteraction?: string;
    vectorString?: string;
    version?: string;
}

export interface Weakness {
    id?: string;
    name?: string;
    prerequisites?: string;
    relatedWeakness?: string[];
    solution?: string;
    summary?: string;
}

export interface VulnerabilitiesDetail {
    advisories?: Advisory[];
    cve?: string;
    cveStandard?: string;
    cwes?: Cwe[];
    description?: string;
    identifier?: string;
    impacts?: Impact[];
    infectedProducts?: InfectedProduct[];
    lastModifiedDate?: string;
    publishDate?: string;
    weaknesses?: Weakness[];
}


export interface CVEDetails {
    statusCode?: number;
    message?: string;
    vulnerabilitiesDetail?: VulnerabilitiesDetail[];
    total?: number;
    pages?: number;
}
// CVE Detaile End here
export interface AssetList {
    category?: string;
    id?: number;
    ingestionDate?: Date;
    name?: string;
    patchDate?: Date;
    type?: string;
    updatedDate?: Date;
    vendor?: string;
    versionNumber?: string;
    nameValid?: boolean;
    vendorValid?: boolean;
    variantId?: string;
}
export interface AsmLookupForType {
    type?: string;
}
export interface VulnerabilitiesLookup {
    statusCode: number;
    message: string;
    list: any[];
}
export interface getAssetFileFormatLookup {
    format?: string;
}
export interface AsmLookupForCategory {
    category?: string;
}
export interface AssetLkpDto {
    category?: string;
    id?: number;
    ingestionDate?: Date;
    name?: string;
    patchDate?: Date;
    type?: string;
    updatedDate?: Date;
    vendor?: string;
    versionNumber?: string;
}

export interface addAssets {
    assetLkpDtos?: AssetLkpDto[];
    orgName?: string;
}
export interface Asset {
    id?: number;
    name?: string;
    vendor?: string;
    versionNumber?: string;
    category?: string;
    type?: string;
}

export interface AssetsFileUplaod {
    message?: string;
    statusCode?: number;
    assets?: Asset[];
}

export interface Cve {
    cve: string;
    id: number;
}

export interface PatchHistory {
    cves: Cve[];
    patchDate: Date;
    versionNumber: string;
}

export interface PatchStaus {
    count?: string;
    value?: string;
}
export interface DonutSeries {
    name?: string;
    count?: string;
}
export interface DonutLegends {
    name?: string;
}
export interface DonutCounts {
    value?: string;
}
export interface Preference {
    id?: number;
    preference?: string;
    preferenceValue?: string;
}

export interface User {
    id?: number;
    notifyTo?: string;
}

export interface VpScheduleReports {
    id?: number;
    notifyBy?: string;
    status?: boolean;
    lastNotificationDate?: any;
    notifyOrg?: string;
    preferences?: Preference[];
    users?: User[];
}
// Organization Base Vulnerabilites
export interface Product {
    manufacturer: string;
    assetName: string;
    assetType: string;
    versions: string[];
}

export interface OrgVulnerbailites {
    products: Product[];
    cvssScore: number;
    publishedDate: string;
    modifiedDate: string;
    cve: string;
}

export interface GetOrganizationCVEs {
    statusCode: number;
    vulnerabilities: OrgVulnerbailites[];
    total: number;
    pages: number;
}
