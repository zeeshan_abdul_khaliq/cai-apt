import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-main-heading',
  templateUrl: './main-heading.component.html',
  styleUrls: ['./main-heading.component.scss']
})
export class MainHeadingComponent implements OnInit {

  @Input() text: string;
  @Input() boldtext: string;
  @Input() class: string;

  constructor() { }

  ngOnInit(): void {
  }

}
