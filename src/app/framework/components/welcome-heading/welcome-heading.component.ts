import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome-heading',
  templateUrl: './welcome-heading.component.html',
  styleUrls: ['./welcome-heading.component.scss']
})
export class WelcomeHeadingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
