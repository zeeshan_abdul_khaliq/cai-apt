import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import {ToastrService} from 'ngx-toastr';
import {AptService} from '../../../core-services/apt.service';
import {CalendarSettingService} from '../../../core-services/calendarSetting.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  items: MenuItem[];
  role: string;
  checkIfdisabeldRoles = true;
  exploreItems: any[];
  exploreResponse: any;

  constructor(private communityService: AptService, private toastr: ToastrService,
              private calendarSettings: CalendarSettingService) {
    this.exploreItems = [];
    this.role = sessionStorage.getItem('role');
    if (this.role === 'Master Soc Analyst' || this.role === 'Distributor Soc Analyst' || this.role === 'Partner Soc Analyst') {
      this.checkIfdisabeldRoles = false;
    }
    this.items = [
          {
              label: 'Home',
              routerLink: '/cai/home',
          },
          {
              label: 'Threat Intelligence Platform',
              styleClass: 'modules',
              routerLink: '/tip/dashboard',
          },
          {
              label: 'Threat Sharing Community',
              styleClass: 'modules',
              routerLink: '/community/dashboard',

          },
          {
              label: 'Advanced Persistent Threats',
              styleClass: 'modules',
              items: [{
                  label: 'Dashboard',
                  routerLink: '/apt/dashboard',
                  routerLinkActiveOptions: true
              },
                  {
                      label: 'Groups',
                      routerLink: '/apt/groupList',
                      routerLinkActiveOptions: true
                  },
                  {
                      label: 'Attacks/Campaigns',
                      routerLink: '/apt/attacksCampaignList',
                      routerLinkActiveOptions: true
                  }
              ]
          },
          {
              label: 'Infrastructure Monitoring',
              styleClass: 'modules',
              routerLink: '/networkMon/dashboard',
          },
          {
              label: 'Vulnerability Prioritization',
              styleClass: 'modules',
              routerLink: '/vp/dashboard',
          },
          {
              label: 'Distributed Deception Platform',
              styleClass: 'modules',
          },
          {
              label: 'SIEM',
              styleClass: 'modules',
          },
          {
              label: 'SOAR',
          },

          {
              label: 'Third Party Vendors',
              styleClass: 'modules',
              items: [
                  {
                      label: 'Skurio - DRP ',
                      routerLink: '#',
                  },
                  {
                      label: 'RiskXchange - ASM',
                      routerLink: '#',
                  }

              ]
          },

      ];
  }

  ngOnInit(): void {
  }
}
