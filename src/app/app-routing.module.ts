import { NgModule } from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {AptModule} from './Advanced Persistent Threats/apt.module';
const routes: Routes = [];
@NgModule({
  imports: [RouterModule.forRoot(routes, {  }), AptModule],
  exports: [RouterModule],
})
export class AppRoutingModule { }
