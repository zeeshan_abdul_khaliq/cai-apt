import {Component, ElementRef, Input, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfirmationService, MenuItem} from "primeng/api";
import {NgxSpinnerService} from "ngx-spinner";
import {ToastrService} from "ngx-toastr";
import {GlobalConstants} from '../../core-services/global-constants';
import {AptService} from '../../core-services/apt.service';

@Component({
  selector: 'app-apt-campaigns-create-form',
  templateUrl: './apt-campaigns-add-form.component.html',
  styleUrls: ['./apt-campaigns-add-form.component.scss'],
  providers: [ConfirmationService]
})
export class AptCampaignsAddFormComponent implements OnInit {
  breadCrumbs: MenuItem[];

  createCampaignForm: FormGroup;
  zoom = 3;
  address: string;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  loading = false;
  hide = true;
  result: any;
  name: string;
  types: any = [];
  value: any;
  values = [];
  private fileIoCReponse: any;
  submitNextOne = false;
  submitNextThree = false;
  regionResult: any = [];
  sectorResult: any = [];
  groupNameResult: any = [];
  groupName: any = [];
  caseName: any = [];
  LcaseName: any;
  caseNameResult: any = [];
  regions: any = [];
  sectors: any = [];
  groupData: any = [];
  isTrue = false;
  groupRes: any = [];
  caseId: any;
  minDateValue: any;
   caseNIndicator: boolean = false;
  aliases: any = [];
  itemList = [];
  selectedItems = [];
  selectedItemsS = [];
  settings: any;
  settingsSector: any;
  @ViewChild('aliasInput') aliasInput: ElementRef<HTMLInputElement>;
  @ViewChild(GlobalConstants.file) uploadElRef: ElementRef;
  display = 'none';
  fileList: any;
  @ViewChild(GlobalConstants.file) file;
  filesToUpload: Set<File> = new Set();
  campaignFile: File;
  fileloading = false;
  spin = false;
  groupDetails: any;
  fileopen = false;
  fileNameopen = false;
  hostNameopen = false;
  emailopen = false;
  cveopen = false;
  filehash = false;
  popen = false;
  domainopne = false;
  ipopen = false;
  urlopen = false;
  registryopen = false;
  mutexopen = false;
  pathopen = false;
  constructor(private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private toastr: ToastrService,
    private ngZone: NgZone, private aptService: AptService, private confirmationService: ConfirmationService,
    private router: Router, private route: ActivatedRoute) {
    this.minDateValue = new Date();
    this.breadCrumbs = [
      {label: 'Attacks/Campaigns Listing', routerLink: '/apt/attacksCampaignList', routerLinkActiveOptions: true},
      {label: 'Create Attack/Campaign'},
    ];

    // redirect to home if already logged in

  }

  ngOnInit(): void {
    this.getLookupServices();
    this.initReactiveForm();
  }

  initReactiveForm() {
    this.createCampaignForm = this.formBuilder.group({
      attackName: ['', Validators.required],
      alias: [''],
      filess: [''],
      sectors: [''],
      regions: [''],
      methodToolUsed: [''],
      specialCharacteristics: [''],
      summary: [''],
      sources: [''],
      groups: this.formBuilder.array([this.groupNm('', '', '')]),
      dateIoc: this.formBuilder.array([this.dateIoc()]),
      fileName: this.formBuilder.array([this.fileName()]),
      hostName: this.formBuilder.array([this.hostName()]),
      email: this.formBuilder.array([this.email()]),
      cve: this.formBuilder.array([this.cve()]),
      filePath: this.formBuilder.array([this.filePath()]),
      dns: this.formBuilder.array([this.dns()]),
      process: this.formBuilder.array([this.processIoc()]),
      domains: this.formBuilder.array([this.domainsIoc()]),
      ips: this.formBuilder.array([this.ipsIoc()]),
      urls: this.formBuilder.array([this.urlsIoc()]),
      registeries: this.formBuilder.array([this.registeriesIoc()]),
      mutexes: this.formBuilder.array([this.mutexesIoc()]),
      paths: this.formBuilder.array([this.pathsIoc()])
    });
  }

  // convenience getter for easy access to form fields
  get createCampaignControl() {
    return this.createCampaignForm.controls;
  }

  onFilesAdded() {
    this.spin = true;
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (const key in files) {
      if (!isNaN(parseInt(key, 10))) {
        this.campaignFile = files[key];
        this.filesToUpload.add(files[key]);
      }
    }
    if (this.filesToUpload.size === 1) {
      this.uploadFile(this.filesToUpload);
    } else if (this.filesToUpload.size < 1) {
      this.toastr.error('You must select a file');
    } else {
      this.toastr.error('You can only select 1 file');
    }
  }

  public uploadFile(files: Set<File>): any {
    files.forEach(file => {
      const formData: FormData = new FormData();
      formData.append(GlobalConstants.file, file, file.name);
      this.fileloading = false;
      this.spinner.show('ioc-spinner');
      this.aptService.uploadIoCFile(formData).subscribe(
        res => {
          this.spinner.hide('ioc-spinner');

          this.spin = false;
          files.clear();
          this.filesToUpload.clear();
          this.fileIoCReponse = res;

          if (this.fileIoCReponse) {
            this.toastr.success('File Uploaded Successfully');
            if (this.fileIoCReponse.iocs && this.fileIoCReponse.iocs.length > 0) {
              this.fileloading = true;
              this.getAttackCampaignDetails(this.fileIoCReponse.iocs);
            } else {
              this.fileloading = false;
              this.toastr.error('No IoCs in File');
              this.uploadElRef.nativeElement.value = '';
              files.clear();
              this.filesToUpload.clear();
            }
          } else {
            this.spinner.hide('ioc-spinner');
            this.toastr.error('Only CSV file is accepted Kindly Try again');
            this.uploadElRef.nativeElement.value = '';
            files.clear();
            this.filesToUpload.clear();
          }

        },
        error => {
          this.spinner.hide('ioc-spinner');
          this.fileloading = false;
          this.uploadElRef.nativeElement.value = '';
          files.clear();
          this.filesToUpload.clear();
          this.spinner.hide('ioc-spinner');
          this.toastr.error('Some error occurred Please try again');
          this.spin = false;
        });
    });
  }

  getAttackCampaignDetails(iocs) {
    this.groupDetails = iocs;
    let domains = [];
    let fileName = [];
    let cve = [];
    let email = [];
    let hostName = [];
    let hashes = [];
    let ips = [];
    let mutexes = [];
    let paths = [];
    let process = [];
    let registries = [];
    let urls = [];
    if (this.groupDetails && this.groupDetails.length > 0) {
      this.groupDetails.map(iocObj => {
        if (iocObj.iocType && iocObj.iocType.id === 3) {
          hashes.push({
            hashValue: iocObj.value,
            sha1: iocObj.sha1,
            sha256: iocObj.sha256,
            md5: iocObj.md5,
            fileDescription: iocObj.fileDescription,
            fileType: iocObj.fileType,
            targetMachine: iocObj.targetMachine,
            malwareSignatureType: iocObj.malwareSignatureType,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 2) {
          domains.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 1) {
          ips.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 4) {
          mutexes.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 5) {
          paths.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 8) {
          registries.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 7) {
          urls.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 6) {
          process.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 9) {
          hostName.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 10) {
          email.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 11) {
          cve.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 12) {
          fileName.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        }
      });
    }
    this.formatIocs(hashes, GlobalConstants.hashData, GlobalConstants.hashes);
    if (this.groupDetails.hashes && this.groupDetails.hashes.length > 0) {
      this.fileopen = true;
      const control = this.createCampaignForm.controls.dateIoc as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let hashIndex = 0; hashIndex < this.groupDetails.hashes.length; hashIndex++) {
        const edd = this.groupDetails.hashes[hashIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.dateIoc, edd.id));
        }
        if (this.groupDetails.hashes[hashIndex].hashData && this.groupDetails.hashes[hashIndex].hashData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.dateIoc)['controls'][hashIndex].get(GlobalConstants.hashes);
          for (const edds of this.groupDetails.hashes[hashIndex].hashData) {
            cont.push(this.setHashValue(edds.hashValue, edds.sha1, edds.sha256, edds.md5,
              edds.fileDescription, edds.fileType, edds.targetMachine, edds.malwareSignatureType, edds.id));
          }
        }
      }
    }
    this.formatIocs(domains, GlobalConstants.domainData, GlobalConstants.domains);
    if (this.groupDetails.domains && this.groupDetails.domains.length > 0) {
      this.domainopne = true;
      const control = this.createCampaignForm.controls.domains as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.domains.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.domains[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.domains, edd.id));
        }
        if (this.groupDetails.domains[createCampaignFormIndex].domainData && this.groupDetails.domains[createCampaignFormIndex].domainData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.domains)['controls'][createCampaignFormIndex].get(GlobalConstants.domains);
          for (const edds of this.groupDetails.domains[createCampaignFormIndex].domainData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }
    this.formatIocs(process, GlobalConstants.processData, GlobalConstants.process);
    if (this.groupDetails.process && this.groupDetails.process.length > 0) {
      this.popen = true;
      const control = this.createCampaignForm.controls.process as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.process.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.process[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.process, edd.id));
        }
        if (this.groupDetails.process[createCampaignFormIndex].processData && this.groupDetails.process[createCampaignFormIndex].processData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.process)['controls'][createCampaignFormIndex].get(GlobalConstants.process);
          for (const edds of this.groupDetails.process[createCampaignFormIndex].processData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }
    this.formatIocs(urls, GlobalConstants.urlData, GlobalConstants.urls);
    if (this.groupDetails.urls && this.groupDetails.urls.length > 0) {
      this.urlopen = true;
      const control = this.createCampaignForm.controls.urls as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.urls.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.urls[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.urls, edd.id));
        }
        if (this.groupDetails.urls[createCampaignFormIndex].urlData && this.groupDetails.urls[createCampaignFormIndex].urlData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.urls)['controls'][createCampaignFormIndex].get(GlobalConstants.urls);
          for (const edds of this.groupDetails.urls[createCampaignFormIndex].urlData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }
    this.formatIocs(registries, GlobalConstants.registryData, GlobalConstants.registeries);
    if (this.groupDetails.registries && this.groupDetails.registries.length > 0) {
      this.registryopen = true;
      const control = this.createCampaignForm.controls.registeries as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.registries.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.registries[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.registeries, edd.id));
        }
        if (this.groupDetails.registries[createCampaignFormIndex].registryData && this.groupDetails.registries[createCampaignFormIndex].registryData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.registeries)['controls'][createCampaignFormIndex].get(GlobalConstants.registeries);
          for (const edds of this.groupDetails.registries[createCampaignFormIndex].registryData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }
    this.formatIocs(mutexes, GlobalConstants.mutexData, GlobalConstants.mutexes);
    if (this.groupDetails.mutexes && this.groupDetails.mutexes.length > 0) {
      this.mutexopen = true;
      const control = this.createCampaignForm.controls.mutexes as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.mutexes.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.mutexes[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.mutexes, edd.id));
        }
        if (this.groupDetails.mutexes[createCampaignFormIndex].mutexData && this.groupDetails.mutexes[createCampaignFormIndex].mutexData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.mutexes)['controls'][createCampaignFormIndex].get(GlobalConstants.mutexes);
          for (const edds of this.groupDetails.mutexes[createCampaignFormIndex].mutexData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }
    this.formatIocs(paths, GlobalConstants.pathData, GlobalConstants.paths);
    if (this.groupDetails.paths && this.groupDetails.paths.length > 0) {
      this.pathopen = true;
      const control = this.createCampaignForm.controls.paths as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.paths.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.paths[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.paths, edd.id));
        }
        if (this.groupDetails.paths[createCampaignFormIndex].pathData && this.groupDetails.paths[createCampaignFormIndex].pathData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.paths)['controls'][createCampaignFormIndex].get(GlobalConstants.paths);
          for (const edds of this.groupDetails.paths[createCampaignFormIndex].pathData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }
    this.formatIocs(ips, GlobalConstants.ipData, GlobalConstants.ips);
    if (this.groupDetails.ips && this.groupDetails.ips.length > 0) {
      this.ipopen = true;
      const control = this.createCampaignForm.controls.ips as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.ips.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.ips[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.ips, edd.id));
        }
        if (this.groupDetails.ips[createCampaignFormIndex].ipData && this.groupDetails.ips[createCampaignFormIndex].ipData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.ips)['controls'][createCampaignFormIndex].get(GlobalConstants.ips);
          for (const edds of this.groupDetails.ips[createCampaignFormIndex].ipData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }


    this.formatIocs(fileName, GlobalConstants.fileNameData, GlobalConstants.fileName);
    if (this.groupDetails.fileName && this.groupDetails.fileName.length > 0) {
      this.fileNameopen = true;
      const control = this.createCampaignForm.controls.fileName as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.fileName.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.fileName[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.fileName, edd.id));
        }
        if (this.groupDetails.fileName[createCampaignFormIndex].fileNameData && this.groupDetails.fileName[createCampaignFormIndex].fileNameData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.fileName)['controls'][createCampaignFormIndex].get(GlobalConstants.Files);
          for (const edds of this.groupDetails.fileName[createCampaignFormIndex].fileNameData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }

    this.formatIocs(hostName, GlobalConstants.hostNameData, GlobalConstants.hostName);
    if (this.groupDetails.hostName && this.groupDetails.hostName.length > 0) {
      this.hostNameopen = true;
      const control = this.createCampaignForm.controls.hostName as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.hostName.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.hostName[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.hostName, edd.id));
        }
        if (this.groupDetails.hostName[createCampaignFormIndex].hostNameData && this.groupDetails.hostName[createCampaignFormIndex].hostNameData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.hostName)['controls'][createCampaignFormIndex].get(GlobalConstants.hostNames);
          for (const edds of this.groupDetails.hostName[createCampaignFormIndex].hostNameData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }

    this.formatIocs(cve, GlobalConstants.cveData, GlobalConstants.cve);
    if (this.groupDetails.cve && this.groupDetails.cve.length > 0) {
      this.cveopen = true;
      const control = this.createCampaignForm.controls.cve as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.cve.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.cve[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.cve, edd.id));
        }
        if (this.groupDetails.cve[createCampaignFormIndex].cveData && this.groupDetails.cve[createCampaignFormIndex].cveData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.cve)['controls'][createCampaignFormIndex].get('cves');
          for (const edds of this.groupDetails.cve[createCampaignFormIndex].cveData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }

    this.formatIocs(email, GlobalConstants.emailData, GlobalConstants.email);
    if (this.groupDetails.email && this.groupDetails.email.length > 0) {
      this.emailopen = true;
      const control = this.createCampaignForm.controls.email as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let createCampaignFormIndex = 0; createCampaignFormIndex < this.groupDetails.email.length; createCampaignFormIndex++) {

        const edd = this.groupDetails.email[createCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          control.push(this.setDomain(edd.date, GlobalConstants.email, edd.id));
        }
        if (this.groupDetails.email[createCampaignFormIndex].emailData && this.groupDetails.email[createCampaignFormIndex].emailData.length > 0) {
          const cont = this.createCampaignForm.get(GlobalConstants.email)['controls'][createCampaignFormIndex].get(GlobalConstants.emails);
          for (const edds of this.groupDetails.email[createCampaignFormIndex].emailData) {
            cont.push(this.setIocValue(edds.value, edds.id));
          }
        }
      }
    }
  }

  formatIocs(iocArr, type, iocType) {
    const ioc = [];
    iocArr.forEach(mapResponse => {
      let match = ioc.find(r => r.date === mapResponse.date);
      if (match) {
        match[type] = match[type].concat(mapResponse);
      } else {
        const iocData = {
          date: mapResponse.date,
          id: mapResponse.id,
        };
        iocData[type] = [mapResponse];
        ioc.push(iocData);
      }
    });
    this.groupDetails[iocType] = ioc;
  }

  setDomain(date, type, id): FormGroup {
    if (type === GlobalConstants.domains) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        domains: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.mutexes) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        mutexes: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.registeries) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        registeries: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.dateIoc) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        hashes: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.ips) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        ips: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.urls) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        urls: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.paths) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        paths: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.process) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        process: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.registeries) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        registeries: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.fileName) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        Files: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.hostName) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        hostNames: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.email) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        emails: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.cve) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        cves: this.formBuilder.array([])
      });
    }
  }

  setIocValue(value, id): FormGroup {
    return this.formBuilder.group({
      value: [value ],
      id: id
    });
  }

  setHashValue(hash, sha256, sha1, md5, fileDescription, fileType, targetMachine, malwareSignatureType, id) {
    return this.formBuilder.group({
      hash: [hash ],
      sha256: [sha256 ],
      sha1: [sha1 ],
      md5: [md5 ],
      fileDescription: [fileDescription ],
      fileType: [fileType ],
      targetMachine: [targetMachine ],
      malwareSignatureType: [malwareSignatureType ],
      id: id
    });
  }
  /*--------------Submit form------------------*/
  onSubmit() {
    let groups = [];
    let domains = [];
    let hashes = [];
    let ips = [];
    let mutexes = [];
    let paths = [];
    let process = [];
    let registries = [];
    let urls = [];
    let iocs = [];
    let fileName = [];
    let hostName = [];
    let cve = [];
    let email = [];
    this.caseName.map(mapResponse => {
      this.groupData.map(createCampaignFormIndex => {
        if (this.createCampaignForm.value.attackName === createCampaignFormIndex.caseName) {
          if (createCampaignFormIndex.caseName) {
            this.isTrue = true;
            this.caseId = createCampaignFormIndex.id;
          }
        }
      });
    });
    if (this.createCampaignForm.value.groups && this.createCampaignForm.value.groups.length > 0) {
      this.createCampaignForm.value.groups.map(iocObj => {
        if (iocObj.id === '') {
          let idGrp: any;
          this.groupRes.map(createCampaignFormIndex => {
            if (createCampaignFormIndex.groupName === iocObj.groupName) {
              idGrp = createCampaignFormIndex.id;
              groups.push({
                groupName: iocObj.groupName,
                id: idGrp,
                mitreName: iocObj.mitreName
              });
            }
          });
        } else {
          groups.push({
            groupName: iocObj.groupName,
            id: iocObj.id,
            mitreName: iocObj.mitreName
          });
        }
      });
    }

    if (this.createCampaignForm.value.dateIoc && this.createCampaignForm.value.dateIoc.length > 0) {
      hashes = this.createCampaignForm.value.dateIoc.map(iocObj => {
        if (iocObj.date !== '' && iocObj.hashes && iocObj.hashes.length > 0) {
          iocObj.hashes.map(iocSubObj => {
            if ((iocSubObj.fileDescription !== '' && iocSubObj.fileDescription !== undefined && iocSubObj.fileDescription !== null) ||
              (iocSubObj.fileType !== '' && iocSubObj.fileType !== undefined && iocSubObj.fileType !== null) ||
              (iocSubObj.hash !== '' && iocSubObj.hash !== undefined && iocSubObj.hash !== null) ||
              (iocSubObj.sha1 !== '' && iocSubObj.sha1 !== undefined && iocSubObj.sha1 !== null) ||
              (iocSubObj.sha256 !== '' && iocSubObj.sha256 !== undefined && iocSubObj.sha256 !== null) ||
              (iocSubObj.md5 !== '' && iocSubObj.md5 !== undefined && iocSubObj.md5 !== null) ||
              (iocSubObj.malwareSignatureType !== '' && iocSubObj.malwareSignatureType !== undefined && iocSubObj.malwareSignatureType !== null) ||
              (iocSubObj.targetMachine !== '' && iocSubObj.targetMachine !== undefined && iocSubObj.targetMachine !== null)) {
              iocs.push({
                fileDescription: iocSubObj.fileDescription,
                fileType: iocSubObj.fileType,
                value: iocSubObj.hash,
                malwareSignatureType: iocSubObj.malwareSignatureType,
                md5: iocSubObj.md5,
                sha1: iocSubObj.sha1,
                sha256: iocSubObj.sha256,
                targetMachine: iocSubObj.targetMachine,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 3,
                  value: GlobalConstants.Hash,
                },
              });
            }
          });
        }
      });
    }
    if (this.createCampaignForm.value.domains && this.createCampaignForm.value.domains.length > 0) {
      domains = this.createCampaignForm.value.domains.map(iocObj => {
        if (iocObj.date !== '' && iocObj.domains && iocObj.domains.length > 0) {
          
          iocObj.domains.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 2,
                  value: GlobalConstants.Domain,

                },
              });
            }
          });
        }
      });

    }
    if (this.createCampaignForm.value.ips && this.createCampaignForm.value.ips.length > 0) {
      ips = this.createCampaignForm.value.ips.map(iocObj => {
        if (iocObj.date !== '' && iocObj.ips && iocObj.ips.length > 0) {
          
          iocObj.ips.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 1,
                  value: GlobalConstants.IPv4,
                },
              });
            }
          });
        }
      });
    }
    if (this.createCampaignForm.value.mutexes && this.createCampaignForm.value.mutexes.length > 0) {
      mutexes = this.createCampaignForm.value.mutexes.map(iocObj => {
        if (iocObj.date !== '' && iocObj.mutexes && iocObj.mutexes.length > 0) {
          
          iocObj.mutexes.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 4,
                  value: GlobalConstants.Mutex,
                },
              });
            }
          });
        }
      });
    }
    if (this.createCampaignForm.value.paths && this.createCampaignForm.value.paths.length > 0) {
      paths = this.createCampaignForm.value.paths.map(iocObj => {
        if (iocObj.date !== '' && iocObj.paths && iocObj.paths.length > 0) {
          
          iocObj.paths.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 5,
                  value: GlobalConstants.Path,
                },
              });
            }
          });
        }
      });
    }
    if (this.createCampaignForm.value.process && this.createCampaignForm.value.process.length > 0) {
      process = this.createCampaignForm.value.process.map(iocObj => {
        if (iocObj.date !== '' && iocObj.process && iocObj.process.length > 0) {
          
          iocObj.process.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 6,
                  value: GlobalConstants.Process,
                },
              });
            }
          });
        }
      });
    }
    if (this.createCampaignForm.value.registeries && this.createCampaignForm.value.registeries.length > 0) {
      registries = this.createCampaignForm.value.registeries.map(iocObj => {
        if (iocObj.date !== '' && iocObj.registeries && iocObj.registeries.length > 0) {
          
          iocObj.registeries.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 8,
                  value: GlobalConstants.Registry,
                },
              });
            }
          });
        }
      });
    }
    if (this.createCampaignForm.value.urls && this.createCampaignForm.value.urls.length > 0) {
      urls = this.createCampaignForm.value.urls.map(iocObj => {
        if (iocObj.date !== '' && iocObj.urls && iocObj.urls.length > 0) {
          
          iocObj.urls.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 7,
                  value: GlobalConstants.URL,
                },
              });
            }
          });
        }
      });
    }

    if (this.createCampaignForm.value.fileName && this.createCampaignForm.value.fileName.length > 0) {
      fileName = this.createCampaignForm.value.fileName.map(iocObj => {
        if (iocObj.date !== '' && iocObj.Files && iocObj.Files.length > 0) {
          
          iocObj.Files.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 12,
                  value: GlobalConstants.File,
                },
              });
            }
          });
        }
      });

    }
    if (this.createCampaignForm.value.hostName && this.createCampaignForm.value.hostName.length > 0) {
      hostName = this.createCampaignForm.value.hostName.map(iocObj => {
        if (iocObj.date !== '' && iocObj.hostNames && iocObj.hostNames.length > 0) {
          
          iocObj.hostNames.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 9,
                  value: GlobalConstants.Hostname,
                },
              });
            }
          });
        }
      });

    }
    if (this.createCampaignForm.value.email && this.createCampaignForm.value.email.length > 0) {
      email = this.createCampaignForm.value.email.map(iocObj => {
        if (iocObj.date !== '' && iocObj.emails && iocObj.emails.length > 0) {
          
          iocObj.emails.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 10,
                  value: GlobalConstants.Email,
                },
              });
            }
          });
        }
      });

    }
    if (this.createCampaignForm.value.cve && this.createCampaignForm.value.cve.length > 0) {
      cve = this.createCampaignForm.value.cve.map(iocObj => {
        if (iocObj.date !== '' && iocObj.cves && iocObj.cves.length > 0) {
          
          iocObj.cves.map(iocSubObj => {
            if (iocSubObj.value !== '' && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 11,
                  value: GlobalConstants.CVE,
                },
              });
            }
          });

        }
      });

    }
    if (this.createCampaignForm.value.regions === '') {
      this.createCampaignForm.value.regions = [];
    }
    if (this.createCampaignForm.value.sectors === '') {
      this.createCampaignForm.value.sectors = [];
    }
    if (this.isTrue) {
      const dataJson = {
        aliases: this.aliases,
        caseName: this.createCampaignForm.value.attackName,
        id: this.caseId,
        methodToolUsed: this.createCampaignForm.value.methodToolUsed,
        sources: this.createCampaignForm.value.sources,
        groups: groups,
        specialCharacteristics: this.createCampaignForm.value.specialCharacteristics,
        summary: this.createCampaignForm.value.summary,
        countries: this.createCampaignForm.value.regions,
        sectors: this.createCampaignForm.value.sectors,
        iocs: iocs
      };
      this.updateForm(dataJson);
    } else {
      const dataJson = {
        aliases: this.aliases,
        caseName: this.createCampaignForm.value.attackName,
        methodToolUsed: this.createCampaignForm.value.methodToolUsed,
        sources: this.createCampaignForm.value.sources,
        groups: groups,
        specialCharacteristics: this.createCampaignForm.value.specialCharacteristics,
        summary: this.createCampaignForm.value.summary,
        countries: this.createCampaignForm.value.regions,
        sectors: this.createCampaignForm.value.sectors,
        iocs: iocs

      };
      this.createForm(dataJson);
    }

  }

  add(event) {
    const input = event.input;
    let value = event.value;

    if ((value || '').trim()) {
      value = value.trim();
      if (this.aliases.find((test) => test.value.toLowerCase() === value.toLowerCase()) === undefined) {
        this.aliases.push({value: value.trim()});
        this.createCampaignForm.value.alias = this.aliases;
      }
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.aliasInput = null;
  }

  remove(fruit: string): void {
    const index = this.aliases.indexOf(fruit);

    if (index >= 0) {
      this.aliases.splice(index, 1);
      this.createCampaignForm.value.alias = this.aliases;
    }
  }


  createForm(dataJson) {
    this.spinner.show('createattack-spinner');
    this.aptService.postAttacksCampaign(dataJson).subscribe(
      res => {
        this.spinner.hide('createattack-spinner');
        const response: any = res;
        if (response.responseDto.responseCode === 200 || response.responseDto.responseCode === 201) {
          this.toastr.success(response.responseDto.response);
          this.viewAttackCampaign(response.id);
        } else {
          this.toastr.error('Some error occurred');
        }
      },
      error => {
        this.spinner.hide('createattack-spinner');
        const err = error;
        this.toastr.error(err.responseDto.response);
      });
  }

  updateForm(dataJson) {
    this.spinner.show('createattack-spinner');

    this.aptService.updateAttacksCampaign(dataJson).subscribe(
        res => {
          this.spinner.hide('createattack-spinner');
          const response: any = res;
          if (response.responseDto.responseCode === 200 || response.responseDto.responseCode === 201) {
            this.toastr.success(response.responseDto.response);
            this.viewAttackCampaign(response.id);
          } else {
            this.toastr.error('Some error occurred');
          }
        },
        error => {
          this.spinner.hide('createattack-spinner');
          const err = error;
          this.toastr.error(err.responseDto.response);
        });
  }

  searchCasename(event): void {
    this.caseNIndicator = false;
    this.caseNameResult = this.caseName.filter(iocObj => iocObj.toLowerCase().startsWith(event.query.toLowerCase()));
  }
  caseNameSelect(event): void {
    this.selectedItems = [];
    this.selectedItemsS = [];
    this.caseNIndicator = false;
    for (let cas of this.LcaseName) {
      if (cas.caseName === event && cas.indicatorCount > 0) {
        this.caseNIndicator = !this.caseNIndicator;
        this.createCampaignForm.reset();
        this.createCampaignForm.setControl(GlobalConstants.groups, new FormArray([this.groupNm('', '', '')]));
        this.createCampaignForm.setControl(GlobalConstants.dateIoc, new FormArray([this.dateIoc()]));
        this.createCampaignForm.setControl(GlobalConstants.filePath, new FormArray([this.filePath()]));
        this.createCampaignForm.setControl(GlobalConstants.dns, new FormArray([this.dns()]));
        this.createCampaignForm.setControl(GlobalConstants.fileName, new FormArray([this.fileName()]));
        this.createCampaignForm.setControl(GlobalConstants.hostName, new FormArray([this.hostName()]));
        this.createCampaignForm.setControl(GlobalConstants.email, new FormArray([this.email()]));
        this.createCampaignForm.setControl(GlobalConstants.cve, new FormArray([this.cve()]));
        this.createCampaignForm.setControl(GlobalConstants.process, new FormArray([this.processIoc()]));
        this.createCampaignForm.setControl(GlobalConstants.domains, new FormArray([this.domainsIoc()]));
        this.createCampaignForm.setControl(GlobalConstants.ips, new FormArray([this.ipsIoc()]));
        this.createCampaignForm.setControl(GlobalConstants.urls, new FormArray([this.urlsIoc()]));
        this.createCampaignForm.setControl(GlobalConstants.registeries, new FormArray([this.registeriesIoc()]));
        this.createCampaignForm.setControl(GlobalConstants.mutexes, new FormArray([this.mutexesIoc()]));
        this.createCampaignForm.setControl(GlobalConstants.paths, new FormArray([this.pathsIoc()]));
        this.fileloading = false;
        this.uploadElRef.nativeElement.value = '';
        this.filesToUpload.clear();
        this.fileopen = false;
        this.filehash = false;
        this.fileNameopen = false;
        this.hostNameopen = false;
        this.cveopen = false;
        this.emailopen = false;
        this.popen = false;
        this.domainopne = false;
        this.ipopen = false;
        this.urlopen = false;
        this.registryopen = false;
        this.mutexopen = false;
        this.pathopen = false;
        break;
      }
      else if (cas.caseName === event && cas.indicatorCount === 0) {
        this.caseNIndicator = false;
        if (cas.countries && cas.countries.length > 0) {
          for (let region of cas.countries) {
            const data = {
              id: region.id,
              itemName: region.countryName,
              category: region.region
            };
            this.selectedItems.push(data);

          }
        }
        if (cas.sectors && cas.sectors.length > 0) {
          for (let sec of cas.sectors) {
            const data = {
              id: sec.id,
              itemName: sec.value,
            };
            this.selectedItemsS.push(data);

          }
        }
      } else if (cas.caseName !== event){
        this.caseNIndicator = false;

      }
    }
  }
  searchGroupName(event): void {
    if (event) {
      this.groupNameResult = this.groupName.filter(iocObj => iocObj.toLowerCase().startsWith(event.query.toLowerCase()));
    }
  }

  /*--------------Form Array Fields - Initialize Form Array - Add mapResponse to form array - Remove mapResponse from form array------------------*/

  dateIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      hashes: this.formBuilder.array([this.hashs()]),
    });
  }

  filePath(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      filePaths: this.formBuilder.array([this.paths()]),
    });
  }

  dns(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      dnss: this.formBuilder.array([this.domains()]),
    });
  }
  fileName(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      Files: this.formBuilder.array([this.iocValue()]),
    });
  }
  hostName(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      hostNames: this.formBuilder.array([this.iocValue()]),
    });
  }
  email(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      emails: this.formBuilder.array([this.iocValue()]),
    });
  }
  cve(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      cves: this.formBuilder.array([this.iocValue()]),
    });
  }

  paths(): FormGroup {
    return this.formBuilder.group({
      path: ['' ],
    });
  }


  processIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      process: this.formBuilder.array([this.iocValue()]),
    });
  }

  domainsIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      domains: this.formBuilder.array([this.iocValue()]),
    });
  }

  ipsIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      ips: this.formBuilder.array([this.iocValue()]),
    });
  }

  urlsIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      urls: this.formBuilder.array([this.iocValue()]),
    });
  }

  registeriesIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      registeries: this.formBuilder.array([this.iocValue()]),
    });
  }

  mutexesIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      mutexes: this.formBuilder.array([this.iocValue()]),
    });
  }

  pathsIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      paths: this.formBuilder.array([this.iocValue()]),
    });
  }

  iocValue(): FormGroup {
    return this.formBuilder.group({
      value: ['' ],
    });
  }

  addProcess() {
    const control = this.createCampaignForm.controls.process as FormArray;
    control.push(this.processIoc());
  }

  addDomain() {
    const control = this.createCampaignForm.controls.domains as FormArray;
    control.push(this.domainsIoc());
  }

  addIps() {
    const control = this.createCampaignForm.controls.ips as FormArray;
    control.push(this.ipsIoc());
  }

  addUrls() {
    const control = this.createCampaignForm.controls.urls as FormArray;
    control.push(this.urlsIoc());
  }

  addRegisteries() {
    const control = this.createCampaignForm.controls.registeries as FormArray;
    control.push(this.registeriesIoc());
  }

  addMutexes() {
    const control = this.createCampaignForm.controls.mutexes as FormArray;
    control.push(this.mutexesIoc());
  }

  addPath() {
    const control = this.createCampaignForm.controls.paths as FormArray;
    control.push(this.pathsIoc());
  }
  addFileName() {
    const control = this.createCampaignForm.controls.fileName as FormArray;
    control.push(this.fileName());
  }
  addHostName() {
    const control = this.createCampaignForm.controls.hostName as FormArray;
    control.push(this.hostName());
  }
  addEmail() {
    const control = this.createCampaignForm.controls.email as FormArray;
    control.push(this.email());
  }
  addCVE() {
    const control = this.createCampaignForm.controls.cve as FormArray;
    control.push(this.cve());
  }



  addSubIoc(j, type) {
    if (type === GlobalConstants.process) {
      j.get(GlobalConstants.process).push(this.iocValue());
    } else if (type === GlobalConstants.domains) {
      j.get(GlobalConstants.domains).push(this.iocValue());
    } else if (type === GlobalConstants.ips) {
      j.get(GlobalConstants.ips).push(this.iocValue());
    } else if (type === GlobalConstants.urls) {
      j.get(GlobalConstants.urls).push(this.iocValue());
    } else if (type === GlobalConstants.registeries) {
      j.get(GlobalConstants.registeries).push(this.iocValue());
    } else if (type === GlobalConstants.mutexes) {
      j.get(GlobalConstants.mutexes).push(this.iocValue());
    } else if (type === GlobalConstants.paths) {
      j.get(GlobalConstants.paths).push(this.iocValue());
    } else if (type === GlobalConstants.fileName) {
      j.get(GlobalConstants.Files).push(this.iocValue());
    } else if (type === GlobalConstants.hostName) {
      j.get(GlobalConstants.hostNames).push(this.iocValue());
    } else if (type === GlobalConstants.email) {
      j.get('emails').push(this.iocValue());
    } else if (type === GlobalConstants.cve) {
      j.get('cves').push(this.iocValue());
    }
  }

  removeProcess(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.process as FormArray;
    control.removeAt(createCampaignFormIndex);
  }

  removeDomain(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.domains as FormArray;
    control.removeAt(createCampaignFormIndex);
  }

  removeIps(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.ips as FormArray;
    control.removeAt(createCampaignFormIndex);
  }

  removeUrls(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.urls as FormArray;
    control.removeAt(createCampaignFormIndex);
  }

  removeRegisteries(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.registeries as FormArray;
    control.removeAt(createCampaignFormIndex);
  }

  removeMutexes(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.mutexes as FormArray;
    control.removeAt(createCampaignFormIndex);
  }

  removePath(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.paths as FormArray;
    control.removeAt(createCampaignFormIndex);
  }
  removeFileName(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.fileName as FormArray;
    control.removeAt(createCampaignFormIndex);
  }
  removeHostName(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.hostName as FormArray;
    control.removeAt(createCampaignFormIndex);
  }
  removeEmail(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.email as FormArray;
    control.removeAt(createCampaignFormIndex);
  }
  removeCVE(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.cve as FormArray;
    control.removeAt(createCampaignFormIndex);
  }
  removeSubIocs(createCampaignFormIndex: number, j, type) {
    if (type === GlobalConstants.process) {
      const control = j.controls.process as FormArray;
      control.removeAt(createCampaignFormIndex);
    } else if (type === GlobalConstants.domains) {
      const control = j.controls.domains as FormArray;
      control.removeAt(createCampaignFormIndex);
    } else if (type === GlobalConstants.ips) {
      const control = j.controls.ips as FormArray;
      control.removeAt(createCampaignFormIndex);
    } else if (type === GlobalConstants.urls) {
      const control = j.controls.urls as FormArray;
      control.removeAt(createCampaignFormIndex);
    } else if (type === GlobalConstants.registeries) {
      const control = j.controls.registeries as FormArray;
      control.removeAt(createCampaignFormIndex);
    } else if (type === GlobalConstants.mutexes) {
      const control = j.controls.mutexes as FormArray;
      control.removeAt(createCampaignFormIndex);
    } else if (type === GlobalConstants.paths) {
      const control = j.controls.paths as FormArray;
      control.removeAt(createCampaignFormIndex);
    }else if (type === GlobalConstants.fileName) {
      const control = j.controls.Files as FormArray;
      control.removeAt(createCampaignFormIndex);
    } else if (type === GlobalConstants.hostName) {
      const control = j.controls.hostNames as FormArray;
      control.removeAt(createCampaignFormIndex);
    } else if (type === GlobalConstants.email) {
      const control = j.controls.emails as FormArray;
      control.removeAt(createCampaignFormIndex);
    } else if (type === GlobalConstants.cve) {
      const control = j.controls.cves as FormArray;
      control.removeAt(createCampaignFormIndex);
    }
  }


  domains(): FormGroup {
    return this.formBuilder.group({
      domain: ['' ],
    });
  }
  Files(): FormGroup {
    return this.formBuilder.group({
      File: ['' ],
    });
  }
  hostNames(): FormGroup {
    return this.formBuilder.group({
      host: ['' ],
    });
  }
  emails(): FormGroup {
    return this.formBuilder.group({
      emai: ['' ],
    });
  }
  cves(): FormGroup {
    return this.formBuilder.group({
      cv: ['' ],
    });
  }

  hashs() {
    return this.formBuilder.group({
      hash: [null  ],
      sha256: [null  ],
      sha1: [null ],
      md5: [null  ],
      fileDescription: [null ],
      fileType: [null ],
      targetMachine: [null  ],
      malwareSignatureType: [null  ]
    });
  }

  groupNm(id, type, value) {
    return this.formBuilder.group({
      id: [id],
      groupName: [type, Validators.required],
      mitreName: [{value, disabled: true}, Validators.required],
    });
  }

  attackCampaigns(): FormGroup {
    return this.formBuilder.group({
      associatedIndicators: ['' ],
      caseName: [''],
      regions: ['' ],
      sectors: ['' ]
    });
  }

  addGroups() {
    const control = this.createCampaignForm.controls.groups as FormArray;
    control.push(this.groupNm('', '', ''));
  }

  removeGroups(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.groups as FormArray;
    control.removeAt(createCampaignFormIndex);
  }

  addHash(j) {
    j.get(GlobalConstants.hashes).push(this.hashs());
  }

  addDateHash() {
    const control = this.createCampaignForm.controls.dateIoc as FormArray;
    control.push(this.dateIoc());
  }

  removeHash(createCampaignFormIndex: number, j) {
    const control = j.controls.hashes as FormArray;
    control.removeAt(createCampaignFormIndex);
  }
  removeDateHash(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.dateIoc as FormArray;
    control.removeAt(createCampaignFormIndex);
  }
  // in case of zero index
  removeGroup(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.groups as FormArray;
    if (control.length <= 1) {
      const control = this.createCampaignForm.get(GlobalConstants.groups)['controls'];
      control[createCampaignFormIndex]['controls'].id.patchValue('');
      control[createCampaignFormIndex]['controls'].groupName.patchValue('');
      control[createCampaignFormIndex]['controls'].mitreName.patchValue('');
      control[createCampaignFormIndex]['controls'].id.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].groupName.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].mitreName.updateValueAndValidity();
    } else if (control.length > 1) {
      const control = this.createCampaignForm.controls.groups as FormArray;
      control.removeAt(createCampaignFormIndex);
    }
  }
  removeDateHashZero(createCampaignFormIndex: number) {
    const control = this.createCampaignForm.controls.dateIoc as FormArray;
    if (control.length <= 1) {
      const control = this.createCampaignForm.get(GlobalConstants.dateIoc)['controls'];
      control[createCampaignFormIndex]['controls'].date.patchValue('');
      control[createCampaignFormIndex]['controls'].date.updateValueAndValidity();
      const cont = this.createCampaignForm.get(GlobalConstants.dateIoc)['controls'][0].get(GlobalConstants.hashes);
      while (cont.length !== 0) {
        cont.removeAt(0)
      }
      cont.push(this.setHashValue(null , null , null , null ,
        null , null , null , null , null ));


    } else if (control.length > 1) {
      control.removeAt(createCampaignFormIndex);

    }
  }
  removeHashz(createCampaignFormIndex: number, j) {
    const control = j.controls.hashes as FormArray;
    if (control.length <= 1) {
      const control = j.get(GlobalConstants.hashes)['controls'];
      control[createCampaignFormIndex]['controls'].hash.patchValue('');
      control[createCampaignFormIndex]['controls'].sha256.patchValue('');
      control[createCampaignFormIndex]['controls'].sha1.patchValue('');
      control[createCampaignFormIndex]['controls'].md5.patchValue('');
      control[createCampaignFormIndex]['controls'].fileDescription.patchValue('');
      control[createCampaignFormIndex]['controls'].fileType.patchValue('');
      control[createCampaignFormIndex]['controls'].targetMachine.patchValue('');
      control[createCampaignFormIndex]['controls'].malwareSignatureType.patchValue('');
      control[createCampaignFormIndex]['controls'].id.patchValue('');
      control[createCampaignFormIndex]['controls'].hash.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].sha256.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].md5.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].fileDescription.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].targetMachine.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].malwareSignatureType.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].fileType.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].id.updateValueAndValidity();
      control[createCampaignFormIndex]['controls'].sha1.updateValueAndValidity();

    } else if (control.length > 1) {
      control.removeAt(createCampaignFormIndex);

    }
  }

  removeProcessZ(createCampaignFormIndex: number, type, Vtype) {
    const control = this.createCampaignForm.controls[type] as FormArray;
    if (control.length <= 1) {
      const control = this.createCampaignForm.get(type)['controls'];
      control[createCampaignFormIndex]['controls'].date.patchValue('');
      control[createCampaignFormIndex]['controls'].date.updateValueAndValidity();
      const cont = this.createCampaignForm.get(type)['controls'][0].get(Vtype);
      while (cont.length !== 0) {
        cont.removeAt(0)
      }
      cont.push(this.setIocValue('', '' ));


    } else if (control.length > 1) {
      control.removeAt(createCampaignFormIndex);

    }
  }
  removeSubIocsZ(createCampaignFormIndex: number, j, type) {
    const control = j.controls[type] as FormArray;
    if (control.length <= 1) {
      const control = j.get(type)['controls'];
      control[createCampaignFormIndex]['controls'].value.patchValue('');
      control[createCampaignFormIndex]['controls'].value.updateValueAndValidity();

    } else if (control.length > 1) {
      control.removeAt(createCampaignFormIndex);

    }
  }
  getLookupServices() {
    this.settings = {
      singleSelection: false,
      text: "Select Countries",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Regions and Countries',
      enableSearchFilter: true,
      badgeShowLimit: 5,
      groupBy: "category"
    };
    this.settingsSector = {
      singleSelection: false,
      text: "Select Sectors",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Sectors',
      enableSearchFilter: true,
      badgeShowLimit: 5,
    };
    this.itemList = [];
    this.aptService.getSectors().subscribe(
      res => {
        const result: any = res;
        this.selectedItemsS = [];
        this.sectorResult = [];
        if (result && result.length > 0) {
          for (let sec of result) {
            const data = {
              id: sec.id,
              itemName: sec.value,
            };
            this.sectorResult.push(data);

          }

        }else {
          this.sectorResult = [];
        }
      },
      err => {
      }
    );

    this.aptService.getRegions().subscribe(
      res => {
        const result: any = res;
        this.selectedItems = [];
        if (result && result.length > 0) {
          this.regionResult = result;
          for (let region of this.regionResult) {
            const data = {
              id: region.id,
              itemName: region.countryName,
              category: region.region
            };
            this.itemList.push(data);

          }

        }else {
          this.regionResult = [];
        }
      },
      err => {
      }
    );

    this.aptService.getGroupNames().subscribe(
      res => {
        const result: any = res;
        this.groupName = [];
        this.groupRes = [];
        if (result && result.length > 0) {
          result.map(mapResponse => {
            if (mapResponse.groupName && mapResponse.groupName !== undefined && mapResponse.id) {
              this.groupName.push(mapResponse.groupName);
              this.groupRes.push(mapResponse);
              return mapResponse.groupName;
            }
          });
        } else {
          this.groupName = [];
        }
      },
      err => {
      }
    );

    this.aptService.getCaseNames().subscribe(
      res => {
        const result: any = res;
        this.caseName = [];
        if (result && result.length > 0) {
          this.groupData = result;
          this.LcaseName = result;
          if (result && result.length > 0) {
            result.map(mapResponse => {
              if (mapResponse.caseName && mapResponse.caseName !== undefined) {
                this.caseName.push(mapResponse.caseName);
                return mapResponse.caseName;
              }
            });
          }
        } else {
          this.caseName = [];
        }
      },
      err => {
      }
    );

  }

  getGroupData(event) {
    if (event && event.value) {
      if (this.groupData && this.groupData.length > 0) {
        for (const edd of this.groupData) {
          if (edd.caseName === event.value) {
            const control = this.createCampaignForm.controls.groups as FormArray;
            if (control && control.value) {
              for (let createCampaignFormIndex = 0; createCampaignFormIndex < control.value.length; createCampaignFormIndex++) {
                control.removeAt(createCampaignFormIndex);
              }
            }
            if (edd.groupDtos && edd.groupDtos.length > 0) {
              for (const ed of edd.groupDtos) {
                const controls = this.createCampaignForm.controls.groups as FormArray;
                controls.push(this.groupNm(ed.id, ed.groupName, ed.mitreName));
              }
            } else {
              control.push(this.groupNm('', '', ''));
            }
          }
        }
      } else {
        const control = this.createCampaignForm.controls.groups as FormArray;
        control.push(this.groupNm('', '', ''));
      }
    }
  }

  patchMitreDetails(event, type, createCampaignFormIndex) {
    if (event.groupName.value) {
      this.groupRes.filter((iocObj, j) => {
        if (event.groupName.value === iocObj.groupName) {
          const control = this.createCampaignForm.get(type)['controls'];
          if (control[createCampaignFormIndex].controls) {
            control[createCampaignFormIndex] = this.groupNm(iocObj.id, iocObj.groupName, iocObj.mitreName);
          }
        }
      });
    }
  }

  viewAttackCampaign(id) {
    this.router.navigate(['apt/viewCampaign'], {queryParams: {id: id, send: false, lastPage: 'Create Attack/Campaign'}});

  }


  onCancel() {
    this.router.navigate(['apt/attacksCampaignList']);
  }
}
