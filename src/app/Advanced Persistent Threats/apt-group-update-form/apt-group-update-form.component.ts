import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfirmationService, MenuItem} from "primeng/api";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {ToastrService} from "ngx-toastr";
import {NgxSpinnerService} from "ngx-spinner";
import {AptService} from '../../core-services/apt.service';
@Component({
  selector: 'app-apt-group-update-form',
  templateUrl: './apt-group-update-form.component.html',
  styleUrls: ['./apt-group-update-form.component.scss'],
  providers: [ConfirmationService]
})
export class AptGroupUpdateFormComponent implements OnInit {
  breadCrumbs: MenuItem[];
  itemList = [];
  selectedItems = [];
  selectedItemsS = [];
  settings : any;
  settingsSector : any;
  updateGroupForm: FormGroup;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  loading = false;
  submitted = false;
  hide = true;
  result: any;
  name: string;
  salary: any;
  types: any = [];
  caseName: any = [];
  caseNameResult: any = [];
  value: any;
  values = [];
  submitNextOne = false;
  submitNextThree = false;
  regionResult: any = [];
  sectorResult: any = [];
  regions: any = [];
  sectors: any = [];
  techniqueName: any = [];
  techniqueNameResult: any = [];
  techniqueId: any = [];
  techniqueIdResult: any = [];
  techniqueDetails: any = [];

  mitreInitialAccesstid: any = [];
  mitreInitialAccesstname: any = [];
  mitreExecutiontid: any = [];
  mitreExecutiontname: any = [];
  mitrePersistencetid: any = [];
  mitrePersistencetname: any = [];
  mitrePrivilegeEscalationtid: any = [];
  mitrePrivilegeEscalationtname: any = [];
  mitreDefenseEvasiontid: any = [];
  mitreDefenseEvasiontname: any = [];
  mitreCredentialAccesstid: any = [];
  mitreCredentialAccesstname: any = [];
  mitreDiscoverytid: any = [];
  mitreDiscoverytname: any = [];
  mitreLateralMovementtid: any = [];
  mitreLateralMovementname: any = [];
  mitreCollectiontid: any = [];
  mitreCollectiontname: any = [];
  mitreCommandAndControltid: any = [];
  mitreCommandAndControltname: any = [];
  mitreExfiltrationtid: any = [];
  mitreExfiltrationtname: any = [];
  mitreImpacttid: any = [];
  mitreImpacttname: any = [];
  mitreResourcetname: any = [];
  mitreResourcettid: any = [];
  mitreRecontid: any = [];
  mitreRecontname: any = [];
  alertId: any;
  details: any;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  selectable = true;
  removable = true;
  aliases: any = [];
  mitreIdArr: any = [];
  mitreValueArr: any = [];
  @ViewChild('aliasInput') aliasInput: ElementRef<HTMLInputElement>;
  groupData: any;
  caseId: any;
  isTrue = false;

  @ViewChild('file') uploadElRef: ElementRef;
  display = 'none';
  fileList: any;
  @ViewChild('file') file;
  filesToUpload: Set<File> = new Set();
  filee: File;
  file_loading = false;
  spin = false;
  respose: any;
  groupDetails: any;


  reconopen = false;
  resopen = false;
  iaopen = false;
  exeopen = false;
  persopen = false;
  privopen = false;
  defEVAopen = false;
  credAccopen = false;
  discopen = false;
  lateralopen = false;
  collecopen = false;
  cacopen = false;
  exifopen = false;
  impopen = false;
  constructor(
    private formBuilder: FormBuilder, private spinner: NgxSpinnerService,
    private ngZone: NgZone, private aptService: AptService, private toastr: ToastrService,
    private router: Router, private route: ActivatedRoute) {

    this.settings = {
      singleSelection: false,
      text: "Select Countries",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Regions and Countries',
      enableSearchFilter: true,
      badgeShowLimit: 5,
      groupBy: "category"
    };
    this.settingsSector = {
      singleSelection: false,
      text: "Select Sectors",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Sectors',
      enableSearchFilter: true,
      badgeShowLimit: 5,
    };
    this.getLookupServices();
    this.initReactiveForm();
    // redirect to home if already logged in

  }

  ngOnInit(): void {
    this.alertId = this.route.snapshot.queryParamMap.get('id');
    this.breadCrumbs = [
      {label: 'Group Details', routerLink: '/apt/viewGroup', queryParams: {id: this.alertId, send: false, lastPage: 'Update Group'}, routerLinkActiveOptions: true},
      {label: 'Update Group'},
    ];
    this.mitreInitialAccesstid = [];
    this.mitreInitialAccesstname = [];
    this.mitreExecutiontid = [];
    this.mitreExecutiontname = [];
    this.mitrePersistencetid = [];
    this.mitrePersistencetname = [];
    this.mitrePrivilegeEscalationtid = [];
    this.mitrePrivilegeEscalationtname = [];
    this.mitreDefenseEvasiontid = [];
    this.mitreDefenseEvasiontname = [];
    this.mitreCredentialAccesstid = [];
    this.mitreCredentialAccesstname = [];
    this.mitreDiscoverytid = [];
    this.mitreDiscoverytname = [];
    this.mitreLateralMovementtid = [];
    this.mitreLateralMovementname = [];
    this.mitreCollectiontid = [];
    this.mitreCollectiontname = [];
    this.mitreCommandAndControltid = [];
    this.mitreCommandAndControltname = [];
    this.mitreExfiltrationtid = [];
    this.mitreExfiltrationtname = [];
    this.mitreImpacttid = [];
    this.mitreImpacttname = [];
    this.mitreResourcetname = [];
    this.mitreResourcettid = [];
    this.mitreRecontid = [];
    this.mitreRecontname = [];
    this.techniqueDetails = [];
    this.initReactiveForm();
    this.getGroupData();
  }

  initReactiveForm() {
    this.updateGroupForm = this.formBuilder.group({
      groupName: ['', Validators.required],
      mitreName: [''],
      alias: [''],
      summary: [''],
      sectors: [''],
      regions: [''],
      filess: [''],
      mitreReconnaissance: this.formBuilder.array([]),
      mitreResourceDevelopment: this.formBuilder.array([]),
      mitreInitialAccess: this.formBuilder.array([]),
      mitreExecution: this.formBuilder.array([]),
      mitrePersistence: this.formBuilder.array([]),
      mitrePrivilegeEscalation: this.formBuilder.array([]),
      mitreDefenseEvasion: this.formBuilder.array([]),
      mitreCredentialAccess: this.formBuilder.array([]),
      mitreDiscovery: this.formBuilder.array([]),
      mitreLateralMovement: this.formBuilder.array([]),
      mitreCollection: this.formBuilder.array([]),
      mitreCommandAndControl: this.formBuilder.array([]),
      mitreExfiltration: this.formBuilder.array([]),
      mitreImpact: this.formBuilder.array([]),
      attackCampaigns: this.formBuilder.array([])
    });
  }
  onFilesAdded() {
    // for malware search
    this.spin = true;
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (const key in files) {
      if (!isNaN(parseInt(key, 10))) {
        this.filee = files[key];
        this.filesToUpload.add(files[key]);
      }
    }
    if (this.filesToUpload.size === 1) {
      this.uploadFile(this.filesToUpload);
    } else if (this.filesToUpload.size < 1) {
      this.toastr.error('You must select a file');
    } else {
      this.toastr.error('You can only select 1 file');
    }
  }

  public uploadFile(files: Set<File>): any {
    files.forEach(file => {
      const formData: FormData = new FormData();
      formData.append('file', file, file.name);
      this.file_loading = false;
      this.spinner.show('mitre-spinner');

      this.aptService.uploadGroupCV(this.alertId, formData).subscribe(
        res => {
          this.spin = false;
          this.spinner.hide('mitre-spinner');

          files.clear();
          this.filesToUpload.clear();
          this.respose = res;

          if (this.respose) {
            this.toastr.success(this.respose.responseDto.reponse);
            if (this.respose.rejectedTechniques && this.respose.rejectedTechniques.length > 0) {
              let erru = '';
              for (let tech of this.respose.rejectedTechniques) {
                erru = erru + ' ' + tech;
              }
              this.toastr.error('Rejected Techniques: \n' + erru);

            }
            if (this.respose.techniques && this.respose.techniques.length > 0) {
              this.file_loading = true;
              this.getGroupDetails(this.respose.techniques);
            } else {
              this.file_loading = false;
              this.spinner.hide('mitre-spinner');

              this.toastr.error('No MITRES in file.');
              this.uploadElRef.nativeElement.value = '';
              files.clear();
              this.filesToUpload.clear();
            }
          } else {
            this.toastr.error('Only CSV file is accepted Kindly Try again');
            this.uploadElRef.nativeElement.value = '';
            files.clear();
            this.filesToUpload.clear();
          }

        },
        error => {
          this.spin = false;

          let err = error.error;
          err = err.responseDto;
          this.file_loading = false;
          this.spinner.hide('mitre-spinner');

          this.uploadElRef.nativeElement.value = '';
          files.clear();
          this.filesToUpload.clear();
          this.toastr.error(err.response);
        });
    });
  }

  getGroupDetails(iocs) {
    this.groupDetails = iocs;
    let mitreInitialAccess: any = [];
    let mitreExecution: any = [];
    let mitrePersistence: any = [];
    let mitrePrivilegeEscalation: any = [];
    let mitreDefenseEvasion: any = [];
    let mitreCredentialAccess: any = [];
    let mitreDiscovery: any = [];
    let mitreLateralMovement: any = [];
    let mitreCollection: any = [];
    let mitreCommandAndControl: any = [];
    let mitreExfiltration: any = [];
    let mitreImpact: any = [];
    let mitreReconnaissance: any = [];
    let mitreResourceDevelopment: any = [];
    if (this.groupDetails && this.groupDetails.length > 0) {
      this.groupDetails.map(item => {
        if (item.technique.techniqueTypes && item.technique.techniqueTypes.length > 0) {
          if (this.techniqueDetails.find((test) => test.techniqueId === item.technique.techniqueId) === undefined) {
            this.techniqueDetails.push(item.technique);
          }
          item.technique.techniqueTypes.map(updateGroupIndex => {
            if (updateGroupIndex.id === 1) {
              mitreInitialAccess.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 1
                }
              });
            } else if (updateGroupIndex.id === 2) {
              mitreExecution.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 2
                }
              });
            } else if (updateGroupIndex.id === 3) {
              mitrePersistence.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 3
                }
              });
            } else if (updateGroupIndex.id === 4) {
              mitrePrivilegeEscalation.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 4
                }
              });
            } else if (updateGroupIndex.id === 5) {
              mitreDefenseEvasion.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 5
                }
              });
            } else if (updateGroupIndex.id === 6) {
              mitreCredentialAccess.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 6
                }
              });
            } else if (updateGroupIndex.id === 7) {
              mitreDiscovery.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 7
                }
              });
            } else if (updateGroupIndex.id === 8) {
              mitreLateralMovement.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 8
                }
              });
            } else if (updateGroupIndex.id === 9) {
              mitreCollection.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 9
                }
              });
            } else if (updateGroupIndex.id === 10) {
              mitreCommandAndControl.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 10
                }
              });
            } else if (updateGroupIndex.id === 11) {
              mitreExfiltration.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 11
                }
              });
            } else if (updateGroupIndex.id === 12) {
              mitreImpact.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 12
                }
              });
            }else if (updateGroupIndex.id === 13) {
              mitreReconnaissance.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 13
                }
              });
            }else if (updateGroupIndex.id === 14) {
              mitreResourceDevelopment.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 14
                }
              });
            }
          });
        }
      });
    }
    if (mitreInitialAccess && mitreInitialAccess.length > 0) {
      this.iaopen = true;
      this.getTechniqueByindx(2);
      const groupControl = this.updateGroupForm.controls.mitreInitialAccess as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreInitialAccess.length; updateGroupIndex++) {
        const getGroupObj = mitreInitialAccess[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
   if (mitreExecution && mitreExecution.length > 0) {
      this.exeopen = true;
      this.getTechniqueByindx(3);

      const groupControl = this.updateGroupForm.controls.mitreExecution as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreExecution.length; updateGroupIndex++) {
        const getGroupObj = mitreExecution[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
   if (mitrePersistence && mitrePersistence.length > 0) {
      this.persopen = true;
      this.getTechniqueByindx(4);

      const groupControl = this.updateGroupForm.controls.mitrePersistence as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitrePersistence.length; updateGroupIndex++) {
        const getGroupObj = mitrePersistence[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
   if (mitrePrivilegeEscalation && mitrePrivilegeEscalation.length > 0) {
      this.privopen = true;
      this.getTechniqueByindx(5);
      const groupControl = this.updateGroupForm.controls.mitrePrivilegeEscalation as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitrePrivilegeEscalation.length; updateGroupIndex++) {
        const getGroupObj = mitrePrivilegeEscalation[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
   if (mitreDefenseEvasion && mitreDefenseEvasion.length > 0) {
      this.defEVAopen = true;
      this.getTechniqueByindx(6);

      const groupControl = this.updateGroupForm.controls.mitreDefenseEvasion as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreDefenseEvasion.length; updateGroupIndex++) {
        const getGroupObj = mitreDefenseEvasion[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
    if (mitreCredentialAccess && mitreCredentialAccess.length > 0) {
      this.credAccopen = true;

      this.getTechniqueByindx(7);
      const groupControl = this.updateGroupForm.controls.mitreCredentialAccess as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreCredentialAccess.length; updateGroupIndex++) {
        const getGroupObj = mitreCredentialAccess[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
   if (mitreDiscovery && mitreDiscovery.length > 0) {
      this.discopen = true;
      this.getTechniqueByindx(8);

      const groupControl = this.updateGroupForm.controls.mitreDiscovery as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreDiscovery.length; updateGroupIndex++) {
        const getGroupObj = mitreDiscovery[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
  if (mitreLateralMovement && mitreLateralMovement.length > 0) {
      this.lateralopen = true;
      this.getTechniqueByindx(9);

      const groupControl = this.updateGroupForm.controls.mitreLateralMovement as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreLateralMovement.length; updateGroupIndex++) {
        const getGroupObj = mitreLateralMovement[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
  if (mitreCollection && mitreCollection.length > 0) {
      this.collecopen = true;
      this.getTechniqueByindx(10);

      const groupControl = this.updateGroupForm.controls.mitreCollection as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreCollection.length; updateGroupIndex++) {
        const getGroupObj = mitreCollection[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
   if (mitreCommandAndControl && mitreCommandAndControl.length > 0) {
      this.cacopen = true;
      this.getTechniqueByindx(11);

      const groupControl = this.updateGroupForm.controls.mitreCommandAndControl as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreCommandAndControl.length; updateGroupIndex++) {
        const getGroupObj = mitreCommandAndControl[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
  if (mitreExfiltration && mitreExfiltration.length > 0) {
      this.exifopen = true;
      this.getTechniqueByindx(12);

      const groupControl = this.updateGroupForm.controls.mitreExfiltration as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreExfiltration.length; updateGroupIndex++) {
        const getGroupObj = mitreExfiltration[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
  if (mitreReconnaissance && mitreReconnaissance.length > 0) {
      this.reconopen = true;
      this.getTechniqueByindx(0);

      const groupControl = this.updateGroupForm.controls.mitreReconnaissance as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreReconnaissance.length; updateGroupIndex++) {
        const getGroupObj = mitreReconnaissance[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
   if (mitreResourceDevelopment && mitreResourceDevelopment.length > 0) {
      this.resopen = true;
      this.getTechniqueByindx(1);

      const groupControl = this.updateGroupForm.controls.mitreResourceDevelopment as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreResourceDevelopment.length; updateGroupIndex++) {
        const getGroupObj = mitreResourceDevelopment[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
   if (mitreImpact && mitreImpact.length > 0) {
      this.impopen = true;
      this.getTechniqueByindx(13);

      const groupControl = this.updateGroupForm.controls.mitreImpact as FormArray;
      let count = groupControl.length;
      if (count > 1) {
        groupControl.removeAt(count);
        groupControl[count] = [];
      } else {
        groupControl.removeAt(0);
        groupControl[0] = [];

      }
      for (let updateGroupIndex = 0; updateGroupIndex < mitreImpact.length; updateGroupIndex++) {
        const getGroupObj = mitreImpact[updateGroupIndex];
        groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, false));
      }
    }
  }
  viewGroups(id) {
    this.router.navigate(['apt/viewGroup'], {queryParams: {id: id, send: false, lastPage: 'Update Group'}});

  }

  // convenience getter for easy access to form fields
  get errorControl() {
    return this.updateGroupForm.controls;
  }


  /*--------------Submit form------------------*/
  onSubmit() {
    let mitreReconnaissance = this.updateGroupForm.value.mitreReconnaissance.map(item => {
      if ( item.name !== "" && item.techniqueId !== "" && item.name !== undefined && item.techniqueId !== undefined) {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 13
          }
        };
      }
    });
    let mitreResourceDevelopment = this.updateGroupForm.value.mitreResourceDevelopment.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 14
          }
        };
      }
    });
    let mitreInitial = this.updateGroupForm.value.mitreInitialAccess.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 1
          }
        };
      }
    });
    let mitreExecution = this.updateGroupForm.value.mitreExecution.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 2
          }
        };
      }
    });
    let mitrePersistence = this.updateGroupForm.value.mitrePersistence.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 3
          }
        };
      }
    });
    let mitrePrivilegeEscalation = this.updateGroupForm.value.mitrePrivilegeEscalation.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 4
          }
        };
      }
    });
    let mitreDefenseEvasion = this.updateGroupForm.value.mitreDefenseEvasion.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 5
          }
        };
      }
    });
    let mitreCredentialAccess = this.updateGroupForm.value.mitreCredentialAccess.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 6
          }
        };
      }
    });
    let mitreDiscovery = this.updateGroupForm.value.mitreDiscovery.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 7
          }
        };
      }
    });
    let mitreLateralMovement = this.updateGroupForm.value.mitreLateralMovement.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 8
          }
        };
      }
    });
    let mitreCollection = this.updateGroupForm.value.mitreCollection.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 12
          }
        };
      }
    });
    let mitreCommandAndControl = this.updateGroupForm.value.mitreCommandAndControl.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 9
          }
        };
      }
    });
    let mitreExfiltration: any = this.updateGroupForm.value.mitreExfiltration.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 10
          }
        };
      }
    });
    let mitreImpact: any = this.updateGroupForm.value.mitreImpact.map(item => {
      if ( item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 11
          }
        };
      }
    });
    let newArray = [];
    this.mitreIdArr = [];
    this.mitreValueArr = [];
    if (mitreImpact.length === 1 && (mitreImpact[0] === null || mitreImpact[0] === undefined)) {
      mitreImpact = [];
    } else {
      newArray.push(...mitreImpact);
    }
    if (mitreExfiltration.length === 1 && (mitreExfiltration[0] === null || mitreExfiltration[0] === undefined)) {
      mitreExfiltration = [];
    } else {
      newArray.push(...mitreExfiltration);
    }
    if (mitreCommandAndControl.length === 1 && (mitreCommandAndControl[0] === null || mitreCommandAndControl[0] === undefined)) {
      mitreCommandAndControl = [];
    } else {
      newArray.push(...mitreCommandAndControl);
    }
    if (mitreCollection.length === 1 && (mitreCollection[0] === null || mitreCollection[0] === undefined)) {
      mitreCollection = [];
    } else {
      newArray.push(...mitreCollection);
    }
    if (mitreExecution.length === 1 && (mitreExecution[0] === null || mitreExecution[0] === undefined)) {
      mitreExecution = [];
    } else {
      newArray.push(...mitreExecution);
    }
    if (mitreLateralMovement.length === 1 && (mitreLateralMovement[0] === null || mitreLateralMovement[0] === undefined)) {
      mitreLateralMovement = [];
    } else {
      newArray.push(...mitreLateralMovement);
    }
    if (mitreDiscovery.length === 1 && (mitreDiscovery[0] === null || mitreDiscovery[0] === undefined)) {
      mitreDiscovery = [];
    } else {
      newArray.push(...mitreDiscovery);
    }
    if (mitreCredentialAccess.length === 1 && (mitreCredentialAccess[0] === null || mitreCredentialAccess[0] === undefined)) {
      mitreCredentialAccess = [];
    } else {
      newArray.push(...mitreCredentialAccess);
    }
    if (mitreDefenseEvasion.length === 1 && (mitreDefenseEvasion[0] === null || mitreDefenseEvasion[0] === undefined)) {
      mitreDefenseEvasion = [];
    } else {
      newArray.push(...mitreDefenseEvasion);
    }
    if (mitrePrivilegeEscalation.length === 1 && (mitrePrivilegeEscalation[0] === null || mitrePrivilegeEscalation[0] === undefined)) {
      mitrePrivilegeEscalation = [];
    } else {
      newArray.push(...mitrePrivilegeEscalation);
    }
    if (mitrePersistence.length === 1 && (mitrePersistence[0] === null || mitrePersistence[0] === undefined)) {
      mitrePersistence = [];
    } else {
      newArray.push(...mitrePersistence);
    }
    if (mitreInitial.length === 1 && (mitreInitial[0] === null || mitreInitial[0] === undefined)) {
      mitreInitial = [];
    } else {
      newArray.push(...mitreInitial);
    }
    if (mitreReconnaissance.length === 1 && (mitreReconnaissance[0] === null || mitreReconnaissance[0] === undefined)) {
      mitreReconnaissance = [];
    } else {
      newArray.push(...mitreReconnaissance);
    }
    if (mitreResourceDevelopment.length === 1 && (mitreResourceDevelopment[0] === null || mitreResourceDevelopment[0] === undefined)) {
      mitreResourceDevelopment = [];
    } else {
      newArray.push(...mitreResourceDevelopment);
    }
    this.mitreValueArr = [];
    this.mitreIdArr = [];
    if (this.techniqueDetails && this.techniqueDetails.length > 0) {

      this.techniqueDetails.map((groupFormObj, updateGroupIndex) => {
        const idArr: any = [];
        newArray.map((x, j) => {
          if (x === undefined) {
            newArray.splice(j, 1);
          } else {
            if (groupFormObj && groupFormObj.id !== '' && x.id !== '' && groupFormObj.id === x.id && groupFormObj.id !== undefined && x.id !== undefined) {
              idArr.push({id: x.techniqueTypes.id});
              this.mitreIdArr.push({
                description: x.description,
                name: x.name,
                id: x.id,
                techniqueId: x.techniqueId,
                techniqueTypes: idArr
              });
            }
            else if (x.id === '') {
              this.mitreValueArr.push({
                description: x.description,
                name: x.name,
                techniqueId: x.techniqueId,
                techniqueTypes: [{id: x.techniqueTypes.id}]
              });
            }
          }

        });
      });
    } else {
      newArray.map(x => {
        this.mitreValueArr.push({
          description: x.description,
          name: x.name,
          techniqueId: x.techniqueId,
          techniqueTypes: [{id: x.techniqueTypes.id}]
        });
      });
    }

    const techniquesArr = [];
    if (this.mitreValueArr && this.mitreValueArr.length > 0) {
      const dataArray = this.mitreValueArr.filter((item, updateGroupIndex, arr) =>
        arr.findIndex((x) => (x.techniqueId === item.techniqueId)) === updateGroupIndex);
      techniquesArr.push(...dataArray);
    }
    if (this.mitreIdArr && this.mitreIdArr.length > 0) {
      const newd = [];
      const dataArray = this.mitreIdArr.filter((item, updateGroupIndex, arr) =>
        arr.findIndex((x) => (x.techniqueId === item.techniqueId)) === updateGroupIndex);
      dataArray.map(item => item).filter(updateGroupIndex => {
        const updateArr = updateGroupIndex.techniqueTypes.map(updateGroupIndex => updateGroupIndex.id).filter((value, index, self) => self.indexOf(value) === index);
        const data = {
          description: updateGroupIndex.description,
          name: updateGroupIndex.name,
          id: updateGroupIndex.id,
          techniqueId: updateGroupIndex.techniqueId,
          techniqueTypes: updateArr.map(j => {
            return {id: j};
          })
        };
        newd.push(data);
      });
      techniquesArr.push(...newd);
    }
    const attackCampaigns: any = [];
      this.updateGroupForm.value.attackCampaigns.filter(item => {
      if (item && item.id !== "" && item.associatedIndicators !== "" && item.caseName !== "") {
        this.isTrue = false;
        let sectors = [];
        let regions = [];
        if ((item.associatedIndicators !== "" && item.caseName !== "")) {
          this.groupData.map(updateGroupIndex => {
            if (item.caseName === updateGroupIndex.caseName) {
              if (updateGroupIndex.caseName) {
                this.isTrue = true;
                this.caseId = updateGroupIndex.id;
              }
            }
          });
          if (item.sectors && item.sectors.length > 0) { sectors = item.sectors; }
          if (item.regions && item.regions.length > 0 ) { regions = item.regions; }
          if (this.isTrue) {
            attackCampaigns.push({
              id: this.caseId,
              associatedIndicators: item.associatedIndicators,
              caseName: item.caseName,
              countries: regions,
              sectors: sectors
            });
          } else {
            attackCampaigns.push({
              associatedIndicators: item.associatedIndicators,
              caseName: item.caseName,
              countries: regions,
              sectors: sectors
            });
          }
        }
      }
    });
    if (this.updateGroupForm.value.regions === '') {
      this.updateGroupForm.value.regions = [];
    }
    if (this.updateGroupForm.value.sectors === '') {
      this.updateGroupForm.value.sectors = [];
    }
    const dataJson = {
      aliases: this.aliases,
      groupName: this.updateGroupForm.value.groupName,
      mitreName: this.updateGroupForm.value.mitreName,
      summary: this.updateGroupForm.value.summary,
      attackCampaigns: attackCampaigns,
      techniques: techniquesArr,
      countries: this.updateGroupForm.value.regions,
      sectors: this.updateGroupForm.value.sectors,
      id: this.alertId
    };
    this.spinner.show('updategroup-spinner');

    this.aptService.updateGroups(dataJson).subscribe(
      res => {
        const response: any = res;
        this.spinner.hide('updategroup-spinner');

        if (response.responseDto.responseCode === 201 || response.responseDto.responseCode === 200) {
          this.toastr.success(response.responseDto.response);
          this.viewGroups(response.id);
        } else {
          this.toastr.error('Some error occurred while creating');
        }
      },
      error => {
        this.spinner.hide('updategroup-spinner');
        this.toastr.error('Some error occurred while creating');
      });
  }

  /*-----------------------------------------------------------------------------*/

  /*--------------Form Array Fields - Initialize Form Array - Add item to form array - Remove item from form array------------------*/
  mitreInitial(techId, name, description, id): FormGroup {
    return this.formBuilder.group({
      techniqueId: [techId],
      name: [name, Validators.required],
      description: [description],
      id: [id],
      isdisable: [false]

    });
  }

  patchMitreInitial(techniqueId, name, id, description, disablename): FormGroup {
    return this.formBuilder.group({
      techniqueId: [techniqueId],
      name: [name, Validators.required],
      id: [id],
      description: [description],
      isdisable: [disablename]
    });
  }

  attackCampaigns(ind): FormGroup {
    return this.formBuilder.group({
      associatedIndicators: [ind],
      caseName: ['', Validators.required],
      regions: [''],
      sectors: [''],
    });
  }
  pattackCampaigns(id, ind, caseName, countries, sectors): FormGroup {
    return this.formBuilder.group({
      associatedIndicators: [ind],
      caseName: [caseName, Validators.required],
      regions: [countries],
      sectors: [sectors],
      id: [id]

    });
  }
  patchIndiactorDetails(event, type, updateGroupIndex) {
    const sector = [];
    const countries = [];
    if (event.caseName.value) {
      this.groupData.filter((groupFormObj, j) => {
        if (event.caseName.value === groupFormObj.caseName) {
          const groupControl = this.updateGroupForm.get(type)['controls'];
          if (groupControl[updateGroupIndex].controls) {
            if (groupFormObj.sectors && groupFormObj.sectors.length > 0) {
              for (let sec of groupFormObj.sectors) {
                const data = {
                  id: sec.id,
                  itemName: sec.value,
                };
                sector.push(data);

              }
              groupFormObj.sectors = sector;

            }
            if (groupFormObj.countries && groupFormObj.countries.length > 0) {
              for (let region of groupFormObj.countries) {
                const data = {
                  id: region.id,
                  itemName: region.countryName,
                  category: region.region
                };
                countries.push(data);
              }
              groupFormObj.countries = countries;
            }
            groupControl[updateGroupIndex] = this.pattackCampaigns(groupFormObj.id, groupFormObj.indicatorCount, groupFormObj.caseName, countries, sector );
          }
        }
      });
    }
  }

  patchAttackCampaigns(id, caseName, associatedIndicators, regions, sectors): FormGroup {
    return this.formBuilder.group({
      associatedIndicators: [associatedIndicators],
      caseName: [caseName, Validators.required],
      regions: [regions],
      sectors: [sectors],
      id: [id]
    });
  }

  addAttackCampaigns() {
    const groupControl = this.updateGroupForm.controls.attackCampaigns as FormArray;
    groupControl.push(this.attackCampaigns(0));
  }


  addMitreInitial(value) {
    if (value === 'Reconnaissance') {
      const groupControl = this.updateGroupForm.controls.mitreReconnaissance as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Resource Development') {
      const groupControl = this.updateGroupForm.controls.mitreResourceDevelopment as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Initial Access') {
      const groupControl = this.updateGroupForm.controls.mitreInitialAccess as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Execution') {
      const groupControl = this.updateGroupForm.controls.mitreExecution as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Persistence') {
      const groupControl = this.updateGroupForm.controls.mitrePersistence as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Privilege Escalation') {
      const groupControl = this.updateGroupForm.controls.mitrePrivilegeEscalation as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Defense Evasion') {
      const groupControl = this.updateGroupForm.controls.mitreDefenseEvasion as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Credential Access') {
      const groupControl = this.updateGroupForm.controls.mitreCredentialAccess as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Discovery') {
      const groupControl = this.updateGroupForm.controls.mitreDiscovery as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Lateral Movement') {
      const groupControl = this.updateGroupForm.controls.mitreLateralMovement as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Collection') {
      const groupControl = this.updateGroupForm.controls.mitreCollection as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Command and groupControl') {
      const groupControl = this.updateGroupForm.controls.mitreCommandAndControl as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Exfiltration') {
      const groupControl = this.updateGroupForm.controls.mitreExfiltration as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Impact') {
      const groupControl = this.updateGroupForm.controls.mitreImpact as FormArray;
      groupControl.push(this.mitreInitial('', '', '', ''));
    }
  }

  removeMitreInitial(updateGroupIndex: number, value) {
    if (value === 'Reconnaissance') {
      const groupControl = this.updateGroupForm.controls.mitreReconnaissance as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Resource Development') {
      const groupControl = this.updateGroupForm.controls.mitreResourceDevelopment as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Initial Access') {
      const groupControl = this.updateGroupForm.controls.mitreInitialAccess as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Execution') {
      const groupControl = this.updateGroupForm.controls.mitreExecution as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Persistence') {
      const groupControl = this.updateGroupForm.controls.mitrePersistence as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Privilege Escalation') {
      const groupControl = this.updateGroupForm.controls.mitrePrivilegeEscalation as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Defense Evasion') {
      const groupControl = this.updateGroupForm.controls.mitreDefenseEvasion as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Credential Access') {
      const groupControl = this.updateGroupForm.controls.mitreCredentialAccess as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Discovery') {
      const groupControl = this.updateGroupForm.controls.mitreDiscovery as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Lateral Movement') {
      const groupControl = this.updateGroupForm.controls.mitreLateralMovement as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Collection') {
      const groupControl = this.updateGroupForm.controls.mitreCollection as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Command and groupControl') {
      const groupControl = this.updateGroupForm.controls.mitreCommandAndControl as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Exfiltration') {
      const groupControl = this.updateGroupForm.controls.mitreExfiltration as FormArray;
      groupControl.removeAt(updateGroupIndex);
    } else if (value === 'Impact') {
      const groupControl = this.updateGroupForm.controls.mitreImpact as FormArray;
      groupControl.removeAt(updateGroupIndex);
    }
  }

  removeAttackCampaigns(updateGroupIndex: number) {
    const groupControl = this.updateGroupForm.controls.attackCampaigns as FormArray;
    groupControl.removeAt(updateGroupIndex);
  }
  // in case of zero
  removeAttackCampaignsZ(updateGroupIndex: number) {
    const groupControl = this.updateGroupForm.controls.attackCampaigns as FormArray;
    if (groupControl.length <= 1) {
      const groupControl = this.updateGroupForm.get('attackCampaigns')['controls'];
      groupControl[updateGroupIndex]['controls'].associatedIndicators.patchValue('');
      groupControl[updateGroupIndex]['controls'].caseName.patchValue('');
      groupControl[updateGroupIndex]['controls'].caseId.patchValue('');
      groupControl[updateGroupIndex]['controls'].regions.patchValue('');
      groupControl[updateGroupIndex]['controls'].sectors.patchValue('');
      groupControl[updateGroupIndex]['controls'].associatedIndicators.updateValueAndValidity();
      groupControl[updateGroupIndex]['controls'].caseName.updateValueAndValidity();
      groupControl[updateGroupIndex]['controls'].caseId.updateValueAndValidity();
      groupControl[updateGroupIndex]['controls'].regions.updateValueAndValidity();
      groupControl[updateGroupIndex]['controls'].sectors.updateValueAndValidity();
    } else if (groupControl.length > 1) {
      groupControl.removeAt(updateGroupIndex);

    }
  }
  removeMitreInitialZ(updateGroupIndex: number, type) {
    const groupControl = this.updateGroupForm.controls[type] as FormArray;
    if (groupControl.length <= 1) {
      const groupControl = this.updateGroupForm.get(type)['controls'];
      groupControl[updateGroupIndex]['controls'].techniqueId.patchValue('');
      groupControl[updateGroupIndex]['controls'].name.patchValue('');
      groupControl[updateGroupIndex]['controls'].description.patchValue('');
      groupControl[updateGroupIndex]['controls'].id.patchValue('');
      groupControl[updateGroupIndex]['controls'].techniqueId.updateValueAndValidity();
      groupControl[updateGroupIndex]['controls'].name.updateValueAndValidity();
      groupControl[updateGroupIndex]['controls'].description.updateValueAndValidity();
      groupControl[updateGroupIndex]['controls'].id.updateValueAndValidity();

    } else if (groupControl.length > 1) {
      groupControl.removeAt(updateGroupIndex);

    }
  }

  add(event: any): void {
    const input = event.input;
    let value = event.value;

    if ((value || '').trim()) {
      value = value.trim();
      if (this.aliases.find((test) => test.value.toLowerCase() === value.toLowerCase()) === undefined) {
        this.aliases.push({value: value.trim()});
        this.updateGroupForm.value.alias = this.aliases;
      }
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.aliasInput = null;
  }

  remove(fruit: string): void {
    const index = this.aliases.indexOf(fruit);

    if (index >= 0) {
      this.aliases.splice(index, 1);
      this.updateGroupForm.value.alias = this.aliases;
    }
  }

  getGroupData() {
    this.spinner.show('updategroup-spinner');
    this.reconopen = false;
    this.resopen = false;
    this.iaopen = false;
    this.exeopen = false;
    this.persopen = false;
    this.privopen = false;
    this.defEVAopen = false;
    this.credAccopen = false;
    this.discopen = false;
    this.lateralopen = false;
    this.collecopen = false;
    this.cacopen = false;
    this.exifopen = false;
    this.impopen = false;
    this.aptService.getGroups(this.alertId).subscribe(
      res => {
        this.details = res;
        this.spinner.hide('updategroup-spinner');

        if (this.details && this.details.responseDto) {
          if (this.details.responseDto.responseCode && this.details.responseDto.responseCode === Number(200)) {
            this.details = res;
            const forAlias = [];
            if (this.details.aliases && this.details.aliases.length > 0) {
              for (let tag of this.details.aliases) {
                forAlias.push(tag.value);
              }
              this.aliases = this.details.aliases;
            }
            this.aptService.getRegions().subscribe(
              res => {
                const result: any = res;
                this.selectedItems = [];
                if (result && result.length > 0) {
                  this.regionResult = [];
                  for (let region of result) {
                    const data = {
                      id: region.id,
                      itemName: region.countryName,
                      category: region.region
                    };
                    this.itemList.push(data);
                    this.regionResult.push(data);

                  }

                }else {
                  this.regionResult = [];
                }
                if (this.details.countries && this.details.countries.length > 0) {
                  for (let region of this.details.countries) {
                    const data = {
                      id: region.id,
                      itemName: region.countryName,
                      category: region.region
                    };
                    this.selectedItems.push(data);

                  }
                  this.details.countries = this.selectedItems;
                }
              },
              err => {
              }
            );
            this.aptService.getSectors().subscribe(
              res => {
                const result: any = res;
                this.selectedItemsS = [];
                this.sectorResult = [];
                if (result && result.length > 0) {
                  for (let sec of result) {
                    const data = {
                      id: sec.id,
                      itemName: sec.value,
                    };
                    this.sectorResult.push(data);

                  }

                }else {
                  this.sectorResult = [];
                }
                if (this.details.sectors && this.details.sectors.length > 0) {
                  for (let sector of this.details.sectors) {
                    const data = {
                      id: sector.id,
                      itemName: sector.value,
                    };
                    this.selectedItemsS.push(data);
                  }

                  this.details.sectors = this.selectedItemsS;

                }
              },
              err => {
              }
            );
            this.updateGroupForm.patchValue({
              groupName: this.details.groupName,
              mitreName: this.details.mitreName,
              summary: this.details.summary,
              sectors: this.details.sectors,
              alias: forAlias,
              regions: this.details.countries
            });

            if (this.details.attackCampaigns && this.details.attackCampaigns.length > 0) {
              for (let updateGroupIndex = 0; updateGroupIndex < this.details.attackCampaigns.length; updateGroupIndex++) {
                const sector = [];
                const countries = [];
                const groupControl = this.updateGroupForm.controls.attackCampaigns as FormArray;
                const getGroupObj = this.details.attackCampaigns[updateGroupIndex];
                if (getGroupObj.sectors && getGroupObj.sectors.length > 0) {
                  for (let sec of getGroupObj.sectors) {
                    const data = {
                      id: sec.id,
                      itemName: sec.value,
                    };
                    sector.push(data);

                  }
                  getGroupObj.sectors = sector;

                }
                if (getGroupObj.countries && getGroupObj.countries.length > 0) {
                  for (let region of getGroupObj.countries) {
                    const data = {
                      id: region.id,
                      itemName: region.countryName,
                      category: region.region
                    };
                    countries.push(data);
                  }
                  getGroupObj.countries = countries;
                }
                groupControl.push(this.patchAttackCampaigns(getGroupObj.id, getGroupObj.caseName, getGroupObj.associatedIndicators, countries,
                  sector));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.attackCampaigns as FormArray;
              groupControl.push(this.patchAttackCampaigns('', '', '', '', ''));
            }
            const mitreInitialAccess: any = [];
            const mitreExecution: any = [];
            const mitrePersistence: any = [];
            const mitrePrivilegeEscalation: any = [];
            const mitreDefenseEvasion: any = [];
            const mitreCredentialAccess: any = [];
            const mitreDiscovery: any = [];
            const mitreLateralMovement: any = [];
            const mitreCollection: any = [];
            const mitreCommandAndControl: any = [];
            const mitreExfiltration: any = [];
            const mitreImpact: any = [];
            const mitreReconnaissance: any = [];
            const mitreResourceDevelopment: any = [];
            if (this.details.techniques && this.details.techniques.length > 0) {
              this.details.techniques.map(item => {
                if (item.techniqueTypes && item.techniqueTypes.length > 0) {
                  if (this.techniqueDetails.find((test) => test.techniqueId === item.techniqueId) === undefined) {
                    this.techniqueDetails.push(item);
                  }
                  item.techniqueTypes.map(updateGroupIndex => {
                    if (updateGroupIndex.id === 1) {
                      mitreInitialAccess.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 1
                        }
                      });
                    } else if (updateGroupIndex.id === 2) {
                      mitreExecution.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 2
                        }
                      });
                    } else if (updateGroupIndex.id === 3) {
                      mitrePersistence.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 3
                        }
                      });
                    } else if (updateGroupIndex.id === 4) {
                      mitrePrivilegeEscalation.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 4
                        }
                      });
                    } else if (updateGroupIndex.id === 5) {
                      mitreDefenseEvasion.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 5
                        }
                      });
                    } else if (updateGroupIndex.id === 6) {
                      mitreCredentialAccess.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 6
                        }
                      });
                    } else if (updateGroupIndex.id === 7) {
                      mitreDiscovery.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 7
                        }
                      });
                    } else if (updateGroupIndex.id === 8) {
                      mitreLateralMovement.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 8
                        }
                      });
                    } else if (updateGroupIndex.id === 9) {
                      mitreCollection.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 9
                        }
                      });
                    } else if (updateGroupIndex.id === 10) {
                      mitreCommandAndControl.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 10
                        }
                      });
                    } else if (updateGroupIndex.id === 11) {
                      mitreExfiltration.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 11
                        }
                      });
                    } else if (updateGroupIndex.id === 12) {
                      mitreImpact.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 12
                        }
                      });
                    }else if (updateGroupIndex.id === 13) {
                      mitreReconnaissance.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 13
                        }
                      });
                    }else if (updateGroupIndex.id === 14) {
                      mitreResourceDevelopment.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 14
                        }
                      });
                    }
                  });
                }
              });
            }
            if (mitreInitialAccess && mitreInitialAccess.length > 0) {
              this.iaopen = true;
              this.getTechniqueByindx(2);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreInitialAccess.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreInitialAccess as FormArray;
                const getGroupObj = mitreInitialAccess[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreInitialAccess as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreExecution && mitreExecution.length > 0) {
              this.exeopen = true;
              this.getTechniqueByindx(3);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreExecution.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreExecution as FormArray;
                const getGroupObj = mitreExecution[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreExecution as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitrePersistence && mitrePersistence.length > 0) {
              this.persopen = true;
              this.getTechniqueByindx(4);

              for (let updateGroupIndex = 0; updateGroupIndex < mitrePersistence.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitrePersistence as FormArray;
                const getGroupObj = mitrePersistence[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitrePersistence as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitrePrivilegeEscalation && mitrePrivilegeEscalation.length > 0) {
              this.privopen = true;
              this.getTechniqueByindx(5);

              for (let updateGroupIndex = 0; updateGroupIndex < mitrePrivilegeEscalation.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitrePrivilegeEscalation as FormArray;
                const getGroupObj = mitrePrivilegeEscalation[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitrePrivilegeEscalation as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreDefenseEvasion && mitreDefenseEvasion.length > 0) {
              this.defEVAopen = true;
              this.getTechniqueByindx(6);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreDefenseEvasion.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreDefenseEvasion as FormArray;
                const getGroupObj = mitreDefenseEvasion[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreDefenseEvasion as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreCredentialAccess && mitreCredentialAccess.length > 0) {
              this.credAccopen = true;
              this.getTechniqueByindx(7);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreCredentialAccess.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreCredentialAccess as FormArray;
                const getGroupObj = mitreCredentialAccess[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreCredentialAccess as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreDiscovery && mitreDiscovery.length > 0) {
              this.discopen = true;
              this.getTechniqueByindx(8);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreDiscovery.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreDiscovery as FormArray;
                const getGroupObj = mitreDiscovery[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreDiscovery as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreLateralMovement && mitreLateralMovement.length > 0) {
              this.lateralopen = true;
              this.getTechniqueByindx(9);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreLateralMovement.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreLateralMovement as FormArray;
                const getGroupObj = mitreLateralMovement[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreLateralMovement as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreCollection && mitreCollection.length > 0) {
              this.collecopen = true;
              this.getTechniqueByindx(10);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreCollection.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreCollection as FormArray;
                const getGroupObj = mitreCollection[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreCollection as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreCommandAndControl && mitreCommandAndControl.length > 0) {
              this.cacopen = true;
              this.getTechniqueByindx(11);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreCommandAndControl.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreCommandAndControl as FormArray;
                const getGroupObj = mitreCommandAndControl[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreCommandAndControl as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreExfiltration && mitreExfiltration.length > 0) {
              this.exifopen = true;
              this.getTechniqueByindx(12);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreExfiltration.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreExfiltration as FormArray;
                const getGroupObj = mitreExfiltration[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreExfiltration as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreReconnaissance && mitreReconnaissance.length > 0) {
              this.reconopen = true;
              this.getTechniqueByindx(0);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreReconnaissance.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreReconnaissance as FormArray;
                const getGroupObj = mitreReconnaissance[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreReconnaissance as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreResourceDevelopment && mitreResourceDevelopment.length > 0) {
              this.resopen = true;
              this.getTechniqueByindx(1);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreResourceDevelopment.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreResourceDevelopment as FormArray;
                const getGroupObj = mitreResourceDevelopment[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreResourceDevelopment as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }

            if (mitreImpact && mitreImpact.length > 0) {
              this.impopen = true;
              this.getTechniqueByindx(13);

              for (let updateGroupIndex = 0; updateGroupIndex < mitreImpact.length; updateGroupIndex++) {
                const groupControl = this.updateGroupForm.controls.mitreImpact as FormArray;
                const getGroupObj = mitreImpact[updateGroupIndex];
                groupControl.push(this.patchMitreInitial(getGroupObj.techniqueId, getGroupObj.name, getGroupObj.id, getGroupObj.description, true));
              }
            } else {
              const groupControl = this.updateGroupForm.controls.mitreImpact as FormArray;
              groupControl.push(this.patchMitreInitial('', '', '', '', false));
            }
          }
        }
      }, err => {
          this.spinner.hide('updategroup-spinner');
          this.toastr.error('Some error occurred Please try again');

      }
    );
  }
  searchTechniqueById(event, index): void {
    index  = Number(index);
    if (index === 0) {
      this.techniqueIdResult = this.mitreRecontid.filter(groupFormObj => groupFormObj.startsWith(event.query));

    }
    else if (index === 1) {
      this.techniqueIdResult = this.mitreResourcettid.filter(groupFormObj => groupFormObj.startsWith(event.query));

    }
    else if (index === 2) {
      this.techniqueIdResult = this.mitreInitialAccesstid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 3) {
      this.techniqueIdResult = this.mitreExecutiontid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 4) {
      this.techniqueIdResult = this.mitrePersistencetid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 5) {
      this.techniqueIdResult = this.mitrePrivilegeEscalationtid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 6) {
      this.techniqueIdResult = this.mitreDefenseEvasiontid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 7) {
      this.mitreCredentialAccesstid = this.techniqueId;
      this.techniqueIdResult = this.techniqueId.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 8) {
      this.techniqueIdResult = this.mitreDiscoverytid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 9) {
      this.techniqueIdResult = this.mitreLateralMovementtid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 10) {
      this.techniqueIdResult = this.mitreCollectiontid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 11) {
      this.techniqueIdResult = this.mitreCommandAndControltid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 12) {
      this.techniqueIdResult = this.mitreExfiltrationtid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
    else if (index === 13) {
      this.techniqueIdResult = this.mitreImpacttid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
  }

  patchMitreDetails(event, type, updateGroupIndex, category) {
    let tech;
    if (category === 'id') {
      tech = event.techniqueId.value;
    } else {
      tech = event.name.value;
    }
    this.techniqueDetails.filter((groupFormObj, j) => {
      if (tech) {
        if (event.techniqueId.value === groupFormObj.techniqueId || event.name.value === groupFormObj.name) {
          const groupControl = this.updateGroupForm.get(type)['controls'];
          if (groupControl[updateGroupIndex].controls) {
            groupControl[updateGroupIndex]['controls'].name.patchValue(groupFormObj.name);
            groupControl[updateGroupIndex]['controls'].id.patchValue(groupFormObj.id);
            groupControl[updateGroupIndex]['controls'].id.updateValueAndValidity();
            groupControl[updateGroupIndex]['controls'].name.updateValueAndValidity();
          }
        }
      }
      else {
        const groupControl = this.updateGroupForm.get(type)['controls'];
        if (groupControl[updateGroupIndex].controls) {
          groupControl[updateGroupIndex]['controls'].name.patchValue('');
          groupControl[updateGroupIndex]['controls'].id.patchValue('');
          groupControl[updateGroupIndex]['controls'].id.updateValueAndValidity();
          groupControl[updateGroupIndex]['controls'].name.updateValueAndValidity();
        }
      }
    });
  }

  clear(type, updateGroupIndex) {
    const groupControl = this.updateGroupForm.get(type)['controls'];
    if (groupControl[updateGroupIndex].controls) {
      groupControl[updateGroupIndex]['controls'].name.patchValue('');
      groupControl[updateGroupIndex]['controls'].id.patchValue('');
      groupControl[updateGroupIndex]['controls'].id.updateValueAndValidity();
      groupControl[updateGroupIndex]['controls'].name.updateValueAndValidity();
    }
  }
  getTechnique(e) {
    const index = e.index;
    let id;
    if (index === 0) {
      id = 13;
    }
    else if (index === 1) {
      id = 14;

    }
    else if (index === 2) {
      id = 1;

    }
    else if (index === 3) {
      id = 2;

    }
    else if (index === 4) {
      id = 3;

    }
    else if (index === 5) {
      id = 4;

    }
    else if (index === 6) {
      id = 5;

    }
    else if (index === 7) {
      id = 6;

    }
    else if (index === 8) {
      id = 7;

    }
    else if (index === 9) {
      id = 8;

    }
    else if (index === 10) {
      id = 12;

    }
    else if (index === 11) {
      id = 9;

    }
    else if (index === 12) {
      id = 10;

    }
    else if (index === 13) {
      id = 11;

    }

    this.aptService.getTechniquesById(id).subscribe(
      res => {
        const result: any = res;
        this.techniqueName = [];
        this.techniqueId = [];
        if (result && result.length > 0) {
          result.map(item => {

            if ((item.name && item.name !== undefined)
              && (item.techniqueId && item.techniqueId !== undefined)) {

              if (this.techniqueDetails.find((test) => test.techniqueId === item.techniqueId) === undefined) {
                this.techniqueDetails.push(item);
              }
              this.techniqueName.push(item.name);
              this.techniqueId.push(item.techniqueId);

            }
          });
          if (index === 0) {
            this.mitreRecontid = this.techniqueId;
            this.mitreRecontname = this.techniqueName;
          }
          else if (index === 1) {
            this.mitreResourcettid = this.techniqueId;
            this.mitreResourcetname = this.techniqueName;
          }
          else if (index === 2) {
            this.mitreInitialAccesstid = this.techniqueId;
            this.mitreInitialAccesstname = this.techniqueName;
          }
          else if (index === 3) {
            this.mitreExecutiontid = this.techniqueId;
            this.mitreExecutiontname = this.techniqueName;
          }
          else if (index === 4) {
            this.mitrePersistencetid = this.techniqueId;
            this.mitrePersistencetname = this.techniqueName;
          }
          else if (index === 5) {
            this.mitrePrivilegeEscalationtid = this.techniqueId;
            this.mitrePrivilegeEscalationtname = this.techniqueName;
          }
          else if (index === 6) {
            this.mitreDefenseEvasiontid = this.techniqueId;
            this.mitreDefenseEvasiontname = this.techniqueName;
          }
          else if (index === 7) {
            this.mitreCredentialAccesstid = this.techniqueId;
            this.mitreCredentialAccesstname = this.techniqueName;
          }
          else if (index === 8) {
            this.mitreDiscoverytid = this.techniqueId;
            this.mitreDiscoverytname = this.techniqueName;
          }
          else if (index === 9) {
            this.mitreLateralMovementtid = this.techniqueId;
            this.mitreLateralMovementname = this.techniqueName;
          }
          else if (index === 10) {
            this.mitreCollectiontid = this.techniqueId;
            this.mitreCollectiontname = this.techniqueName;
          }
          else if (index === 11) {
            this.mitreCommandAndControltid = this.techniqueId;
            this.mitreCommandAndControltname = this.techniqueName;
          }
          else if (index === 12) {
            this.mitreExfiltrationtid = this.techniqueId;
            this.mitreExfiltrationtname = this.techniqueName;
          }
          else if (index === 13) {
            this.mitreImpacttid = this.techniqueId;
            this.mitreImpacttname = this.techniqueName;
          }
        } else {
          this.techniqueDetails = [];
          this.techniqueName = [];
          this.techniqueId = [];
        }
      },
      err => {
      }
    );
  }
  getTechniqueByindx(e) {
    const index = e;
    let id;
    if (index === 0) {
      id = 13;
    }
    else if (index === 1) {
      id = 14;

    }
    else if (index === 2) {
      id = 1;

    }
    else if (index === 3) {
      id = 2;

    }
    else if (index === 4) {
      id = 3;

    }
    else if (index === 5) {
      id = 4;

    }
    else if (index === 6) {
      id = 5;

    }
    else if (index === 7) {
      id = 6;

    }
    else if (index === 8) {
      id = 7;

    }
    else if (index === 9) {
      id = 8;

    }
    else if (index === 10) {
      id = 12;

    }
    else if (index === 11) {
      id = 9;

    }
    else if (index === 12) {
      id = 10;

    }
    else if (index === 13) {
      id = 11;

    }

    this.aptService.getTechniquesById(id).subscribe(
      res => {
        const result: any = res;
        this.techniqueName = [];
        this.techniqueId = [];
        if (result && result.length > 0) {
          result.map(item => {

            if ((item.name && item.name !== undefined)
              && (item.techniqueId && item.techniqueId !== undefined)) {

              if (this.techniqueDetails.find((test) => test.techniqueId === item.techniqueId) === undefined) {
                this.techniqueDetails.push(item);
              }
              this.techniqueName.push(item.name);
              this.techniqueId.push(item.techniqueId);

            }
          });
          if (index === 0) {
            this.mitreRecontid = this.techniqueId;
            this.mitreRecontname = this.techniqueName;
          }
          else if (index === 1) {
            this.mitreResourcettid = this.techniqueId;
            this.mitreResourcetname = this.techniqueName;
          }
          else if (index === 2) {
            this.mitreInitialAccesstid = this.techniqueId;
            this.mitreInitialAccesstname = this.techniqueName;
          }
          else if (index === 3) {
            this.mitreExecutiontid = this.techniqueId;
            this.mitreExecutiontname = this.techniqueName;
          }
          else if (index === 4) {
            this.mitrePersistencetid = this.techniqueId;
            this.mitrePersistencetname = this.techniqueName;
          }
          else if (index === 5) {
            this.mitrePrivilegeEscalationtid = this.techniqueId;
            this.mitrePrivilegeEscalationtname = this.techniqueName;
          }
          else if (index === 6) {
            this.mitreDefenseEvasiontid = this.techniqueId;
            this.mitreDefenseEvasiontname = this.techniqueName;
          }
          else if (index === 7) {
            this.mitreCredentialAccesstid = this.techniqueId;
            this.mitreCredentialAccesstname = this.techniqueName;
          }
          else if (index === 8) {
            this.mitreDiscoverytid = this.techniqueId;
            this.mitreDiscoverytname = this.techniqueName;
          }
          else if (index === 9) {
            this.mitreLateralMovementtid = this.techniqueId;
            this.mitreLateralMovementname = this.techniqueName;
          }
          else if (index === 10) {
            this.mitreCollectiontid = this.techniqueId;
            this.mitreCollectiontname = this.techniqueName;
          }
          else if (index === 11) {
            this.mitreCommandAndControltid = this.techniqueId;
            this.mitreCommandAndControltname = this.techniqueName;
          }
          else if (index === 12) {
            this.mitreExfiltrationtid = this.techniqueId;
            this.mitreExfiltrationtname = this.techniqueName;
          }
          else if (index === 13) {
            this.mitreImpacttid = this.techniqueId;
            this.mitreImpacttname = this.techniqueName;
          }
        } else {
          this.techniqueDetails = [];
          this.techniqueName = [];
          this.techniqueId = [];
        }
      },
      err => {
      }
    );
  }
  getLookupServices() {
    this.aptService.getCaseNames().subscribe(
      res => {
        const result: any = res;
        this.caseName = [];
        if (result && result.length > 0) {
          this.groupData = result;
          if (result && result.length > 0) {
            result.map(item => {
              if (item.caseName && item.caseName !== undefined) {
                this.caseName.push(item.caseName);
                return item.caseName;
              }
            });
          }
        } else {
          this.caseName = [];
        }
      },
      err => {
      }
    );
  }

  searchCasename(event): void {
    this.caseNameResult = this.caseName.filter(groupFormObj => groupFormObj.toLowerCase().startsWith(event.query.toLowerCase()));
  }

  onCancel() {
    this.router.navigate(['apt/groupList']);
  }
}
