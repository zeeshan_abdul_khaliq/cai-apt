import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfirmationService, MenuItem} from "primeng/api";
import {ToastrService} from "ngx-toastr";
import {NgxSpinnerService} from "ngx-spinner";
import {GlobalConstants} from '../../core-services/global-constants';
import {AptService} from '../../core-services/apt.service';

@Component({
  selector: 'app-apt-campaigns-update-form',
  templateUrl: './apt-campaigns-update-form.component.html',
  styleUrls: ['./apt-campaigns-update-form.component.scss'],
  providers: [ConfirmationService]
})
export class AptCampaignsUpdateFormComponent implements OnInit {
  breadCrumbs: MenuItem[];

  itemList = [];
  selectedItems = [];
  selectedItemsS = [];
  settings : any;
  settingsSector: any;
  updateCampaignForm: FormGroup;
  zoom = 3;
  address: string;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  loading = false;
  hide = true;
  result: any;
  name: string;
  types: any = [];
  value: any;
  values = [];
  private fileIoCReponse: any;
  submitNextOne = false;
  submitNextThree = false;
  regionResult: any = [];
  sectorResult: any = [];
  groupNameResult: any = [];
  groupName: any = [];
  caseName: any = [];
  caseNameResult: any = [];
  regions: any = [];
  sectors: any = [];
  groupData: any = [];
  groupDetails: any;
  alertId: any;
  groupRes: any = [];
  isTrue = false;
  caseId: any;
  aliases: any = [];
  minDateValue: any;

  @ViewChild('aliasInput') aliasInput: ElementRef<HTMLInputElement>;
  @ViewChild(GlobalConstants.file) uploadElRef: ElementRef;
  display = 'none';
  fileList: any;
  @ViewChild(GlobalConstants.file) file;
  filesToUpload: Set<File> = new Set();
  campaignFile: File;
  spin = false;
  fileopen = false;
  fileNameopen = false;
  hostNameopen = false;
  emailopen = false;
  cveopen = false;
  filehash = false;
  popen = false;
  domainopne = false;
  ipopen = false;
  urlopen = false;
  registryopen = false;
  mutexopen = false;
  pathopen = false;
  fileloading = false;
  constructor(
    private formBuilder: FormBuilder, private toastr: ToastrService, private spinner: NgxSpinnerService,
    private ngZone: NgZone, private aptService: AptService,
    private router: Router, private route: ActivatedRoute) {

    this.settings = {
      singleSelection: false,
      text: "Select Countries",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Regions and Countries',
      enableSearchFilter: true,
      badgeShowLimit: 5,
      groupBy: "category"
    };
    this.settingsSector = {
      singleSelection: false,
      text: "Select Sectors",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Sectors',
      enableSearchFilter: true,
      badgeShowLimit: 5,
    };
    this.minDateValue = new Date();
    this.alertId = this.route.snapshot.queryParamMap.get('id');
    this.breadCrumbs = [
      {label: 'Attack/Campaign Details', routerLink: '/apt/viewCampaign', queryParams: {id: this.alertId, send: false, lastPage: 'Update Attack/Campaign'}, routerLinkActiveOptions: true},
      {label: 'Update Attack/Campaign'},
    ];
  }

  ngOnInit(): void {
    this.getLookupServices();
    this.initReactiveForm();
    this.getAttackCampaignDetails();
  }

  add(event) {
    const input = event.input;
    let value = event.value;

    if ((value || '').trim()) {
      value = value.trim();
      if (this.aliases.find((test) => test.value.toLowerCase() === value.toLowerCase()) === undefined) {
        this.aliases.push({value: value.trim()});
        this.updateCampaignForm.value.alias = this.aliases;
      }
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.aliasInput = null;
  }

  remove(fruit: string): void {
    const index = this.aliases.indexOf(fruit);

    if (index >= 0) {
      this.aliases.splice(index, 1);
      this.updateCampaignForm.value.alias = this.aliases;
    }
  }


  getAttackCampaignDetails() {
    let domains = [];
    let hashes = [];
    let ips = [];
    let mutexes = [];
    let paths = [];
    let process = [];
    let registries = [];
    let urls = [];
    let fileName = [];
    let cve = [];
    let email = [];
    let hostName = [];
    this.spinner.show('createattack-spinner');
    this.aptService.getAttackCampaign(this.alertId).subscribe(
      res => {
        this.groupDetails = res;
        this.spinner.hide('createattack-spinner');

        if (this.groupDetails && this.groupDetails.responseDto) {
          if (this.groupDetails.responseDto.responseCode && this.groupDetails.responseDto.responseCode === Number(200)) {
            this.groupDetails = res;
            const forAlias = [];

            if (this.groupDetails.aliases && this.groupDetails.aliases.length > 0) {
              this.aliases = this.groupDetails.aliases;
              for (let tag of this.groupDetails.aliases) {
                forAlias.push(tag.value);
              }
            }
            this.aptService.getRegions().subscribe(
              res => {
                const result: any = res;
                this.selectedItems = [];
                if (result && result.length > 0) {
                  this.regionResult = result;
                  for (let region of this.regionResult) {
                    const data = {
                      id: region.id,
                      itemName: region.countryName,
                      category: region.region
                    };
                    this.itemList.push(data);

                  }
                }else {
                  this.regionResult = [];
                }

                if (this.groupDetails.countries && this.groupDetails.countries.length > 0) {
                  for (let region of this.groupDetails.countries) {
                    const data = {
                      id: region.id,
                      itemName: region.countryName,
                      category: region.region
                    };
                    this.selectedItems.push(data);

                  }
                  this.groupDetails.countries = this.selectedItems;
                }



              },
              err => {
              }
            );
            this.aptService.getSectors().subscribe(
              res => {
                const result: any = res;
                this.selectedItemsS = [];
                this.sectorResult = [];
                if (result && result.length > 0) {
                  for (let sec of result) {
                    const data = {
                      id: sec.id,
                      itemName: sec.value,
                    };
                    this.sectorResult.push(data);

                  }

                }else {
                  this.sectorResult = [];
                }
                if (this.groupDetails.sectors && this.groupDetails.sectors.length > 0) {
                  for (let sector of this.groupDetails.sectors) {
                    const data = {
                      id: sector.id,
                      itemName: sector.value,
                    };
                    this.selectedItemsS.push(data);
                  }

                  this.groupDetails.sectors = this.selectedItemsS;
                }
              },
              err => {
              }
            );
            this.updateCampaignForm.patchValue({
              attackName: this.groupDetails.caseName,
              sectors: this.groupDetails.sectors,
              alias: forAlias,
              regions: this.groupDetails.countries,
              methodToolUsed: this.groupDetails.methodToolUsed,
              specialCharacteristics: this.groupDetails.specialCharacteristics,
              summary: this.groupDetails.summary,
              sources: this.groupDetails.sources,
              groups: this.groupDetails.groups
            });
            if (this.groupDetails.groups && this.groupDetails.groups.length > 0) {
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.groups.length; updateCampaignFormIndex++) {
                const attackCampaignControl = this.updateCampaignForm.controls.groups as FormArray;
                const edd = this.groupDetails.groups[updateCampaignFormIndex];
                attackCampaignControl.push(this.groupNm(edd.id, edd.groupName, edd.mitreName));
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.groups as FormArray;
              attackCampaignControl.push(this.groupNm('', '', ''));
            }

            if (this.groupDetails.iocs && this.groupDetails.iocs.length > 0) {
              this.groupDetails.iocs.map(iocObj => {
                if (iocObj.iocType && iocObj.iocType.id === 3) {
                  hashes.push({
                    hashValue: iocObj.value,
                    sha1: iocObj.sha1,
                    sha256: iocObj.sha256,
                    md5: iocObj.md5,
                    fileDescription: iocObj.fileDescription,
                    fileType: iocObj.fileType,
                    targetMachine: iocObj.targetMachine,
                    malwareSignatureType: iocObj.malwareSignatureType,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 2) {
                  domains.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 1) {
                  ips.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 4) {
                  mutexes.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 5) {
                  paths.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 8) {
                  registries.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 7) {
                  urls.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 6) {
                  process.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 9) {
                  hostName.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 10) {
                  email.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 11) {
                  cve.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                } else if (iocObj.iocType && iocObj.iocType.id === 12) {
                  fileName.push({
                    value: iocObj.value,
                    id: iocObj.id,
                    date: iocObj.date
                  });
                }
              });
            }
            this.formatIocs(hashes, GlobalConstants.hashData, GlobalConstants.hashes);
            if (this.groupDetails.hashes && this.groupDetails.hashes.length > 0) {
              this.fileopen = true;

              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.hashes.length; updateCampaignFormIndex++) {
                const attackCampaignControl = this.updateCampaignForm.controls.dateIoc as FormArray;
                const edd = this.groupDetails.hashes[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.dateIoc, edd.id));
                }
                if (this.groupDetails.hashes[updateCampaignFormIndex].hashData && this.groupDetails.hashes[updateCampaignFormIndex].hashData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.dateIoc)['controls'][updateCampaignFormIndex].get(GlobalConstants.hashes);
                  for (const edds of this.groupDetails.hashes[updateCampaignFormIndex].hashData) {
                    subattackCampaignControl.push(this.setHashValue(edds.hashValue,  edds.sha256, edds.sha1, edds.md5,
                      edds.fileDescription, edds.fileType, edds.targetMachine, edds.malwareSignatureType, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.dateIoc)['controls'][updateCampaignFormIndex].get(GlobalConstants.hashes);
                  subattackCampaignControl.push(this.setHashValue(null, null, null, null,
                    null, null, null, null, null));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.dateIoc as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.dateIoc, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.dateIoc)['controls'][0].get(GlobalConstants.hashes);
              subattackCampaignControl.push(this.setHashValue(null, null, null, null, null, null,
                null, null, null));
            }
            this.formatIocs(domains, GlobalConstants.domainData, GlobalConstants.domains);
            if (this.groupDetails.domains && this.groupDetails.domains.length > 0) {
              this.domainopne = true;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.domains.length; updateCampaignFormIndex++) {
                const attackCampaignControl = this.updateCampaignForm.controls.domains as FormArray;
                const edd = this.groupDetails.domains[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.domains, edd.id));
                }
                if (this.groupDetails.domains[updateCampaignFormIndex].domainData && this.groupDetails.domains[updateCampaignFormIndex].domainData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.domains)['controls'][updateCampaignFormIndex].get(GlobalConstants.domains);
                  for (const edds of this.groupDetails.domains[updateCampaignFormIndex].domainData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.domains)['controls'][updateCampaignFormIndex].get(GlobalConstants.domains);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.domains as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.domains, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.domains)['controls'][0].get(GlobalConstants.domains);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }
            this.formatIocs(process, GlobalConstants.processData, GlobalConstants.process);
            if (this.groupDetails.process && this.groupDetails.process.length > 0) {
              this.popen = true;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.process.length; updateCampaignFormIndex++) {
                const attackCampaignControl = this.updateCampaignForm.controls.process as FormArray;
                const edd = this.groupDetails.process[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.process, edd.id));
                }
                if (this.groupDetails.process[updateCampaignFormIndex].processData && this.groupDetails.process[updateCampaignFormIndex].processData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.process)['controls'][updateCampaignFormIndex].get(GlobalConstants.process);
                  for (const edds of this.groupDetails.process[updateCampaignFormIndex].processData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.process)['controls'][updateCampaignFormIndex].get(GlobalConstants.process);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.process as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.process, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.process)['controls'][0].get(GlobalConstants.process);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }
            this.formatIocs(urls, GlobalConstants.urlData, GlobalConstants.urls);
            if (this.groupDetails.urls && this.groupDetails.urls.length > 0) {
              this.urlopen = true;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.urls.length; updateCampaignFormIndex++) {
                const attackCampaignControl = this.updateCampaignForm.controls.urls as FormArray;
                const edd = this.groupDetails.urls[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.urls, edd.id));
                }
                if (this.groupDetails.urls[updateCampaignFormIndex].urlData && this.groupDetails.urls[updateCampaignFormIndex].urlData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.urls)['controls'][updateCampaignFormIndex].get(GlobalConstants.urls);
                  for (const edds of this.groupDetails.urls[updateCampaignFormIndex].urlData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.urls)['controls'][updateCampaignFormIndex].get(GlobalConstants.urls);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.urls as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.urls, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.urls)['controls'][0].get(GlobalConstants.urls);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }
            this.formatIocs(registries, GlobalConstants.registryData, GlobalConstants.registeries);
            if (this.groupDetails.registries && this.groupDetails.registries.length > 0) {
              this.registryopen = true;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.registries.length; updateCampaignFormIndex++) {
                const attackCampaignControl = this.updateCampaignForm.controls.registeries as FormArray;
                const edd = this.groupDetails.registries[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.registeries, edd.id));
                }
                if (this.groupDetails.registries[updateCampaignFormIndex].registryData && this.groupDetails.registries[updateCampaignFormIndex].registryData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.registeries)['controls'][updateCampaignFormIndex].get(GlobalConstants.registeries);
                  for (const edds of this.groupDetails.registries[updateCampaignFormIndex].registryData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.registeries)['controls'][updateCampaignFormIndex].get(GlobalConstants.registeries);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.registeries as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.registeries, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.registeries)['controls'][0].get(GlobalConstants.registeries);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }
            this.formatIocs(mutexes, GlobalConstants.mutexData, GlobalConstants.mutexes);
            if (this.groupDetails.mutexes && this.groupDetails.mutexes.length > 0) {
              this.mutexopen = true;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.mutexes.length; updateCampaignFormIndex++) {
                const attackCampaignControl = this.updateCampaignForm.controls.mutexes as FormArray;
                const edd = this.groupDetails.mutexes[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.mutexes, edd.id));
                }
                if (this.groupDetails.mutexes[updateCampaignFormIndex].mutexData && this.groupDetails.mutexes[updateCampaignFormIndex].mutexData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.mutexes)['controls'][updateCampaignFormIndex].get(GlobalConstants.mutexes);
                  for (const edds of this.groupDetails.mutexes[updateCampaignFormIndex].mutexData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.mutexes)['controls'][updateCampaignFormIndex].get(GlobalConstants.mutexes);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.mutexes as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.mutexes, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.mutexes)['controls'][0].get(GlobalConstants.mutexes);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }
            this.formatIocs(paths, GlobalConstants.pathData, GlobalConstants.paths);
            if (this.groupDetails.paths && this.groupDetails.paths.length > 0) {
              this.pathopen = true;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.paths.length; updateCampaignFormIndex++) {
                const attackCampaignControl = this.updateCampaignForm.controls.paths as FormArray;
                const edd = this.groupDetails.paths[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.paths, edd.id));
                }
                if (this.groupDetails.paths[updateCampaignFormIndex].pathData && this.groupDetails.paths[updateCampaignFormIndex].pathData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.paths)['controls'][updateCampaignFormIndex].get(GlobalConstants.paths);
                  for (const edds of this.groupDetails.paths[updateCampaignFormIndex].pathData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.paths)['controls'][updateCampaignFormIndex].get(GlobalConstants.paths);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.paths as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.paths, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.paths)['controls'][0].get(GlobalConstants.paths);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }
            this.formatIocs(ips, GlobalConstants.ipData, GlobalConstants.ips);
            if (this.groupDetails.ips && this.groupDetails.ips.length > 0) {
              this.ipopen = true;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.ips.length; updateCampaignFormIndex++) {
                const attackCampaignControl = this.updateCampaignForm.controls.ips as FormArray;
                const edd = this.groupDetails.ips[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.ips, edd.id));
                }
                if (this.groupDetails.ips[updateCampaignFormIndex].ipData && this.groupDetails.ips[updateCampaignFormIndex].ipData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.ips)['controls'][updateCampaignFormIndex].get(GlobalConstants.ips);
                  for (const edds of this.groupDetails.ips[updateCampaignFormIndex].ipData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.ips)['controls'][updateCampaignFormIndex].get(GlobalConstants.ips);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.ips as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.ips, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.ips)['controls'][0].get(GlobalConstants.ips);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }


            this.formatIocs(fileName, GlobalConstants.fileNameData, GlobalConstants.fileName);
            if (this.groupDetails.fileName && this.groupDetails.fileName.length > 0) {
              this.fileNameopen = true;
              const attackCampaignControl = this.updateCampaignForm.controls.fileName as FormArray;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.fileName.length; updateCampaignFormIndex++) {

                const edd = this.groupDetails.fileName[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.fileName, edd.id));
                }
                if (this.groupDetails.fileName[updateCampaignFormIndex].fileNameData && this.groupDetails.fileName[updateCampaignFormIndex].fileNameData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.fileName)['controls'][updateCampaignFormIndex].get(GlobalConstants.Files);
                  for (const edds of this.groupDetails.fileName[updateCampaignFormIndex].fileNameData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.fileName)['controls'][updateCampaignFormIndex].get(GlobalConstants.Files);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.fileName as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.fileName, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.fileName)['controls'][0].get(GlobalConstants.Files);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }

            this.formatIocs(hostName, GlobalConstants.hostNameData, GlobalConstants.hostName);
            if (this.groupDetails.hostName && this.groupDetails.hostName.length > 0) {
              this.hostNameopen = true;
              const attackCampaignControl = this.updateCampaignForm.controls.hostName as FormArray;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.hostName.length; updateCampaignFormIndex++) {

                const edd = this.groupDetails.hostName[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.hostName, edd.id));
                }
                if (this.groupDetails.hostName[updateCampaignFormIndex].hostNameData && this.groupDetails.hostName[updateCampaignFormIndex].hostNameData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.hostName)['controls'][updateCampaignFormIndex].get(GlobalConstants.hostNames);
                  for (const edds of this.groupDetails.hostName[updateCampaignFormIndex].hostNameData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.hostName)['controls'][updateCampaignFormIndex].get(GlobalConstants.hostNames);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.hostName as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.hostName, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.hostName)['controls'][0].get(GlobalConstants.hostNames);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }

            this.formatIocs(cve, GlobalConstants.cveData, GlobalConstants.cve);
            if (this.groupDetails.cve && this.groupDetails.cve.length > 0) {
              this.cveopen = true;
              const attackCampaignControl = this.updateCampaignForm.controls.cve as FormArray;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.cve.length; updateCampaignFormIndex++) {

                const edd = this.groupDetails.cve[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.cve, edd.id));
                }
                if (this.groupDetails.cve[updateCampaignFormIndex].cveData && this.groupDetails.cve[updateCampaignFormIndex].cveData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.cve)['controls'][updateCampaignFormIndex].get(GlobalConstants.cves);
                  for (const edds of this.groupDetails.cve[updateCampaignFormIndex].cveData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.cve)['controls'][updateCampaignFormIndex].get(GlobalConstants.cves);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.cve as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.cve, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.cve)['controls'][0].get(GlobalConstants.cves);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }

            this.formatIocs(email, GlobalConstants.emailData, GlobalConstants.email);
            if (this.groupDetails.email && this.groupDetails.email.length > 0) {
              this.emailopen = true;
              const attackCampaignControl = this.updateCampaignForm.controls.email as FormArray;
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.email.length; updateCampaignFormIndex++) {

                const edd = this.groupDetails.email[updateCampaignFormIndex];
                if (edd.date !== null || edd.date !== undefined) {
                  edd.date = new Date(edd.date);
                  attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.email, edd.id));
                }
                if (this.groupDetails.email[updateCampaignFormIndex].emailData && this.groupDetails.email[updateCampaignFormIndex].emailData.length > 0) {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.email)['controls'][updateCampaignFormIndex].get(GlobalConstants.emails);
                  for (const edds of this.groupDetails.email[updateCampaignFormIndex].emailData) {
                    subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
                  }
                } else {
                  const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.email)['controls'][updateCampaignFormIndex].get(GlobalConstants.emails);
                  subattackCampaignControl.push(this.setIocValue('', ''));
                }
              }
            } else {
              const attackCampaignControl = this.updateCampaignForm.controls.email as FormArray;
              attackCampaignControl.push(this.setDomain('', GlobalConstants.email, ''));
              const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.email)['controls'][0].get(GlobalConstants.emails);
              subattackCampaignControl.push(this.setIocValue('', ''));
            }
          }
        }
      }, err => {
          this.spinner.hide('createattack-spinner');
          this.toastr.error('Some error occurred Please refresh the page');
      }
    );
  }
  onFilesAdded() {
    this.spin = true;
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (const key in files) {
      if (!isNaN(parseInt(key, 10))) {
        this.campaignFile = files[key];
        this.filesToUpload.add(files[key]);
      }
    }
    if (this.filesToUpload.size === 1) {
      this.uploadFile(this.filesToUpload);
    } else if (this.filesToUpload.size < 1) {
      this.toastr.error('You must select a file');
    } else {
      this.toastr.error('You can only select 1 file');
    }
  }

  public uploadFile(files: Set<File>): any {
    files.forEach(file => {
      const formData: FormData = new FormData();
      formData.append(GlobalConstants.file, file, file.name);
      this.fileloading = false;
      this.spinner.show('ioc-spinner');
      this.aptService.uploadIoCFile(formData).subscribe(
        res => {
          this.spin = false;
          this.spinner.hide('ioc-spinner');

          files.clear();
          this.filesToUpload.clear();
          this.fileIoCReponse = res;

          if (this.fileIoCReponse) {
            this.toastr.success('File Upload Successfully');
            if (this.fileIoCReponse.iocs && this.fileIoCReponse.iocs.length > 0) {
              this.fileloading = true;
              this.getFileDetails(this.fileIoCReponse.iocs);
            } else {
              this.fileloading = false;
              this.toastr.error('No IOCs in file');
              this.uploadElRef.nativeElement.value = '';
              files.clear();
              this.filesToUpload.clear();
            }
          } else {
            this.toastr.error('Only CSV file is accepted Kindly Try again');
            this.uploadElRef.nativeElement.value = '';
            files.clear();
            this.filesToUpload.clear();
          }

        },
        error => {
          this.spinner.hide('ioc-spinner');
          this.fileloading = false;
          this.uploadElRef.nativeElement.value = '';
          files.clear();
          this.filesToUpload.clear();
          this.toastr.error('500 Internal Server Error..');
          this.spin = false;
        });
    });
  }
  getFileDetails(iocs) {
    this.groupDetails = iocs;
    let domains = [];
    let fileName = [];
    let cve = [];
    let email = [];
    let hostName = [];
    let hashes = [];
    let ips = [];
    let mutexes = [];
    let paths = [];
    let process = [];
    let registries = [];
    let urls = [];
    if (this.groupDetails && this.groupDetails.length > 0) {
      this.groupDetails.map(iocObj => {
        if (iocObj.iocType && iocObj.iocType.id === 3) {
          hashes.push({
            hashValue: iocObj.value,
            sha1: iocObj.sha1,
            sha256: iocObj.sha256,
            md5: iocObj.md5,
            fileDescription: iocObj.fileDescription,
            fileType: iocObj.fileType,
            targetMachine: iocObj.targetMachine,
            malwareSignatureType: iocObj.malwareSignatureType,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 2) {
          domains.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 1) {
          ips.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 4) {
          mutexes.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 5) {
          paths.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 8) {
          registries.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 7) {
          urls.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 6) {
          process.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 9) {
          hostName.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 10) {
          email.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 11) {
          cve.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        } else if (iocObj.iocType && iocObj.iocType.id === 12) {
          fileName.push({
            value: iocObj.value,
            id: iocObj.id,
            date: iocObj.date
          });
        }
      });
    }
    this.formatIocs(hashes, GlobalConstants.hashData, GlobalConstants.hashes);
    if (this.groupDetails.hashes && this.groupDetails.hashes.length > 0) {
      this.fileopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.dateIoc as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];
        count = 0;
      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.hashes.length; updateCampaignFormIndex++) {
        const edd = this.groupDetails.hashes[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.dateIoc, edd.id));
        }
        if (this.groupDetails.hashes[updateCampaignFormIndex].hashData && this.groupDetails.hashes[updateCampaignFormIndex].hashData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.dateIoc)['controls'][count].get(GlobalConstants.hashes);
          for (const edds of this.groupDetails.hashes[updateCampaignFormIndex].hashData) {
            subattackCampaignControl.push(this.setHashValue(edds.hashValue, edds.sha1, edds.sha256, edds.md5,
              edds.fileDescription, edds.fileType, edds.targetMachine, edds.malwareSignatureType, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(domains, GlobalConstants.domainData, GlobalConstants.domains);
    if (this.groupDetails.domains && this.groupDetails.domains.length > 0) {
      this.domainopne = true;
      const attackCampaignControl = this.updateCampaignForm.controls.domains as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.domains.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.domains[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.domains, edd.id));
        }
        if (this.groupDetails.domains[updateCampaignFormIndex].domainData && this.groupDetails.domains[updateCampaignFormIndex].domainData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.domains)['controls'][count].get(GlobalConstants.domains);
          for (const edds of this.groupDetails.domains[updateCampaignFormIndex].domainData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(process, GlobalConstants.processData, GlobalConstants.process);
    if (this.groupDetails.process && this.groupDetails.process.length > 0) {
      this.popen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.process as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.process.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.process[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.process, edd.id));
        }
        if (this.groupDetails.process[updateCampaignFormIndex].processData && this.groupDetails.process[updateCampaignFormIndex].processData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.process)['controls'][count].get(GlobalConstants.process);
          for (const edds of this.groupDetails.process[updateCampaignFormIndex].processData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(urls, GlobalConstants.urlData, GlobalConstants.urls);
    if (this.groupDetails.urls && this.groupDetails.urls.length > 0) {
      this.urlopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.urls as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.urls.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.urls[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.urls, edd.id));
        }
        if (this.groupDetails.urls[updateCampaignFormIndex].urlData && this.groupDetails.urls[updateCampaignFormIndex].urlData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.urls)['controls'][count].get(GlobalConstants.urls);
          for (const edds of this.groupDetails.urls[updateCampaignFormIndex].urlData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(registries, GlobalConstants.registryData, GlobalConstants.registeries);
    if (this.groupDetails.registries && this.groupDetails.registries.length > 0) {
      this.registryopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.registeries as FormArray;

      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.registries.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.registries[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.registeries, edd.id));
        }
        if (this.groupDetails.registries[updateCampaignFormIndex].registryData && this.groupDetails.registries[updateCampaignFormIndex].registryData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.registeries)['controls'][count].get(GlobalConstants.registeries);
          for (const edds of this.groupDetails.registries[updateCampaignFormIndex].registryData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(mutexes, GlobalConstants.mutexData, GlobalConstants.mutexes);
    if (this.groupDetails.mutexes && this.groupDetails.mutexes.length > 0) {
      this.mutexopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.mutexes as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.mutexes.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.mutexes[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.mutexes, edd.id));
        }
        if (this.groupDetails.mutexes[updateCampaignFormIndex].mutexData && this.groupDetails.mutexes[updateCampaignFormIndex].mutexData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.mutexes)['controls'][count].get(GlobalConstants.mutexes);
          for (const edds of this.groupDetails.mutexes[updateCampaignFormIndex].mutexData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(paths, GlobalConstants.pathData, GlobalConstants.paths);
    if (this.groupDetails.paths && this.groupDetails.paths.length > 0) {
      this.pathopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.paths as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.paths.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.paths[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.paths, edd.id));
        }
        if (this.groupDetails.paths[updateCampaignFormIndex].pathData && this.groupDetails.paths[updateCampaignFormIndex].pathData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.paths)['controls'][count].get(GlobalConstants.paths);
          for (const edds of this.groupDetails.paths[updateCampaignFormIndex].pathData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(ips, GlobalConstants.ipData, GlobalConstants.ips);
    if (this.groupDetails.ips && this.groupDetails.ips.length > 0) {
      this.ipopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.ips as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.ips.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.ips[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.ips, edd.id));
        }
        if (this.groupDetails.ips[updateCampaignFormIndex].ipData && this.groupDetails.ips[updateCampaignFormIndex].ipData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.ips)['controls'][count].get(GlobalConstants.ips);
          for (const edds of this.groupDetails.ips[updateCampaignFormIndex].ipData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(fileName, GlobalConstants.fileNameData, GlobalConstants.fileName);
    if (this.groupDetails.fileName && this.groupDetails.fileName.length > 0) {
      this.fileNameopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.fileName as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.fileName.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.fileName[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.fileName, edd.id));
        }
        if (this.groupDetails.fileName[updateCampaignFormIndex].fileNameData && this.groupDetails.fileName[updateCampaignFormIndex].fileNameData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.fileName)['controls'][count].get(GlobalConstants.Files);
          for (const edds of this.groupDetails.fileName[updateCampaignFormIndex].fileNameData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(hostName, GlobalConstants.hostNameData, GlobalConstants.hostName);
    if (this.groupDetails.hostName && this.groupDetails.hostName.length > 0) {
      this.hostNameopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.hostName as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.hostName.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.hostName[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.hostName, edd.id));
        }
        if (this.groupDetails.hostName[updateCampaignFormIndex].hostNameData && this.groupDetails.hostName[updateCampaignFormIndex].hostNameData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.hostName)['controls'][count].get(GlobalConstants.hostNames);
          for (const edds of this.groupDetails.hostName[updateCampaignFormIndex].hostNameData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }
    this.formatIocs(cve, GlobalConstants.cveData, GlobalConstants.cve);
    if (this.groupDetails.cve && this.groupDetails.cve.length > 0) {
      this.cveopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.cve as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.cve.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.cve[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.cve, edd.id));
        }
        if (this.groupDetails.cve[updateCampaignFormIndex].cveData && this.groupDetails.cve[updateCampaignFormIndex].cveData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.cve)['controls'][count].get(GlobalConstants.cves);
          for (const edds of this.groupDetails.cve[updateCampaignFormIndex].cveData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;
      }
    }

    this.formatIocs(email, GlobalConstants.emailData, GlobalConstants.email);
    if (this.groupDetails.email && this.groupDetails.email.length > 0) {
      this.emailopen = true;
      const attackCampaignControl = this.updateCampaignForm.controls.email as FormArray;
      let count = attackCampaignControl.length;
      if (count > 1) {
        attackCampaignControl.removeAt(count);
        attackCampaignControl[count] = [];
      } else {
        attackCampaignControl.removeAt(0);
        attackCampaignControl[0] = [];count = 0;

      }
      for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < this.groupDetails.email.length; updateCampaignFormIndex++) {

        const edd = this.groupDetails.email[updateCampaignFormIndex];
        if (edd.date !== null || edd.date !== undefined) {
          edd.date = new Date(edd.date);
          attackCampaignControl.push(this.setDomain(edd.date, GlobalConstants.email, edd.id));
        }
        if (this.groupDetails.email[updateCampaignFormIndex].emailData && this.groupDetails.email[updateCampaignFormIndex].emailData.length > 0) {
          const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.email)['controls'][count].get(GlobalConstants.emails);
          for (const edds of this.groupDetails.email[updateCampaignFormIndex].emailData) {
            subattackCampaignControl.push(this.setIocValue(edds.value, edds.id));
          }
        }
        count++;

      }
    }
  }

  formatIocs(iocArr, type, iocType) {
    const ioc = [];
    iocArr.forEach(mapResponse => {
      let match = ioc.find(r => r.date === mapResponse.date);
      if (match) {
        match[type] = match[type].concat(mapResponse);
      } else {
        const iocData = {
          date: mapResponse.date,
          id: mapResponse.id,
        };
        iocData[type] = [mapResponse];
        ioc.push(iocData);
      }
    });
    this.groupDetails[iocType] = ioc;
  }

  setDomain(date, type, id): FormGroup {
    if (type === GlobalConstants.domains) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        domains: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.mutexes) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        mutexes: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.registeries) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        registeries: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.dateIoc) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        hashes: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.ips) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        ips: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.urls) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        urls: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.paths) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        paths: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.process) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        process: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.registeries) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        registeries: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.fileName) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        Files: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.hostName) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        hostNames: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.email) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        emails: this.formBuilder.array([])
      });
    } else if (type === GlobalConstants.cve) {
      return this.formBuilder.group({
        date: [date ],
        id: id,
        cves: this.formBuilder.array([])
      });
    }
  }

  setIocValue(value, id): FormGroup {
    return this.formBuilder.group({
      value: [value ],
      id: id
    });
  }

  setHashValue(hash, sha256, sha1, md5, fileDescription, fileType, targetMachine, malwareSignatureType, id) {
    return this.formBuilder.group({
      hash: [hash ],
      sha256: [sha256 ],
      sha1: [sha1 ],
      md5: [md5 ],
      fileDescription: [fileDescription ],
      fileType: [fileType ],
      targetMachine: [targetMachine ],
      malwareSignatureType: [malwareSignatureType ],
      id: id
    });
  }

  initReactiveForm() {
    this.updateCampaignForm = this.formBuilder.group({
      attackName: ['', Validators.required],
      alias: [''],
      sectors: [''],
      regions: [''],
      filess: [''],
      methodToolUsed: [''],
      specialCharacteristics: [''],
      summary: [''],
      sources: [''],
      groups: this.formBuilder.array([]),
      dateIoc: this.formBuilder.array([]),
      fileName: this.formBuilder.array([]),
      hostName: this.formBuilder.array([]),
      email: this.formBuilder.array([]),
      cve: this.formBuilder.array([]),
      process: this.formBuilder.array([]),
      domains: this.formBuilder.array([]),
      ips: this.formBuilder.array([]),
      urls: this.formBuilder.array([]),
      registeries: this.formBuilder.array([]),
      mutexes: this.formBuilder.array([]),
      paths: this.formBuilder.array([])
    });
  }

  // convenience getter for easy access to form fields
  get updateCampaignControl() {
    return this.updateCampaignForm.controls;
  }


  /*--------------Submit form------------------*/
  onSubmit() {
    let groups = [];
    let domains = [];
    let hashes = [];
    let ips = [];
    let mutexes = [];
    let paths = [];
    let process = [];
    let registries = [];
    let urls = [];
    let iocs = [];
    let fileName = [];
    let hostName = [];
    let cve = [];
    let email = [];
    this.caseName.map(mapResponse => {
      if (this.updateCampaignForm.value.attackName === mapResponse) {
        this.groupData.map(updateCampaignFormIndex => {
          if (updateCampaignFormIndex.caseName) {
            this.isTrue = true;
            this.caseId = updateCampaignFormIndex.id;
          }
        });
      }
    });
    if (this.updateCampaignForm.value.groups && this.updateCampaignForm.value.groups.length > 0) {
      this.updateCampaignForm.value.groups.map(iocObj => {
        if (iocObj.id === "") {
          let idGrp: any;
          this.groupRes.map(updateCampaignFormIndex => {
            if (updateCampaignFormIndex.groupName === iocObj.groupName) {
              idGrp = updateCampaignFormIndex.id;
              groups.push({
                groupName: iocObj.groupName,
                id: idGrp,
                mitreName: iocObj.mitreName
              });
            }
          });
        } else {
          groups.push({
            groupName: iocObj.groupName,
            id: iocObj.id,
            mitreName: iocObj.mitreName
          });
        }
      });
    }

    if (this.updateCampaignForm.value.dateIoc && this.updateCampaignForm.value.dateIoc.length > 0) {
      hashes = this.updateCampaignForm.value.dateIoc.map(iocObj => {
        if (iocObj.date !== "" && iocObj.hashes && iocObj.hashes.length > 0) {
          iocObj.hashes.map(iocSubObj => {
            if ((iocSubObj.fileDescription !== "" && iocSubObj.fileDescription !== undefined && iocSubObj.fileDescription !== null) ||
              (iocSubObj.fileType !== "" && iocSubObj.fileType !== undefined && iocSubObj.fileType !== null) ||
              (iocSubObj.hash !== "" && iocSubObj.hash !== undefined && iocSubObj.hash !== null) ||
              (iocSubObj.sha1 !== "" && iocSubObj.sha1 !== undefined && iocSubObj.sha1 !== null) ||
              (iocSubObj.sha256 !== "" && iocSubObj.sha256 !== undefined && iocSubObj.sha256 !== null) ||
              (iocSubObj.md5 !== "" && iocSubObj.md5 !== undefined && iocSubObj.md5 !== null) ||
              (iocSubObj.malwareSignatureType !== "" && iocSubObj.malwareSignatureType !== undefined && iocSubObj.malwareSignatureType !== null) ||
              (iocSubObj.targetMachine !== "" && iocSubObj.targetMachine !== undefined && iocSubObj.targetMachine !== null)) {
              iocs.push({
                fileDescription: iocSubObj.fileDescription,
                fileType: iocSubObj.fileType,
                value: iocSubObj.hash,
                malwareSignatureType: iocSubObj.malwareSignatureType,
                md5: iocSubObj.md5,
                sha1: iocSubObj.sha1,
                sha256: iocSubObj.sha256,
                targetMachine: iocSubObj.targetMachine,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 3,
                  value: GlobalConstants.Hash
                },
              });
            }
          });
        }
      });
    }
    if (this.updateCampaignForm.value.domains && this.updateCampaignForm.value.domains.length > 0) {
      domains = this.updateCampaignForm.value.domains.map(iocObj => {
        if (iocObj.date !== "" && iocObj.domains && iocObj.domains.length > 0) {
          
          iocObj.domains.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 2,
                  value: GlobalConstants.Domain,
                },
              });
            }
          });
        }
      });

    }
    if (this.updateCampaignForm.value.ips && this.updateCampaignForm.value.ips.length > 0) {
      ips = this.updateCampaignForm.value.ips.map(iocObj => {
        if (iocObj.date !== "" && iocObj.ips && iocObj.ips.length > 0) {
          
          iocObj.ips.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 1,
                  value: GlobalConstants.IPv4,
                },
              });
            }
          });
        }
      });
    }
    if (this.updateCampaignForm.value.mutexes && this.updateCampaignForm.value.mutexes.length > 0) {
      mutexes = this.updateCampaignForm.value.mutexes.map(iocObj => {
        if (iocObj.date !== "" && iocObj.mutexes && iocObj.mutexes.length > 0) {
          
          iocObj.mutexes.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 4,
                  value: GlobalConstants.Mutex,
                },
              });
            }
          });
        }
      });
    }
    if (this.updateCampaignForm.value.paths && this.updateCampaignForm.value.paths.length > 0) {
      paths = this.updateCampaignForm.value.paths.map(iocObj => {
        if (iocObj.date !== "" && iocObj.paths && iocObj.paths.length > 0) {
          
          iocObj.paths.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 5,
                  value: GlobalConstants.Path,
                },
              });
            }
          });
        }
      });
    }
    if (this.updateCampaignForm.value.process && this.updateCampaignForm.value.process.length > 0) {
      process = this.updateCampaignForm.value.process.map(iocObj => {
        if (iocObj.date !== "" && iocObj.process && iocObj.process.length > 0) {
          
          iocObj.process.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 6,
                  value: GlobalConstants.Process,
                },
              });
            }
          });
        }
      });
    }
    if (this.updateCampaignForm.value.registeries && this.updateCampaignForm.value.registeries.length > 0) {
      registries = this.updateCampaignForm.value.registeries.map(iocObj => {
        if (iocObj.date !== "" && iocObj.registeries && iocObj.registeries.length > 0) {
          
          iocObj.registeries.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 8,
                  value: GlobalConstants.Registry,
                },
              });
            }
          });
        }
      });
    }
    if (this.updateCampaignForm.value.urls && this.updateCampaignForm.value.urls.length > 0) {
      urls = this.updateCampaignForm.value.urls.map(iocObj => {
        if (iocObj.date !== "" && iocObj.urls && iocObj.urls.length > 0) {
          
          iocObj.urls.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 7,
                  value: GlobalConstants.URL,
                },
              });
            }
          });
        }
      });
    }


    if (this.updateCampaignForm.value.fileName && this.updateCampaignForm.value.fileName.length > 0) {
      fileName = this.updateCampaignForm.value.fileName.map(iocObj => {
        if (iocObj.date !== "" && iocObj.Files && iocObj.Files.length > 0) {
          
          iocObj.Files.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 12,
                  value: GlobalConstants.File,
                },
              });
            }
          });
        }
      });

    }
    if (this.updateCampaignForm.value.hostName && this.updateCampaignForm.value.hostName.length > 0) {
      hostName = this.updateCampaignForm.value.hostName.map(iocObj => {
        if (iocObj.date !== "" && iocObj.hostNames && iocObj.hostNames.length > 0) {
          
          iocObj.hostNames.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 9,
                  value: GlobalConstants.Hostname,
                },
              });
            }
          });
        }
      });

    }
    if (this.updateCampaignForm.value.email && this.updateCampaignForm.value.email.length > 0) {
      email = this.updateCampaignForm.value.email.map(iocObj => {
        if (iocObj.date !== "" && iocObj.emails && iocObj.emails.length > 0) {
          
          iocObj.emails.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 10,
                  value: GlobalConstants.Email,
                },
              });
            }
          });
        }
      });

    }
    if (this.updateCampaignForm.value.cve && this.updateCampaignForm.value.cve.length > 0) {
      cve = this.updateCampaignForm.value.cve.map(iocObj => {
        if (iocObj.date !== "" && iocObj.cves && iocObj.cves.length > 0) {
          
          iocObj.cves.map(iocSubObj => {
            if (iocSubObj.value !== "" && iocSubObj.value !== undefined) {
              iocs.push({
                value: iocSubObj.value,
                date: iocObj.date,
                dateStr: iocObj.date,
                iocType: {
                  id: 11,
                  value: GlobalConstants.CVE
                },
              });
            }
          });

        }
      });

    }
    if (domains.length === 1 && (domains[0] === null || domains[0] === undefined)) {
      domains = [];
    }
    if (paths.length === 1 && (paths[0] === null || paths[0] === undefined)) {
      paths = [];
    }
    if (hashes.length === 1 && (hashes[0] === null || hashes[0] === undefined)) {
      hashes = [];
    }
    if (ips.length === 1 && (ips[0] === null || ips[0] === undefined)) {
      ips = [];
    }
    if (urls.length === 1 && (urls[0] === null || urls[0] === undefined)) {
      urls = [];
    }
    if (process.length === 1 && (process[0] === null || process[0] === undefined)) {
      process = [];
    }
    if (registries.length === 1 && (registries[0] === null || registries[0] === undefined)) {
      registries = [];
    }
    if (mutexes.length === 1 && (mutexes[0] === null || mutexes[0] === undefined)) {
      mutexes = [];
    }
    if (this.updateCampaignForm.value.regions === '') {
      this.updateCampaignForm.value.regions = [];
    }
    if (this.updateCampaignForm.value.sectors === '') {
      this.updateCampaignForm.value.sectors = [];
    }
    const dataJson = {
      aliases: this.aliases,
      caseName: this.updateCampaignForm.value.attackName,
      id: this.alertId,
      methodToolUsed: this.updateCampaignForm.value.methodToolUsed,
      sources: this.updateCampaignForm.value.sources,
      groups: groups,
      specialCharacteristics: this.updateCampaignForm.value.specialCharacteristics,
      summary: this.updateCampaignForm.value.summary,
      countries: this.updateCampaignForm.value.regions,
      sectors: this.updateCampaignForm.value.sectors,
      iocs: iocs
    };
    this.updateForm(dataJson);
  }

  updateForm(dataJson) {
    this.spinner.show('createattack-spinner');
    this.aptService.updateAttacksCampaign(dataJson).subscribe(
        res => {
          this.spinner.hide('createattack-spinner');
          const response: any = res;
          if (response.responseDto.responseCode === 200 || response.responseDto.responseCode === 201) {
            this.toastr.success(response.responseDto.response);
            this.viewAttackCampaign(response.id);
          } else {
            this.toastr.error('Some error occurred');
          }
        },
        error => {
          this.spinner.hide('createattack-spinner');
          const err = error;
          this.toastr.error(err.responseDto.response);
        });
  }


  viewAttackCampaign(id) {
    this.router.navigate(['apt/viewCampaign'], {queryParams: {id: id, send: false, lastPage: 'Update Attack/Campaign'}});

  }

  searchCasename(event): void {
    if (event) {
      this.caseNameResult = this.caseName.filter(iocObj => iocObj.toLowerCase().startsWith(event.query.toLowerCase()));
    }
  }

  searchGroupName(event): void {
    if (event) {
      this.groupNameResult = this.groupName.filter(iocObj => iocObj.toLowerCase().startsWith(event.query.toLowerCase()));
    }
  }

  /*--------------Form Array Fields - Initialize Form Array - Add mapResponse to form array - Remove mapResponse from form array------------------*/

  dateIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      hashes: this.formBuilder.array([this.hashs()]),
    });
  }

  filePath(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      filePaths: this.formBuilder.array([this.paths()]),
    });
  }

  dns(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      dnss: this.formBuilder.array([this.domains()]),
    });
  }

  paths(): FormGroup {
    return this.formBuilder.group({
      path: ['' ],
    });
  }

  fileName(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      Files: this.formBuilder.array([this.iocValue()]),
    });
  }
  hostName(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      hostNames: this.formBuilder.array([this.iocValue()]),
    });
  }
  email(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      emails: this.formBuilder.array([this.iocValue()]),
    });
  }
  cve(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      cves: this.formBuilder.array([this.iocValue()]),
    });
  }
  processIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      process: this.formBuilder.array([this.iocValue()]),
    });
  }

  domainsIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      domains: this.formBuilder.array([this.iocValue()]),
    });
  }

  ipsIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      ips: this.formBuilder.array([this.iocValue()]),
    });
  }

  urlsIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      urls: this.formBuilder.array([this.iocValue()]),
    });
  }

  registeriesIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      registeries: this.formBuilder.array([this.iocValue()]),
    });
  }

  mutexesIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      mutexes: this.formBuilder.array([this.iocValue()]),
    });
  }

  pathsIoc(): FormGroup {
    return this.formBuilder.group({
      date: ['' ],
      paths: this.formBuilder.array([this.iocValue()]),
    });
  }

  iocValue(): FormGroup {
    return this.formBuilder.group({
      value: ['' ],
    });
  }

  addProcess() {
    const attackCampaignControl = this.updateCampaignForm.controls.process as FormArray;
    attackCampaignControl.push(this.processIoc());
  }

  addDomain() {
    const attackCampaignControl = this.updateCampaignForm.controls.domains as FormArray;
    attackCampaignControl.push(this.domainsIoc());
  }

  addIps() {
    const attackCampaignControl = this.updateCampaignForm.controls.ips as FormArray;
    attackCampaignControl.push(this.ipsIoc());
  }

  addUrls() {
    const attackCampaignControl = this.updateCampaignForm.controls.urls as FormArray;
    attackCampaignControl.push(this.urlsIoc());
  }

  addRegisteries() {
    const attackCampaignControl = this.updateCampaignForm.controls.registeries as FormArray;
    attackCampaignControl.push(this.registeriesIoc());
  }

  addMutexes() {
    const attackCampaignControl = this.updateCampaignForm.controls.mutexes as FormArray;
    attackCampaignControl.push(this.mutexesIoc());
  }

  addPath() {
    const attackCampaignControl = this.updateCampaignForm.controls.paths as FormArray;
    attackCampaignControl.push(this.pathsIoc());
  }
  addFileName() {
    const attackCampaignControl = this.updateCampaignForm.controls.fileName as FormArray;
    attackCampaignControl.push(this.fileName());
  }
  addHostName() {
    const attackCampaignControl = this.updateCampaignForm.controls.hostName as FormArray;
    attackCampaignControl.push(this.hostName());
  }
  addEmail() {
    const attackCampaignControl = this.updateCampaignForm.controls.email as FormArray;
    attackCampaignControl.push(this.email());
  }
  addCVE() {
    const attackCampaignControl = this.updateCampaignForm.controls.cve as FormArray;
    attackCampaignControl.push(this.cve());
  }

  addSubIoc(subIocIndex, type) {
    if (type === GlobalConstants.process) {
      subIocIndex.get(GlobalConstants.process).push(this.iocValue());
    } else if (type === GlobalConstants.domains) {
      subIocIndex.get(GlobalConstants.domains).push(this.iocValue());
    } else if (type === GlobalConstants.ips) {
      subIocIndex.get(GlobalConstants.ips).push(this.iocValue());
    } else if (type === GlobalConstants.urls) {
      subIocIndex.get(GlobalConstants.urls).push(this.iocValue());
    } else if (type === GlobalConstants.registeries) {
      subIocIndex.get(GlobalConstants.registeries).push(this.iocValue());
    } else if (type === GlobalConstants.mutexes) {
      subIocIndex.get(GlobalConstants.mutexes).push(this.iocValue());
    } else if (type === GlobalConstants.paths) {
      subIocIndex.get(GlobalConstants.paths).push(this.iocValue());
    } else if (type === GlobalConstants.fileName) {
      subIocIndex.get(GlobalConstants.Files).push(this.iocValue());
    } else if (type === GlobalConstants.hostName) {
      subIocIndex.get(GlobalConstants.hostNames).push(this.iocValue());
    } else if (type === GlobalConstants.email) {
      subIocIndex.get(GlobalConstants.emails).push(this.iocValue());
    } else if (type === GlobalConstants.cve) {
      subIocIndex.get(GlobalConstants.cves).push(this.iocValue());
    }
  }

  removeProcess(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.process as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }

  removeDomain(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.domains as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }

  removeIps(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.ips as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }

  removeUrls(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.urls as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }

  removeRegisteries(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.registeries as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }

  removeMutexes(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.mutexes as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }

  removePath(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.paths as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }
  removeFileName(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.fileName as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }
  removeHostName(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.hostName as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }
  removeEmail(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.email as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }
  removeCVE(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.cve as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }
  removeSubIocs(updateCampaignFormIndex: number, subIocIndex, type) {
    if (type === GlobalConstants.process) {
      const attackCampaignControl = subIocIndex.controls.process as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.domains) {
      const attackCampaignControl = subIocIndex.controls.domains as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.ips) {
      const attackCampaignControl = subIocIndex.controls.ips as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.urls) {
      const attackCampaignControl = subIocIndex.controls.urls as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.registeries) {
      const attackCampaignControl = subIocIndex.controls.registeries as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.mutexes) {
      const attackCampaignControl = subIocIndex.controls.mutexes as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.paths) {
      const attackCampaignControl = subIocIndex.controls.paths as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.fileName) {
      const attackCampaignControl = subIocIndex.controls.Files as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.hostName) {
      const attackCampaignControl = subIocIndex.controls.hostNames as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.email) {
      const attackCampaignControl = subIocIndex.controls.emails as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    } else if (type === GlobalConstants.cve) {
      const attackCampaignControl = subIocIndex.controls.cves as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    }
  }


  domains(): FormGroup {
    return this.formBuilder.group({
      domain: ['' ],
    });
  }

  hashs() {
    return this.formBuilder.group({
      hash: [null ],
      sha256: [null ],
      sha1: [null ],
      md5: [null],
      fileDescription: [null ],
      fileType: [null],
      targetMachine: [null],
      malwareSignatureType: [null]
    });
  }

  groupNm(id, type, value) {
    return this.formBuilder.group({
      id: [id],
      groupName: [type, Validators.required],
      mitreName: [{value, disabled: true}, Validators.required],
    });
  }

  attackCampaigns(): FormGroup {
    return this.formBuilder.group({
      associatedIndicators: ['' ],
      caseName: ['' ],
      regions: ['' ],
      sectors: ['' ]
    });
  }

  addGroups() {
    const attackCampaignControl = this.updateCampaignForm.controls.groups as FormArray;
    attackCampaignControl.push(this.groupNm('', '', ''));
  }

  removeGroups(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.groups as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }

  addHash(subIocIndex) {
    subIocIndex.get(GlobalConstants.hashes).push(this.hashs());
  }

  addDateHash() {
    const attackCampaignControl = this.updateCampaignForm.controls.dateIoc as FormArray;
    attackCampaignControl.push(this.dateIoc());
  }

  removeHash(updateCampaignFormIndex: number, subIocIndex) {
    const attackCampaignControl = subIocIndex.controls.hashes as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }

  removeDateHash(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.dateIoc as FormArray;
    attackCampaignControl.removeAt(updateCampaignFormIndex);
  }
  // in case of zero index
  removeGroup(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.groups as FormArray;
    if (attackCampaignControl.length <= 1) {
      const attackCampaignControl = this.updateCampaignForm.get(GlobalConstants.groups)['controls'];
      attackCampaignControl[updateCampaignFormIndex]['controls'].id.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].groupName.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].mitreName.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].id.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].groupName.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].mitreName.updateValueAndValidity();
    } else if (attackCampaignControl.length > 1) {
      const attackCampaignControl = this.updateCampaignForm.controls.groups as FormArray;
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    }
  }
  removeDateHashZero(updateCampaignFormIndex: number) {
    const attackCampaignControl = this.updateCampaignForm.controls.dateIoc as FormArray;
    if (attackCampaignControl.length <= 1) {
      const attackCampaignControl = this.updateCampaignForm.get(GlobalConstants.dateIoc)['controls'];
      attackCampaignControl[updateCampaignFormIndex]['controls'].date.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].date.updateValueAndValidity();
      const subattackCampaignControl = this.updateCampaignForm.get(GlobalConstants.dateIoc)['controls'][0].get(GlobalConstants.hashes);
      while (subattackCampaignControl.length !== 0) {
        subattackCampaignControl.removeAt(0)
      }
      subattackCampaignControl.push(this.setHashValue(null , null , null , null ,
        null , null , null , null , null ));


    } else if (attackCampaignControl.length > 1) {
      attackCampaignControl.removeAt(updateCampaignFormIndex);

    }
  }
  removeHashz(updateCampaignFormIndex: number, subIocIndex) {
    const attackCampaignControl = subIocIndex.controls.hashes as FormArray;
    if (attackCampaignControl.length <= 1) {
      const attackCampaignControl = subIocIndex.get(GlobalConstants.hashes)['controls'];
      attackCampaignControl[updateCampaignFormIndex]['controls'].hash.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].sha256.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].sha1.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].md5.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].fileDescription.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].fileType.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].targetMachine.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].malwareSignatureType.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].id.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].hash.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].sha256.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].md5.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].fileDescription.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].targetMachine.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].malwareSignatureType.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].fileType.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].id.updateValueAndValidity();
      attackCampaignControl[updateCampaignFormIndex]['controls'].sha1.updateValueAndValidity();

    } else if (attackCampaignControl.length > 1) {
      attackCampaignControl.removeAt(updateCampaignFormIndex);

    }
  }

  removeProcessZ(updateCampaignFormIndex: number, type, Vtype) {
    const attackCampaignControl = this.updateCampaignForm.controls[type] as FormArray;
    if (attackCampaignControl.length <= 1) {
      const attackCampaignControl = this.updateCampaignForm.get(type)['controls'];
      attackCampaignControl[updateCampaignFormIndex]['controls'].date.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].date.updateValueAndValidity();
      const subattackCampaignControl = this.updateCampaignForm.get(type)['controls'][0].get(Vtype);
      while (subattackCampaignControl.length !== 0) {
        subattackCampaignControl.removeAt(0)
      }
      subattackCampaignControl.push(this.setIocValue('', '' ));


    } else if (attackCampaignControl.length > 1) {
      attackCampaignControl.removeAt(updateCampaignFormIndex);
    }
  }
  removeSubIocsZ(updateCampaignFormIndex: number, subIocIndex, type) {
    const attackCampaignControl = subIocIndex.controls[type] as FormArray;
    if (attackCampaignControl.length <= 1) {
      const attackCampaignControl = subIocIndex.get(type)['controls'];
      attackCampaignControl[updateCampaignFormIndex]['controls'].value.patchValue('');
      attackCampaignControl[updateCampaignFormIndex]['controls'].value.updateValueAndValidity();

    } else if (attackCampaignControl.length > 1) {
      attackCampaignControl.removeAt(updateCampaignFormIndex);

    }
  }
  getLookupServices() {
    this.itemList = [];
    this.aptService.getGroupNames().subscribe(
      res => {
        const result: any = res;
        this.groupName = [];
        this.groupRes = [];
        if (result && result.length > 0) {
          result.map(mapResponse => {
            if (mapResponse.groupName && mapResponse.groupName !== undefined && mapResponse.id) {
              this.groupName.push(mapResponse.groupName);
              this.groupRes.push(mapResponse);
              return mapResponse.groupName;
            }
          });
        } else {
          this.groupName = [];
        }
      },
      err => {
      });


    this.aptService.getCaseNames().subscribe(
      res => {
        const result: any = res;
        this.caseName = [];
        if (result && result.length > 0) {
          this.groupData = result;
          if (result && result.length > 0) {
            result.map(mapResponse => {
              if (mapResponse.caseName && mapResponse.caseName !== undefined) {
                this.caseName.push(mapResponse.caseName);
                return mapResponse.caseName;
              }
            });
          }
        } else {
          this.caseName = [];
        }
      },
      err => {
      }
    );
  }

  patchMitreDetails(event, type, updateCampaignFormIndex) {
    if (event && event.groupName && event.groupName.value) {
      this.groupRes.filter((iocObj, subIocIndex) => {
        if (event.groupName.value === iocObj.groupName) {
          const attackCampaignControl = this.updateCampaignForm.get(type)['controls'];
          if (attackCampaignControl[updateCampaignFormIndex].controls) {
            attackCampaignControl[updateCampaignFormIndex] = this.groupNm(iocObj.id, iocObj.groupName, iocObj.mitreName);
          }
        }
      });
    }
  }

  getGroupData(event) {
    if (event && event.value) {
      if (this.groupData && this.groupData.length > 0) {
        for (const edd of this.groupData) {
          if (edd.caseName === event.value) {
            const attackCampaignControl = this.updateCampaignForm.controls.groups as FormArray;
            if (attackCampaignControl && attackCampaignControl.value) {
              for (let updateCampaignFormIndex = 0; updateCampaignFormIndex < attackCampaignControl.value.length; updateCampaignFormIndex++) {
                attackCampaignControl.removeAt(updateCampaignFormIndex);
              }
            }
            if (edd.groupDtos && edd.groupDtos.length > 0) {
              for (const ed of edd.groupDtos) {
                const controls = this.updateCampaignForm.controls.groups as FormArray;
                controls.push(this.groupNm(ed.id, ed.groupName, ed.mitreName));
              }
            } else {
              attackCampaignControl.push(this.groupNm('', '', ''));
            }
          }
        }
      } else {
        const attackCampaignControl = this.updateCampaignForm.controls.groups as FormArray;
        attackCampaignControl.push(this.groupNm('', '', ''));
      }
    }
  }

  onCancel() {
    this.router.navigate(['apt/attacksCampaignList']);
  }
}
