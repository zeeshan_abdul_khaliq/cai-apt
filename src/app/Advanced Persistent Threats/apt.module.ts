import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableModule} from 'primeng/table';
import {CardModule} from 'primeng/card';
import {NgxSpinnerModule} from 'ngx-spinner';
import {TooltipModule} from 'primeng/tooltip';
import {TabViewModule} from 'primeng/tabview';
import {NgxEchartsModule} from 'ngx-echarts';
import * as echarts from 'echarts';
import {CalendarModule} from 'primeng/calendar';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MultiSelectModule} from 'primeng/multiselect';
import {AvatarModule} from 'primeng/avatar';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {DashboardComponent} from './dashboard/dashboard.component';
import {TagModule} from 'primeng/tag';
import {EditorModule} from 'primeng/editor';
import {ChipsModule} from 'primeng/chips';
import {AptRoutingModule} from './apt-routing.module';
import {AptComponent} from './apt.component';
import {DatamapIComponent} from './dashboard/datamap/datamapI.component';
import {DatamapAComponent} from './dashboard/datamap/datamapA.component';
import {TargetRegionComponent} from './dashboard/targetRegion.component';
import {TargetSectorComponent} from './dashboard/targetSector.component';
import {ListingAttackCampaignComponent} from './listingAttackCampaign/listingAttackCampaign.component';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ListingGroupsComponent} from './listingGroups/listingGroups.component';
import {InputSwitchModule} from 'primeng/inputswitch';
import {AptGroupsComponent} from './apt-groups/apt-groups.component';
import {AccordionModule} from 'primeng/accordion';
import {AptCampaignsComponent} from './apt-campaigns/apt-campaigns.component';
import {ChartModule} from 'primeng/chart';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {AptGroupCreateFormComponent} from './apt-group-create-form/apt-group-create-form.component';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {AptGroupUpdateFormComponent} from './apt-group-update-form/apt-group-update-form.component';
import {AptCampaignsAddFormComponent} from './apt-campaigns-create-form/apt-campaigns-add-form.component';
import {AptCampaignsUpdateFormComponent} from './apt-campaigns-update-form/apt-campaigns-update-form.component';
import {MyPreferencesComponent} from './myPreferences/myPreferences.component';
import {CoreServicesModule} from '../core-services/core-services.module';
import {ComponentsModule} from '../framework/components/components.module';
import {AuthGuard} from '../core-services/auth.guard';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from '../core-services/token.interceptor.service';
import {ToastrModule, ToastrService} from 'ngx-toastr';

@NgModule({
    declarations: [DashboardComponent, AptComponent, DatamapIComponent, DatamapAComponent,
        ListingAttackCampaignComponent, ListingGroupsComponent, AptGroupsComponent,
        AptCampaignsComponent, AptGroupCreateFormComponent, AptGroupUpdateFormComponent,
        AptCampaignsAddFormComponent, AptCampaignsUpdateFormComponent, MyPreferencesComponent,
        TargetSectorComponent, TargetRegionComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AptRoutingModule,
        CoreServicesModule,
        NgxSpinnerModule,
        ComponentsModule,
        TooltipModule,
        TableModule,
        CardModule,
        CalendarModule,
        BreadcrumbModule,
        AvatarModule,
        ChartModule,
        ChipsModule,
        AutoCompleteModule,
        DynamicDialogModule,
        ConfirmDialogModule,
        TagModule,
        InputSwitchModule,
        AccordionModule,
        TabViewModule,
        MultiSelectModule,
        NgxDaterangepickerMd.forRoot(),
        AngularMultiSelectModule,
        NgxEchartsModule.forRoot({
            echarts,
        }),
        ToastrModule
    ],
    providers: [AuthGuard, {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true,
    },
        ToastrService]
})
export class AptModule {
}
