import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {ConfirmationService, MenuItem} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import 'd3';
import {ToastrService} from "ngx-toastr";
import {NgxSpinnerService} from "ngx-spinner";
import {AptService} from '../../core-services/apt.service';
declare var colors: any;
declare var Datamap: any;
declare var d3: any;
@Component({
  selector: 'app-apt-campaigns',
  templateUrl: './apt-campaigns.component.html',
  styleUrls: ['./apt-campaigns.component.scss'],
  providers: [ConfirmationService]
})
export class AptCampaignsComponent implements OnInit {
  options: any = {};
  details: any;
  showTable: any;
  spin: any = true;
  alertId: any;
  ssl: any;
  ssh: any;
  showIoC: any  = false;
  expire: any;
  ports: any = [];
  data: any = [];
  groupDetails: any = [];
  dates: any = [];
  counts: any = [];
  mitreDetails: any = [];
  send: any;
  showChart: any = false;
  breadCrumbs: MenuItem[];
  elections: any ;
  election: any;
  mapData: any;
  role: any;
  showRole: any = false;
  reporttype: any;
  downloadType: any;
  response: any;
  errors: any;
  attprofile: any;
  groupId: any;

  export_reporting: any = false;
  domains = [];
  hashes = [];
  ips = [];
  mutexes = [];
  paths = [];
  process = [];
  registries = [];
  urls = [];
  iocs = [];
  fileName = [];
  hostName = [];
  cve = [];
  email = [];

  reporttypes: any;
  downloadTypes: any;
  export_reportings: any = false;

  datacombo: any;

  chartOptionscombo: any;
  constructor(private router: Router, private datePipe: DatePipe,
              private toastr: ToastrService, private spinner: NgxSpinnerService,
              private searchApiService: AptService,  private confirmationService: ConfirmationService,
              private route: ActivatedRoute, private aptService: AptService) {
    this.aptService.saveAitem(true);

    this.mapData = [];
    this.elections = [];
    this.breadCrumbs = [];
  }
  downloadProfile (groupName, details) {
    this.export_reportings = true;
    this.spinner.show('aptAttackCampaigns-spinner');

    this.reporttypes = 'application/pdf';
    this.downloadTypes =  groupName + '.pdf';
    this.aptService.downloadaptCampaignProfile(details)
      .subscribe(
        res => {
          this.response = res;
          const file = new Blob([this.response], {
            type: this.reporttypes,
          });
          const fileURL = window.URL.createObjectURL(file);
          const link = document.createElement('a');
          link.href = fileURL;
          link.download = this.downloadTypes;
          document.body.appendChild(link);

          link.click();
          this.export_reportings = false;
          this.spinner.hide('aptAttackCampaigns-spinner');

          this.toastr.success('Profile download successfully');
        },
        err => {
          this.export_reportings = false;
          this.spinner.hide('aptAttackCampaigns-spinner');

          this.errors = err.status;
          if (this.errors === 400) {
            this.toastr.error(this.errors.error.message);
          } else if (this.errors === 500) {
            this.toastr.error('Some error occurred. Please try again later');

          }
        });
  }

  downloadAttack (caseName, alertId) {
    this.export_reporting = true;
    this.spinner.show('aptAttackCampaigns-spinner');

    this.reporttype = 'application/csv';
    this.downloadType =  caseName + '.csv';
    this.aptService.downloadaptIOCs(alertId)
      .subscribe(
        res => {
          this.response = res;
          const file = new Blob([this.response], {
            type: this.reporttype,
          });
          const fileURL = window.URL.createObjectURL(file);
          const link = document.createElement('a');
          link.href = fileURL;
          link.download = this.downloadType;
          document.body.appendChild(link);

          link.click();
          this.export_reporting = false;
          this.spinner.hide('aptAttackCampaigns-spinner');

          this.toastr.success('IoCs Downloaded Successfully');
        },
        err => {
          this.export_reporting = false;
          this.spinner.hide('aptAttackCampaigns-spinner');

          this.errors = err.status;
          if (this.errors === 400) {
            this.toastr.error(this.errors.error.message);
          } else if (this.errors === 500) {
            this.toastr.error('Some error occurred. Please try again later');

          }
        });
  }
  ngOnInit() {

    this.alertId = this.route.snapshot.queryParamMap.get('id');
    this.groupId = this.route.snapshot.queryParamMap.get('groupId');
    if (this.route.snapshot.queryParams['lastPage']) {
      const lastPage = this.route.snapshot.queryParams['lastPage'];
      switch (lastPage) {
        case 'Dashboard':
          this.breadCrumbs.push({label: lastPage, routerLink: '/apt/dashboard', queryParams: {send: true}, routerLinkActiveOptions: true});
          break;
        case 'Attacks/Campaigns Listing' :
          this.breadCrumbs.push({label: lastPage, routerLink: '/apt/attacksCampaignList', queryParams: {send: false}, routerLinkActiveOptions: true});
          break;
        case 'Create Attack/Campaign' :
          this.breadCrumbs.push({label: lastPage, routerLink: '/apt/attacks/campaigns', queryParams: {send: false}, routerLinkActiveOptions: true});
          break;
        case 'Update Attack/Campaign' :
          this.breadCrumbs.push({label: lastPage, routerLink: '/apt/updateAttacksCampaign', queryParams: {id: this.alertId, send: false}, routerLinkActiveOptions: true});
          break;
        case 'Group Details' :
          this.breadCrumbs.push({label: lastPage, routerLink: '/apt/viewGroup', queryParams: {id: this.groupId, send: false}, routerLinkActiveOptions: true});
          break;
      }
      this.breadCrumbs.push({label: 'Attack/Campaign Details'});
    }
    this.role = sessionStorage.getItem('role');
    if (this.role === 'ROLE_SOC_ADMIN' || this.role === 'ROLE_L1_ANALYST' || this.role === 'ROLE_L2_ANALYST') {
      this.showRole = true;

    }
    else if (this.role === 'ROLE_CLIENT_ADMIN' || this.role === 'ROLE_CLIENT_ANALYST') {
      this.showRole = false;
    }
  }
  onNoClick(): void {
    this.send = this.route.snapshot.queryParamMap.get('send');
    if (this.send === 'false') {
      this.router.navigate(['apt/attacksCampaignList']);
    } else {
      this.router.navigate(['apt/dashboard']);

    }
  }
  ngAfterViewInit() {
    this.getdata();
  }
  getdata() {
    this.spinner.show('aptAttackCampaigns-spinner');
    this.counts = [];
    this.dates = [];
    this.domains = [];
    this.hashes = [];
    this.ips = [];
    this.mutexes = [];
    this.paths = [];
    this.process = [];
    this.registries = [];
    this.urls = [];
    this.iocs = [];
    this.fileName = [];
    this.hostName = [];
    this.cve = [];
    this.email = [];
    this.showIoC = false;
    this.aptService.getAttackCampaign(this.alertId).subscribe(
      res => {
        this.spin = false;
        this.spinner.hide('aptAttackCampaigns-spinner');

        this.details = res;
        this.attprofile = res;
        const colors = [
          "#0d552c",
          "#17753f",
          "#ea4040",
          "#499b59",
          "#37ba6f",
          "#45cc7f",
          "#58e092",
          "#6ceda3",
          "#93ffc1",
          "#bdffd9",
          "#e7fff1",
        ];
        let prev = 0, color, hits;
        this.mapData = [];
        if (this.details && this.details.countries && this.details.countries.length > 0) {
          for (let countryIndex = 0; countryIndex < this.details.countries.length; countryIndex++) {
            var obj = {
              fillColor: colors[1],
              country: this.details.countries[countryIndex].countryName,
              countryCode: this.details.countries[countryIndex].cca3
            };
            this.mapData[this.details.countries[countryIndex].cca3] = obj;
            if (obj.countryCode === 'KOR' || obj.countryCode === 'kor') {
              obj.country = 'South Korea';
            }
            if (obj.countryCode === 'PRK' || obj.countryCode === 'prk') {
              obj.country = 'North Korea';

            }
            this.elections.push(obj);
          }
        }
        this.election =  new Datamap({
          element: document.getElementById('container1'),
          projection: 'mercator',
          scope: 'world',
          width: 650,
          height: 530,
          options: {
            staticGeoData: false,
            legendHeight: 200,
            legendwidth: 300 // optionally set the padding for the legend
          },
          fills: {defaultFill: '#0d552c'},
          data: this.mapData,
          geographyConfig: {
            borderColor: '#DEDEDE',
            highlightBorderWidth: 2,
            highlightOnHover: false,
            highlightBorderColor: '#B7B7B7',
            popupTemplate: function (geo, data) {
              if (!data) {
                return ['<div class="hoverinfo">',
                  '<strong>', geo.properties.name, '</strong>',
                  '</div>'].join('');
              }
              // tooltip content
              return ['<div class="hoverinfo">',
                '<strong>', geo.properties.name, '</strong>',
                '</div>'].join('');
            }
          }
        });
        if (this.details && this.details.responseDto) {
          if (this.details.responseDto.responseCode && this.details.responseDto.responseCode === Number(200)) {
            this.details = res;
            if (this.details.groups && this.details.groups.length) {
              this.groupDetails = this.details.groups.map(item => item.groupName);
              this.mitreDetails = this.details.groups.map(item => item.mitreName);
            }
            if (this.details.iocs && this.details.iocs.length > 0) {
              this.showIoC = true;

              this.details.iocs.map(item => {
                const sdate = this.datePipe.transform(item.date, 'yyyy-MM');

                if (item.iocType.id === 1) {
                  if (!this.ips[sdate]) {
                    this.ips[sdate] = [];
                  }
                  this.ips[sdate].push(item);
                  this.ips.length = 1;

                } else  if (item.iocType.id === 2) {
                  if (!this.domains[sdate]) {
                    this.domains[sdate] = [];
                  }
                  this.domains[sdate].push(item);
                  this.domains.length = 1;

                }  else  if (item.iocType.id === 3) {
                  if (!this.hashes[sdate]) {
                    this.hashes[sdate] = [];
                  }
                  this.hashes[sdate].push(item);
                  this.hashes.length = 1;
                } else  if (item.iocType.id === 4) {
                  if (!this.mutexes[sdate]) {
                    this.mutexes[sdate] = [];
                  }
                  this.mutexes[sdate].push(item);
                  this.mutexes.length = 1;

                } else  if (item.iocType.id === 5) {
                  if (!this.paths[sdate]) {
                    this.paths[sdate] = [];
                  }
                  this.paths[sdate].push(item);
                  this.paths.length = 1;

                } else  if (item.iocType.id === 6) {
                  if (!this.process[sdate]) {
                    this.process[sdate] = [];
                  }
                  this.process[sdate].push(item);
                  this.process.length = 1;

                } else  if (item.iocType.id === 7) {
                  if (!this.urls[sdate]) {
                    this.urls[sdate] = [];
                  }
                  this.urls[sdate].push(item);
                  this.urls.length = 1;

                } else  if (item.iocType.id === 8) {
                  if (!this.registries[sdate]) {
                    this.registries[sdate] = [];
                  }
                  this.registries[sdate].push(item);
                  this.registries.length = 1;

                } else  if (item.iocType.id === 9) {
                  if (!this.hostName[sdate]) {
                    this.hostName[sdate] = [];
                  }
                  this.hostName[sdate].push(item);
                  this.hostName.length = 1;

                } else  if (item.iocType.id === 10) {
                  if (!this.email[sdate]) {
                    this.email[sdate] = [];
                  }
                  this.email[sdate].push(item);
                  this.email.length = 1;

                } else  if (item.iocType.id === 11) {
                  if (!this.cve[sdate]) {
                    this.cve[sdate] = [];
                  }
                  this.cve[sdate].push(item);
                  this.cve.length = 1;

                }else  if (item.iocType.id === 12) {
                  if (!this.fileName[sdate]) {
                    this.fileName[sdate] = [];
                  }
                  this.fileName[sdate].push(item);
                  this.fileName.length = 1;

                }
              });

            } else {
              this.showIoC = false;
            }
            if (this.details.timelines && this.details.timelines.length > 0) {
              this.showChart = true;
              for (const data of this.details.timelines) {
                data.date = data.date.split('T');

                const from = this.datePipe.transform(data.date[0], 'MM/yyyy');
                this.dates.push(from);
                this.counts.push(data.count);
              }
              this.charts(this.dates, this.counts);
            } else {
              this.showChart = false;
            }
            this.showTable = true;
          }
        } else {
          this.showTable = false;
          this.showChart = false;
          this.router.navigate(['apt/attacksCampaignList']);

        }


      }, err => {
        this.spin = false;
          this.spinner.hide('aptAttackCampaigns-spinner');

          this.showTable = false;
        this.showIoC = false;
        this.showChart = false;
          this.toastr.error('Some error occurred. Please try again later');

      }
    );
  }
  charts(dates, counts) {
    this.datacombo = {
      labels: this.dates,
      datasets: [
        {
        type: 'line',
        borderColor: '#42A5F5',
        borderWidth: 2,
        fill: false,
        data: this.counts
      }, {
        type: 'bar',
        label: 'IOC Counts',
        backgroundColor: '#FFA726',
        data: this.counts
      }]
    };
    this.chartOptionscombo = {
      responsive: true,
      title: {
        display: true,
        text: 'Indicator Timeline'
      },
      tooltips: {
        mode: 'index',
        intersect: true,
        filter: function (tooltipItem) {
          return tooltipItem.datasetIndex === 1;
        }
      },
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          barPercentage: 0.2
        }],
        zoom: {
          enabled: false,
        },
        yAxes: [{
          ticks: {
            min: 0
          }
        }]
      }
    };
  }
  search(type, value) {
    if (type === 'Hash') {
    type = 'FileHash-MD5';
    }
    window.open('tip/search-ioc?type=' + type + '&value=' + value + '&submitSearch=false', '_blank');

  }
  deleteAttackCampaign(alertid) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this Attack/Campaign?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.aptService.deleteAttackCampaign(alertid).subscribe(
          res => {
            this.toastr.success('Attack/Campaign Deleted Successfully');

            this.router.navigate(['apt/attacksCampaignList']);
          },
          err => {
            this.toastr.error('Some error occurred. Please try again later');

          }
        );
      },
      reject: () => {
      }
    });

  }

  updateAttackCampaign(alert) {
    this.router.navigate(['apt/updateAttacksCampaign'], {queryParams: {id: alert}});
  }
  openSite(siteUrl) {
    window.open( siteUrl, '_blank');
  }
}
