import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AptComponent} from './apt.component';
import {ListingAttackCampaignComponent} from './listingAttackCampaign/listingAttackCampaign.component';
import {ListingGroupsComponent} from './listingGroups/listingGroups.component';
import {AptGroupsComponent} from './apt-groups/apt-groups.component';
import {AptCampaignsComponent} from './apt-campaigns/apt-campaigns.component';
import {AptGroupCreateFormComponent} from './apt-group-create-form/apt-group-create-form.component';
import {AptGroupUpdateFormComponent} from './apt-group-update-form/apt-group-update-form.component';
import {AptCampaignsAddFormComponent} from './apt-campaigns-create-form/apt-campaigns-add-form.component';
import {AptCampaignsUpdateFormComponent} from './apt-campaigns-update-form/apt-campaigns-update-form.component';
import {MyPreferencesComponent} from './myPreferences/myPreferences.component';
const routes: Routes = [
  {
    path: '',
    component: AptComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'groupList',
        component: ListingGroupsComponent
      },
      {
        path: 'viewGroup',
        component: AptGroupsComponent
      },
      {
        path: 'groups',
        component: AptGroupCreateFormComponent
      },
      {
        path: 'updateGroup',
        component: AptGroupUpdateFormComponent
      },
      {
        path: 'attacksCampaignList',
        component: ListingAttackCampaignComponent
      },
      {
        path: 'attacks/campaigns',
        component: AptCampaignsAddFormComponent
      },
      {
        path: 'updateAttacksCampaign',
        component: AptCampaignsUpdateFormComponent
      },

      {
        path: 'viewCampaign',
        component: AptCampaignsComponent,
      },
      {
        path: 'myPreferences',
        component: MyPreferencesComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AptRoutingModule { }
