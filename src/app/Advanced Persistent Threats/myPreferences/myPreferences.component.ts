import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ConfirmationService, MessageService} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup} from "@angular/forms";
import {DatePipe} from "@angular/common";
import {ToastrService} from "ngx-toastr";
import {NgxSpinnerService} from "ngx-spinner";
import 'd3';
import {AptService} from '../../core-services/apt.service';
import {CalendarSettingService} from '../../core-services/calendarSetting.service';
declare var colors: any;
declare var Datamap: any;
declare var d3: any;

@Component({
  selector: 'app-myPreferences',
  templateUrl: './myPreferences.component.html',
  styleUrls: ['./myPreferences.component.scss'],
  providers: [ConfirmationService]
})
export class MyPreferencesComponent implements OnInit, AfterViewInit {
  requestIncident: any;
  showTable: any;
  spin: any = true;
  see = false;
  sectorResult: any;
  regionResult: any;
  data: any = {};
  selectedCountries1 = [];
  selectsector: any[] = [];
  selectedCountries: any[] = [];

  attackRegionLastProcessed: any;
  attackSectorLastProcessed: any;
  groupRegionLastProcessed: any;
  groupSectorLastProcessed: any;

  attackRegionFrequency: any;
  attackSectorFrequency: any;
  groupRegionFrequency: any;
  groupSectorFrequency: any;
  elections: any ;
  preferences: any ;
  election: any;
  mapData: any;
  viewcountries: any;
  itemList = [];
  selectedItems = [];
  countries = [];
  settings: any;
  addOnBlur = true;
  post = true;
  userId: any;
  days: any;
  alerts: any;
  constructor(private aptService: AptService, private toastr: ToastrService, private spinner: NgxSpinnerService,
              private calendarSetting: CalendarSettingService) {
    this.sectorResult = [];
    this.days = [
      {label: '1', value: '1'},
      {label: '3', value: '3'},
      {label: '7', value: '7'},
      {label: '15', value: '15'},
      {label: '30', value: '30'},
    ];
    this.alerts = [
      {label: 'When a Group Targeting my Sectors is added or updated.', value: 'groupSector', selected: false,
        frequency: 'groupSectorFrequency', frequencyValue: this.days[2].value,
        processed: 'groupSectorLastProcessed', processedValue: new Date(),
      },
      {label: 'When a Group Targeting my Regions is added or updated.', value: 'groupRegion', selected: false,
        frequency: 'groupRegionFrequency', frequencyValue: this.days[2].value,
        processed: 'groupRegionLastProcessed', processedValue: new Date(),
      },
      {label: 'When an Attack/Campaign Targeting my Sectors is added or updated.', value: 'campaignSector', selected: false,
       frequency: 'attackSectorFrequency', frequencyValue: this.days[2].value,
        processed: 'attackSectorLastProcessed', processedValue: new Date()},
      {label: 'When an Attack/Campaign Targeting my Regions is added or updated.', value: 'campaignRegion', selected: false,
        frequency: 'attackRegionFrequency', frequencyValue: this.days[2].value,
        processed: 'attackRegionLastProcessed', processedValue: new Date()},
    ];
    this.groupSectorFrequency = this.alerts[0].frequencyValue;
    this.groupRegionFrequency = this.alerts[1].frequencyValue;
    this.attackSectorFrequency = this.alerts[2].frequencyValue;
    this.attackRegionFrequency = this.alerts[3].frequencyValue;

  }

  ngOnInit() {
    this.itemList = [];
    this.sectorResult = [];
    this.selectsector = [];
    this.selectedCountries1 = [];
    this.selectedItems = [];
    this.selectedCountries = [];
    this.countries = [];
    this.mapData = [];
    this.elections = [];
    this.viewcountries = [];
    this.map();
    this.getLookupServices();
  }
  switchFrequency(selectedday, frequency) {
    switch (frequency) {
      case 'groupSectorFrequency':
        this.groupSectorFrequency = selectedday;
        break;
      case 'groupRegionFrequency':
        this.groupRegionFrequency = selectedday;
        break;
      case 'attackSectorFrequency':
        this.attackSectorFrequency = selectedday;
        break;
      case 'attackRegionFrequency':
        this.attackRegionFrequency = selectedday;
        break;

    }
  }

  ngAfterViewInit() {
  }

  search() {
    this.spin = true;
    const sectorP = [];
    const regionP = [];
    for (let sector of this.sectorResult) {
      const sec = {
        "enabled": sector.selected,
        "sector": sector.id,
        "value": sector.itemName
      };
      sectorP.push(sec);
    }
    for (let sector of this.itemList) {
      const sec = {
        "enabled": sector.selected,
        "value": sector.category
      };
      regionP.push(sec);
    }
    if (this.post === true) {
      const body =  {
        "regionPreferences": regionP,
        "sectorPreferences": sectorP,
        "campaignRegion": this.alerts[3].selected,
        "campaignSector": this.alerts[2].selected,
        "groupRegion": this.alerts[1].selected,
        "groupSector": this.alerts[0].selected,
        'groupSectorLastProcessed': this.groupSectorLastProcessed,
      'groupRegionLastProcessed': this.groupRegionLastProcessed,
      'attackSectorLastProcessed': this.attackSectorLastProcessed,
     'attackRegionLastProcessed': this.attackRegionLastProcessed,
      'groupSectorFrequency': this.groupSectorFrequency,
      'groupRegionFrequency': this.groupRegionFrequency,
      'attackSectorFrequency': this.attackSectorFrequency,
      'attackRegionFrequency': this.attackRegionFrequency,
      };
      this.spinner.show('preferences-spinner');

      this.aptService.postpreferences(body).subscribe(
        res => {
          this.spin = false;
          this.spinner.hide('preferences-spinner');

          this.toastr.success('Preferences Set Successfully');

        }, err => {
          this.spin = false;
            this.spinner.hide('preferences-spinner');
            this.toastr.error('Some error occurred Please try again.')
        }
      );
    } else {
     const body =  {
        "regionPreferences": regionP,
        "sectorPreferences": sectorP,
        "userId": this.userId,
       "campaignRegion": this.alerts[3].selected,
       "campaignSector": this.alerts[2].selected,
       "groupRegion": this.alerts[1].selected,
       "groupSector": this.alerts[0].selected,
       'groupSectorLastProcessed': this.groupSectorLastProcessed,
       'groupRegionLastProcessed': this.groupRegionLastProcessed,
       'attackSectorLastProcessed': this.attackSectorLastProcessed,
       'attackRegionLastProcessed': this.attackRegionLastProcessed,
       'groupSectorFrequency': this.groupSectorFrequency,
       'groupRegionFrequency': this.groupRegionFrequency,
       'attackSectorFrequency': this.attackSectorFrequency,
       'attackRegionFrequency': this.attackRegionFrequency,
      };
      this.spinner.show('preferences-spinner');
      this.aptService.putpreferences(body).subscribe(
        res => {
          this.spin = false;
          this.spinner.hide('preferences-spinner');
          this.toastr.success('Preferences Updated Successfully');
        }, err => {
            this.spin = false;
            this.spinner.hide('preferences-spinner');
            this.toastr.error('Some error occurred Please try again.')
          }
      );
    }
  }


  handleRegions(event, category) {
    if (event.checked === true) {
      this.selectedItems.push(category);
      for (let region of this.regionResult) {
        if (region.region === category) {
          this.viewcountries.push(region);
        }
      }
      this.updatemap();

    } else if (event.checked === false) {
      const index = this.selectedItems.findIndex(x => x === category);
      if (index !== -1) {
        this.selectedItems.splice(index, 1);
      }
      if (this.viewcountries.length > 0) {
       const ar = this.viewcountries.filter(item => item.region !== category);
       this.viewcountries = ar;
      }
      this.updatemap();

    }
  }

  handleSectors(event, id) {
    if (event.checked === true) {
      const index = this.selectedCountries1.findIndex(x => x === id);
      if (index === -1) {
        this.selectedCountries1.push(id);
      }
    } else if (event.checked === false) {
      const index = this.selectedCountries1.findIndex(x => x === id);
      if (index !== -1) {
        this.selectedCountries1.splice(index, 1);
      }
    }

  }


  getLookupServices() {
    this.spinner.show('preferences-spinner');
    this.aptService.getpreferences().subscribe(
      res => {
        this.preferences = res;
        this.spinner.hide('preferences-spinner');
        if (this.preferences && (this.preferences.responseDto.responseCode === 200 ||
          this.preferences.responseDto.responseCode === '200' )) {
          this.alerts[0].selected = this.preferences.groupSector;
          this.alerts[1].selected = this.preferences.groupRegion;
          this.alerts[2].selected = this.preferences.campaignSector;
          this.alerts[3].selected = this.preferences.campaignRegion;
          this.groupSectorLastProcessed = this.alerts[0].processedValue = this.preferences.groupSectorLastProcessed;
          this.groupRegionLastProcessed = this.alerts[1].processedValue = this.preferences.groupRegionLastProcessed;
          this.attackSectorLastProcessed = this.alerts[2].processedValue = this.preferences.attackSectorLastProcessed;
          this.attackRegionLastProcessed = this.alerts[3].processedValue = this.preferences.attackRegionLastProcessed;
          this.groupSectorFrequency = this.alerts[0].frequencyValue = this.preferences.groupSectorFrequency.toString();
          this.groupRegionFrequency = this.alerts[1].frequencyValue = this.preferences.groupRegionFrequency.toString();
          this.attackSectorFrequency = this.alerts[2].frequencyValue = this.preferences.attackSectorFrequency.toString();
          this.attackRegionFrequency = this.alerts[3].frequencyValue = this.preferences.attackRegionFrequency.toString();
        }

        this.aptService.getSectors().subscribe(
          res => {
            this.see = false;
            this.spin = false;
            const sectorResponse: any = res;
            const sectorsArray = [];

            this.sectorResult = [];
            this.selectsector = [];
            if (sectorResponse && sectorResponse.length > 0) {
              let value = true;
              for (let sec of sectorResponse) {
                const data = {
                  id: sec.id,
                  itemName: sec.value,
                  selected: value,
                };
                sectorsArray.push(data);
              }
              this.sectorResult = sectorsArray;
            } else {
              this.see = true;
              this.sectorResult = [];
            }
            if (this.preferences && (this.preferences.responseDto.responseCode === 200 ||
              this.preferences.responseDto.responseCode === '200' )) {
              this.post = false;
              this.userId = this.preferences.userId;
              if (this.preferences.sectorPreferences && this.preferences.sectorPreferences.length > 0) {
                for (let sector of this.sectorResult) {
                  for (let prefer of this.preferences.sectorPreferences) {
                    if (sector.id === prefer.sector) {
                      sector.selected = prefer.enabled;
                    }
                  }

                }
              }
            } else {
              this.post = true;
            }

            },
          err => {
          }
        );
        this.aptService.getRegions().subscribe(
          res => {
            const regionResponse: any = res;
            let value;
            if (regionResponse && regionResponse.length > 0) {
              this.regionResult = regionResponse;
              this.viewcountries = regionResponse;
              for (let region of this.regionResult) {
                const data = {
                  id: region.id,
                  itemName: region.countryName,
                  category: region.region,
                  selected: true,

                };
                if (this.itemList.find((test) => test.category === data.category) === undefined) {
                  this.itemList.push(data);
                }
              }
              if (this.preferences && (this.preferences.responseDto.responseCode === 200 ||
                this.preferences.responseDto.responseCode === '200' )) {
                if (this.preferences.regionPreferences && this.preferences.regionPreferences.length > 0) {
                  for (let sector of this.itemList) {
                    for (let prefer of this.preferences.regionPreferences) {
                      if (sector.category === prefer.value) {
                        sector.selected = prefer.enabled;
                      }
                    }
                  }
                }
              }
              for (let reg of this.itemList) {
                if (reg.selected === false) {
                  const ar = this.viewcountries.filter(item => item.region !== reg.category);
                  this.viewcountries = ar;
                }
              }
              this.updatemap();
            } else {
              this.itemList = [];
            }
          },
          err => {
          }
        );
      },
        err => {
          this.spinner.hide('preferences-spinner');

        this.aptService.getSectors().subscribe(
          res => {
            this.see = false;
            this.spin = false;
            const sectorResponse: any = res;
            const sectorsArray = [];

            this.sectorResult = [];
            this.selectsector = [];
            if (sectorResponse && sectorResponse.length > 0) {
              let value = true;
              for (let sec of sectorResponse) {
                const data = {
                  id: sec.id,
                  itemName: sec.value,
                  selected: value,
                };
                sectorsArray.push(data);
              }
              this.sectorResult = sectorsArray;
            } else {
              this.see = true;
              this.sectorResult = [];
            }

          },
          err => {
          }
        );
        this.aptService.getRegions().subscribe(
          res => {
            const regionResponse: any = res;
            let value;
            if (regionResponse && regionResponse.length > 0) {
              this.regionResult = regionResponse;
              this.viewcountries = regionResponse;
              for (let region of this.regionResult) {
                const data = {
                  id: region.id,
                  itemName: region.countryName,
                  category: region.region,
                  selected: true,

                };
                if (this.itemList.find((test) => test.category === data.category) === undefined) {
                  this.itemList.push(data);
                }
              }
              for (let reg of this.itemList) {
                if (reg.selected === false) {
                  const ar = this.viewcountries.filter(item => item.region !== reg.category);
                  this.viewcountries = ar;
                }
              }
              this.updatemap();
            } else {
              this.itemList = [];
            }
          },
          err => {
          }
        );
      }
    );


  }
  updatemap() {
    const colors = [
      "#0d552c",
      "#17753f",
      "#ea4040",
      "#499b59",
      "#37ba6f",
      "#45cc7f",
      "#58e092",
      "#6ceda3",
      "#93ffc1",
      "#bdffd9",
      "#e7fff1",
    ];
    let prev = 0, color, hits;
    this.mapData = [];
    this.elections = [];
    this.election.updateChoropleth(null, {reset: true})
     for (let viewCountryIndex = 0; viewCountryIndex < this.viewcountries.length; viewCountryIndex++) {
        var obj = {
          fillColor: colors[1],
          country: this.viewcountries[viewCountryIndex].countryName,
          countryCode: this.viewcountries[viewCountryIndex].cca3
        };
        this.mapData[this.viewcountries[viewCountryIndex].cca3] = obj;
        if (obj.countryCode === 'KOR' || obj.countryCode === 'kor') {
          obj.country = 'South Korea'
        }
        if (obj.countryCode === 'PRK'|| obj.countryCode === 'prk') {
          obj.country = 'North Korea'

        }

        //////.log($scope.countries);
        this.elections.push(obj);
      }
      this.election.updateChoropleth(this.mapData);


  }
  map() {
    const colors = [
      "#0d552c",
      "#17753f",
    ];
    let prev = 0, color, hits;
    this.mapData = [];
    this.elections = [];
    for (let viewCountryIndex = 0; viewCountryIndex < this.viewcountries.length; viewCountryIndex++) {
      var obj = {
        fillColor: colors[1],
        country: this.viewcountries[viewCountryIndex].countryName,
        countryCode: this.viewcountries[viewCountryIndex].cca3
      };
      this.mapData[this.viewcountries[viewCountryIndex].cca3] = obj;
      if (obj.countryCode === 'KOR' || obj.countryCode === 'kor') {
        obj.country = 'South Korea'
      }
      if (obj.countryCode === 'PRK'|| obj.countryCode === 'prk') {
        obj.country = 'North Korea'

      }
      this.elections.push(obj);
    }
    this.election =  new Datamap({
      element: document.getElementById('prefrencesRegion'),
      projection: 'mercator',
      scope: 'world',
      width: 650,
      height: 530,
      options: {
        staticGeoData: false,
        legendHeight: 200,
        legendwidth: 300 // optionally set the padding for the legend
      },
      fills: {defaultFill: '#0d552c'},

      data: this.mapData,
      geographyConfig: {
        borderColor: '#DEDEDE',
        highlightBorderWidth: 2,
        highlightOnHover: false,
        highlightBorderColor: '#B7B7B7',
        popupTemplate: function (geo, data) {
          if (!data) {
            return ['<div class="hoverinfo">',
              '<strong>', geo.properties.name, '</strong>',
              '</div>'].join('');
          }
          // tooltip content
          return ['<div class="hoverinfo">',
            '<strong>', geo.properties.name, '</strong>',
            '</div>'].join('');
        }
      }
    });

  }
}
