import {Component, Inject, OnInit} from '@angular/core';
import {ConfirmationService, MenuItem, MessageService} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import 'd3';
import {NgxSpinnerService} from "ngx-spinner";
import {ToastrService} from "ngx-toastr";
import {AptService} from '../../core-services/apt.service';
declare var colors: any;
declare var Datamap: any;
declare var d3: any;
@Component({
  selector: 'app-apt-groups',
  templateUrl: './apt-groups.component.html',
  styleUrls: ['./apt-groups.component.scss'],
  providers: [ConfirmationService]
})
export class AptGroupsComponent implements OnInit {
  details: any;
  showTable: any;
  spin: any = true;
  alertId: any;
  ssl: any;
  ssh: any;
  expire: any;
  ports : any = [];
  data : any = [];
  mitreInitial: any = [];
  mitreExecution: any = [];
  mitrePersistence: any = [];
  mitrePrivilegeEscalation: any = [];
  mitreDefenseEvasion: any = [];
  mitreReconnaissance: any = [];
  mitreResourceDevelopment: any = [];
  mitreCredentialAccess: any = [];
  mitreDiscovery: any = [];
  mitreLateralMovement: any = [];
  mitreCollection: any = [];
  mitreCommandAndControl: any = [];
  mitreExfiltration: any = [];
  mitreImpact: any = [];
  send: any;
  breadCrumbs: MenuItem[];

  reporttype: any;
  downloadType: any;
  response: any;
  errors: any;
  export_reporting: any = false;
  elections: any ;
  election: any;
  mapData: any;
  role: any;
  campaignId: any;
  showRole: any = false;
  constructor(private router: Router,
              private spinner: NgxSpinnerService, private taostr: ToastrService,
              private confirmationService: ConfirmationService,
              private route: ActivatedRoute, private aptService: AptService) {
    this.aptService.saveGitem(true);
    this.mapData = [];
    this.elections = [];
    this.breadCrumbs = [];

  }

  ngOnInit() {
    this.alertId = this.route.snapshot.queryParamMap.get('id');
    if (this.route.snapshot.queryParams['lastPage']) {
      const lastPage = this.route.snapshot.queryParams['lastPage'];
      switch (lastPage) {
        case 'Dashboard':
          this.breadCrumbs.push({label: lastPage, routerLink: '/apt/dashboard', routerLinkActiveOptions: true});
          break;
        case 'Groups Listing' :
          this.breadCrumbs.push({label: lastPage, routerLink: '/apt/groupList', routerLinkActiveOptions: true});
          break;
        case 'Create Group' :
          this.breadCrumbs.push({label: lastPage, routerLink: '/apt/createGroup', routerLinkActiveOptions: true});
          break;
        case 'Update Group' :
          this.breadCrumbs.push({label: lastPage, routerLink: '/apt/updateGroup', queryParams: {id: this.alertId}, routerLinkActiveOptions: true});
          break;

      }
      this.breadCrumbs.push({label: 'Group Details'});
    }
    this.role = sessionStorage.getItem('role');
    if (this.role === 'ROLE_SOC_ADMIN' || this.role === 'ROLE_L1_ANALYST' || this.role === 'ROLE_L2_ANALYST') {
      this.showRole = true;

    } else if (this.role === 'ROLE_CLIENT_ADMIN' || this.role === 'ROLE_CLIENT_ANALYST') {
      this.showRole = false;
    }
  }
  onNoClick(): void {
    this.send = this.route.snapshot.queryParamMap.get('send');
    if (this.send === 'false') {
      this.router.navigate(['apt/groupList']);
    } else {
      this.router.navigate(['apt/dashboard']);

    }
  }
  viewAttackCampaign(id) {
    this.router.navigate(['apt/viewCampaign'], {queryParams: {id: id, groupId: this.alertId,  send: false, lastPage: 'Group Details'}});

  }
  downloadProfile (groupName, details) {
    this.export_reporting = true;
    this.spinner.show('aptgroups-spinner');

    this.reporttype = 'application/pdf';
    this.downloadType =  groupName + '.pdf';
    this.aptService.downloadaptGroupProfile(details)
      .subscribe(
        res => {
          this.response = res;
          const file = new Blob([this.response], {
            type: this.reporttype,
          });
          const fileURL = window.URL.createObjectURL(file);
          const link = document.createElement('a');
          link.href = fileURL;
          link.download = this.downloadType;
          document.body.appendChild(link);

          link.click();
          this.export_reporting = false;
          this.spinner.hide('aptgroups-spinner');

          this.taostr.success('Profile Downloaded Successfully');
          },
        err => {
          this.export_reporting = false;
          this.spinner.hide('aptgroups-spinner');

          this.errors = err.status;
          if (this.errors === 400) {
            this.taostr.error(this.errors.error.message);
          } else if (this.errors === 500) {
           this.taostr.error('Some error occurred. Please try again later');
          }
        });
  }

  deleteGroups(alertid) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this Groups?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.aptService.deleteGroups(alertid).subscribe(
          res => {
            this.taostr.success('Group Deleted Successfully');
            this.router.navigate(['apt/groupList']);
          },
          err => {
            this.taostr.error('Some error occurred. Please try again later');

          }
        );
      },
      reject: () => {
      }
    });

  }

  updateGroups(alert) {
    this.router.navigate(['apt/updateGroup'], {queryParams: {id: alert}});
  }
  ngAfterViewInit() {
    this.getData();
  }
  getData() {
    this.spinner.show('aptgroups-spinner');

    this.aptService.getGroups(this.alertId).subscribe(
      res => {
        this.spinner.hide('aptgroups-spinner');

        this.spin = false;
        this.details = res;
        const colors = [
          "#0d552c",
          "#17753f",
          "#ea4040",
          "#499b59",
          "#37ba6f",
          "#45cc7f",
          "#58e092",
          "#6ceda3",
          "#93ffc1",
          "#bdffd9",
          "#e7fff1",
        ];
        let prev = 0, color, hits;
        if (this.details.countries) {
          for (let countryIndex = 0; countryIndex < this.details.countries.length; countryIndex++) {
            var obj = {
              fillColor: colors[1],
              country: this.details.countries[countryIndex].countryName,
              countryCode: this.details.countries[countryIndex].cca3
            };
            this.mapData[this.details.countries[countryIndex].cca3] = obj;
            if (obj.countryCode === 'KOR' || obj.countryCode === 'kor') {
              obj.country = 'South Korea'
            }
            if (obj.countryCode === 'PRK'|| obj.countryCode === 'prk') {
              obj.country = 'North Korea'

            }
            this.elections.push(obj);
          }
        }
        this.election =  new Datamap({
          element: document.getElementById('container1'),
          projection: 'mercator',
          scope: 'world',
          width: 650,
          height: 530,
          options: {
            staticGeoData: false,
            legendHeight: 200,
            legendwidth: 300 // optionally set the padding for the legend
          },
          fills: {defaultFill: '#0d552c'},

          data: this.mapData,
          geographyConfig: {
            borderColor: '#DEDEDE',
            highlightBorderWidth: 2,
            highlightOnHover: false,
            highlightBorderColor: '#B7B7B7',
            popupTemplate: function (geo, data) {
              if (!data) {
                return ['<div class="hoverinfo">',
                  '<strong>', geo.properties.name, '</strong>',
                  '</div>'].join('');
                ;
              }
              // tooltip content
              return ['<div class="hoverinfo">',
                '<strong>', geo.properties.name, '</strong>',
                '</div>'].join('');
            }
          }
        });
        if (this.details  && this.details.responseDto) {
          if (this.details.responseDto.responseCode && this.details.responseDto.responseCode === Number(200)) {
            this.details = res;
            this.campaignId = this.details.id;
            if (this.details.techniques && this.details.techniques.length > 0) {
              this.details.techniques.map(item => {
                if (item.techniqueTypes && item.techniqueTypes.length > 0) {
                  item.techniqueTypes.map(techniqueIndex => {
                    if (techniqueIndex.id === 1) {
                      this.mitreInitial.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 1
                        }
                      });
                    } else if (techniqueIndex.id === 2) {
                      this.mitreExecution.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 2
                        }
                      });
                    } else if (techniqueIndex.id === 3) {
                      this.mitrePersistence.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 3
                        }
                      });
                    } else if (techniqueIndex.id === 4) {
                      this.mitrePrivilegeEscalation.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 4
                        }
                      });
                    } else if (techniqueIndex.id === 5) {
                      this.mitreDefenseEvasion.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 5
                        }
                      });
                    } else if (techniqueIndex.id === 6) {
                      this.mitreCredentialAccess.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 6
                        }
                      });
                    } else if (techniqueIndex.id === 7) {
                      this.mitreDiscovery.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 7
                        }
                      });
                    } else if (techniqueIndex.id === 8) {
                      this.mitreLateralMovement.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 8
                        }
                      });
                    } else if (techniqueIndex.id === 9) {
                      this.mitreCollection.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 9
                        }
                      });
                    } else if (techniqueIndex.id === 10) {
                      this.mitreCommandAndControl.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 10
                        }
                      });
                    } else if (techniqueIndex.id === 11) {
                      this.mitreExfiltration.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 11
                        }
                      });
                    } else if (techniqueIndex.id === 12) {
                      this.mitreImpact.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 12
                        }
                      });
                    } else if (techniqueIndex.id === 13) {
                      this.mitreReconnaissance.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 5
                        }
                      });
                    } else if (techniqueIndex.id === 14) {
                      this.mitreResourceDevelopment.push({
                        description: item.description,
                        name: item.name,
                        techniqueId: item.techniqueId,
                        id: item.id,
                        techniqueTypes: {
                          id: 5
                        }
                      });
                    }
                  });
                }
              });
            }
            this.showTable = true;
          }
        } else {
          this.showTable = false;
          this.router.navigate(['apt/groupList']);

        }
      }, err => {
        this.spinner.hide('aptgroups-spinner');
        this.spin = false;
        this.showTable = false;

          this.taostr.error('Some error occurred. Please try again later');

      }
    );
  }
}
