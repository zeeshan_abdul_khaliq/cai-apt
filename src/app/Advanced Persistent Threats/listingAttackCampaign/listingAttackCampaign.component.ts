import {
  AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, Output, ViewChild
} from '@angular/core';
import {ConfirmationService} from "primeng/api";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {FormBuilder, FormGroup} from "@angular/forms";
import {NgxSpinnerService} from "ngx-spinner";
import {ToastrService} from "ngx-toastr";
import {Table} from "primeng/table";
import {DaterangepickerDirective} from "ngx-daterangepicker-material";
import {AptService} from '../../core-services/apt.service';
import {CalendarSettingService} from '../../core-services/calendarSetting.service';

@Component({
  selector: 'app-ListingAttackCampaignComponent',
  templateUrl: './listingAttackCampaign.component.html',
  styleUrls: ['./listingAttackCampaign.component.scss'],
  providers: [ConfirmationService]
})
export class ListingAttackCampaignComponent implements OnInit, AfterViewInit {
  requestIncident: any;
  showTable: any;
  spin: any = true;
  see = false;
  isSoc = false;
  aptForm: FormGroup;
  sectorResult: any;
  regionResult: any;
  data: any = {};
  selectedCountries1 = [];
  filteredCountries: any[] = [];
  selectsector: any[] = [];
  selectedCountries: any[] = [];
  user: any;
  from: any;
  to: any;
  locale: any;
  ranges: any;
  selected: any;
  @Output() local: any;
  itemList = [];
  selectedItems = [];
  countries = [];
  settings = {};
  settingsS = {};
  defaultPreference = true;
  preferences: any;
  preferencesCheck: boolean = false;
  aptTags: any[];
  responseGroup: any;
  @ViewChild('dt', { static: false }) table: Table;
  @ViewChild(DaterangepickerDirective) picker: DaterangepickerDirective;

  detectChange = false;
  constructor(private aptService: AptService, private caledarSettings: CalendarSettingService,
              private spinner: NgxSpinnerService, private toastr: ToastrService,
              private formBuilder: FormBuilder, private route: ActivatedRoute, private cdr: ChangeDetectorRef,
              private datepipe: DatePipe,
              private confirmationService: ConfirmationService, private router: Router) {
    this.sectorResult = [];
    this.aptService.saveAitem(true);

    this.checkRole();
    this.initReactiveForm();
    this.locale = this.caledarSettings.locale;
    this.ranges = this.caledarSettings.aptRanges;
    this.selected = this.caledarSettings.selected;
    this.from  = this.caledarSettings.transformDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformDate(this.selected.endDate);
  }
  openDatepicker() {
    this.picker.open();
  }
  ngOnInit() {
    this.itemList = [];
    this.sectorResult = [];
    this.selectsector = [];
    this.selectedCountries1 = [];
    this.selectedItems = [];
    this.selectedCountries = [];
    this.countries = [];
    this.getLookupServices();
  }

  filterCountry(event) {
    let filtered: any[] = [];
    let query = event.query;
    for (let countryIndex = 0; countryIndex < this.countries.length; countryIndex++) {
      let country = this.countries[countryIndex];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }

    this.filteredCountries = filtered;
  }

  initReactiveForm() {
    this.aptForm = this.formBuilder.group({
      sectors: [''],
      countries: [''],
      values: [''],
    });
  }

  ngAfterViewInit() { }

  getAttackCampaignList(body) {

    this.spin = true;
    this.spinner.show('listAttackCampaign-spinner');
    if (body === 'clear') {
      this.aptTags = [];
      this.selectedCountries1 = [];
      this.selectedCountries = [];
      this.selectedItems = [];
      this.data['from'] = null;
      this.data['to'] = null;

      for (let sec of this.itemList) {
        sec.selected = false;
      }
      for (let sec of this.sectorResult) {
        sec.selected = false;
      }
      body = {};
    }

    this.selected.startDate = this.from;
    this.selected.endDate = this.to;

    if (this.route.snapshot.queryParams['defaultPreferences'] &&
        this.route.snapshot.queryParams['defaultPreferences'] === 'false') {
      this.defaultPreference = false;
      this.preferencesCheck = false;

    }
    this.requestIncident = [];
    this.aptService.getAttackCampaignList(body,this.defaultPreference).subscribe(
      res => {
        this.spin = false;
        this.spinner.hide('listAttackCampaign-spinner');
        this.responseGroup = res;
        if (this.responseGroup) {
          this.requestIncident = [...this.responseGroup];

        } else {
          this.requestIncident = [];
        }
        this.table.first = 0;
        this.aptForm.enable();
        }, err => {
          this.spin = false;
          this.spinner.hide('listAttackCampaign-spinner');
          this.aptForm.enable();
          this.toastr.error('Some Error occured. Please try later');

      }
    );
  }

  viewAttackCampaign(id) {
    this.router.navigate(['apt/viewCampaign'], {queryParams: {id: id, send: false, lastPage: 'Attacks/Campaigns Listing'}});

  }

  search() {
    this.data = {};
    let body = this.aptForm.value;
    this.aptTags = this.aptForm.value.values;
    if (this.aptTags.length > 0 || body.from !== undefined || body.to !== undefined ||
      this.selectedCountries1.length > 0 || this.selectedItems.length > 0) {
      this.defaultPreference = false;
      this.aptForm.disable();
      if (body.from !== undefined) {
        this.data['from'] = body.from;
      }
      if (body.to !== undefined) {
        this.data['to'] = body.to;
      }
      if (this.aptTags.length > 0) {
        this.data['values'] = [];
        for (let tag of this.aptTags) {
          this.data['values'].push(tag.name);
        }
      }
      if (this.route.snapshot.queryParams['attackCampaign']) {
        this.data['attackCampaign'] = true;
      }
      if (this.selectedCountries1.length > 0) {
        this.data['sectorIds'] = [];
        this.data['sectorIds'] = this.selectedCountries1;
      }
      if (this.selectedItems.length > 0) {
        this.data['countryIds'] = [];
        this.data['regions'] = [];
        this.data['regions'] = this.selectedItems;
      }
    }
    this.getAttackCampaignList(this.data);

  }

  handleRegions(event, category) {
    if (event.checked === true) {
      this.selectedItems.push(category);
    } else if (event.checked === false) {
      const index = this.selectedItems.findIndex(x => x === category);
      if (index !== -1) {
        this.selectedItems.splice(index, 1);
      }
    }
    this.search();
  }

  handleSectors(event, id) {
    event.selected = !event.selected;
    if (event.selected === true) {
      const index = this.selectedCountries1.findIndex(x => x === id);
      if (index === -1) {
        this.selectedCountries1.push(id);
      }
    } else if (event.selected === false) {
      const index = this.selectedCountries1.findIndex(x => x === id);
      if (index !== -1) {
        this.selectedCountries1.splice(index, 1);
      }
    }
    this.search();

  }

  createAttackCampaign() {
    this.router.navigate(['apt/attacks/campaigns']);
  }

  checkRole() {
    const role = sessionStorage.getItem('role');
    if (role === 'ROLE_SOC_ADMIN' || role === 'ROLE_L1_ANALYST' || role === 'ROLE_L2_ANALYST') {
      this.isSoc = true;
    } else {
      this.isSoc = false;
    }
  }

  onKeyUp(event: KeyboardEvent) : void{
    if (event.key == "Enter" || event.key == ",") {
      let tokenInput = event.srcElement as any;
      if (tokenInput.value) {
        tokenInput.value = tokenInput.value.trim();
        if (event.key === ','){
          tokenInput.value = tokenInput.value.slice(0, -1);
        }
        if (this.selectedCountries.find((test) => test.name.toLowerCase() === tokenInput.value.toLowerCase()) === undefined) {
          this.selectedCountries.push({name: tokenInput.value});
        }
        tokenInput.value = '';
      }
    }
  }
  onLoseFocus(event: KeyboardEvent) : void{
    let tokenInput = event.srcElement as any;

    if (tokenInput.value) {

      tokenInput.value = tokenInput.value.trim();

      if (this.selectedCountries.find((test) => test.name.toLowerCase() === tokenInput.value.toLowerCase()) === undefined) {
        this.selectedCountries.push({name: tokenInput.value});
      }
      tokenInput.value = '';
    }
  }

  getLookupServices(): void {

    this.settingsS = {
      text: "Select Sectors"
    };
    this.data = {};

    if (this.route.snapshot.queryParams['attackCampaign']) {
      this.data['attackCampaign'] = true;
    }
    if (this.route.snapshot.queryParams['countries']) {
      this.data['countryIds'] = [];
      const id = this.route.snapshot.queryParams['countries'];
      this.data['countryIds'].push(id);
    } else if (this.route.snapshot.queryParams['sectors']) {
      this.data['sectorIds'] = [];
      const id = this.route.snapshot.queryParams['sectors'];
      this.data['sectorIds'].push(Number(id));
      this.selectedCountries1.push(Number(id));

    }
    if (this.route.snapshot.queryParams['from']) {
      const id = this.route.snapshot.queryParams['from'];
      this.data['from'] = id;
      this.from = id;
      this.selected.startDate = this.from;
    }
    if (this.route.snapshot.queryParams['to']) {
      const id = this.route.snapshot.queryParams['to'];
      this.data['to'] = id;
      this.to = id;
      this.selected.endData = this.to;
    }
    this.aptService.getpreferences().subscribe(
      res => {
        this.preferences = res;
        this.aptTags = [];

        if (this.preferences && (this.preferences.responseDto.responseCode === 200 ||
          this.preferences.responseDto.responseCode === '200')) {
          if ((this.preferences.sectorPreferences && this.preferences.sectorPreferences.length > 0)
          || (this.preferences.regionPreferences && this.preferences.regionPreferences.length > 0)) {
            this.defaultPreference = true;
            this.preferencesCheck = true;


          } else {
            this.preferencesCheck = false;
            this.defaultPreference = false;

          }
        } else {
          this.preferencesCheck = false;
          this.defaultPreference = false;

        }
        this.aptService.getSectors().subscribe(
          res => {
            this.see = false;
            const result: any = res;
            const abc = [];
            this.sectorResult = [];
            this.selectsector = [];

            const id = this.route.snapshot.queryParams['sectors'];
            if (result && result.length > 0) {
              let value;
              for (let sec of result) {

                const data = {
                  id: sec.id,
                  itemName: sec.value,
                  selected: false,
                };
                abc.push(data);
              }
              this.sectorResult = abc;
              if (this.preferences && (this.preferences.responseDto.responseCode === 200 ||
                this.preferences.responseDto.responseCode === '200')) {
                if (this.preferences.sectorPreferences && this.preferences.sectorPreferences.length > 0) {
                  let result = this.sectorResult.filter(o1 =>
                    !this.preferences.sectorPreferences.some(o2 => (o1.id === o2.sector && o2.enabled === false)));
                  this.sectorResult = result;
                }
              }
              if (this.route.snapshot.queryParams['sectors']) {
                this.defaultPreference = false;

                for (let sec of this.sectorResult) {
                  if (sec.id === Number(id)) {
                    sec.selected = true;
                  } else {
                    sec.selected = false;
                  }
                }
              }
            } else {
              this.see = true;
              this.sectorResult = [];
            }
          },
          err => {
          }
        );
        this.aptService.suggestionACListing().subscribe(
          res => {
            let sect = [];
            const result: any = res;
            if (result && result.length > 0) {
              this.countries = result;
              for (let sec of this.countries) {
                const data = {
                  name: sec
                };
                if (data.name === null || data.name == '') {
                } else {
                  sect.push(data);

                }
              }
              this.countries = sect;
            } else {
              this.countries = [];
            }
          },
          err => {
          }
        );
        this.aptService.getRegions().subscribe(
          res => {
            const result: any = res;
            let value;
            const id = this.route.snapshot.queryParams['countries'];
            if (result && result.length > 0) {
              this.regionResult = result;
              for (let region of this.regionResult) {
                if (this.route.snapshot.queryParams['countries'] && region.id === Number(id)) {
                  value = region;
                }
                const data = {
                  id: region.id,
                  itemName: region.countryName,
                  category: region.region,
                  selected: false,

                };
                if (this.itemList.find((test) => test.category === data.category) === undefined) {
                  this.itemList.push(data);
                }
              }

              if (this.preferences && (this.preferences.responseDto.responseCode === 200 ||
                this.preferences.responseDto.responseCode === '200')) {
                if (this.preferences.regionPreferences && this.preferences.regionPreferences.length > 0) {
                  let result = this.itemList.filter(o1 =>
                    !this.preferences.regionPreferences.some(o2 => (o1.category === o2.value && o2.enabled === false)));
                  this.itemList = result;
                }
              }
              if (this.route.snapshot.queryParams['countries']) {
                this.defaultPreference = false;

                for (let region of this.itemList) {
                  if (region.category === value.region) {
                    region.selected = true;
                  } else {
                    region.selected = false;
                  }
                }
              }
            } else {
              this.itemList = [];
            }
          },
          err => {
          }
        );
      },
      err => {

      });
  }

  public selectedDate() {
    if (this.detectChange) {
      this.from  = this.caledarSettings.transformDate(this.selected.startDate);
      this.to  = this.caledarSettings.transformDate(this.selected.endDate);
      this.aptForm.value['from'] = this.from;
      this.aptForm.value['to'] = this.to;
      this.search();
    } else{
      this.detectChange = true;
      this.getAttackCampaignList(this.data);
    }

  }
  pagination() {
    window.scroll(0, 0);
  }
}
