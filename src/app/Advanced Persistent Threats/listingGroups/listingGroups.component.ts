import {AfterViewInit, Component, Input, NgZone, OnInit, Output, ViewChild} from '@angular/core';
import {ConfirmationService} from "primeng/api";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DatePipe} from "@angular/common";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {NgxSpinnerService} from "ngx-spinner";
import {ToastrService} from "ngx-toastr";
import {Table} from "primeng/table";
import {DaterangepickerDirective} from "ngx-daterangepicker-material";
import {AptService} from '../../core-services/apt.service';
import {CalendarSettingService} from '../../core-services/calendarSetting.service';

@Component({
  selector: 'app-ListingGroups',
  templateUrl: './listingGroups.component.html',
  styleUrls: ['./listingGroups.component.scss'],
  providers: [ConfirmationService]
})
export class ListingGroupsComponent implements OnInit, AfterViewInit {
  requestIncident: any;
  showTable: any;
  spin: any = true;
  isSoc = false;
  registerForm: FormGroup;
  sectorResult: any;
  regionResult: any;
  selectedCountries1: any = [];
  data: any = {};
  settingsS: {};
  preferences: any;

  filteredCountries: any[] = [];
  countries: any[] = [];

  selectedCountries: any[] = [];
  @Input() position = 'normal';
  user: any;
  from: any;
  to: any;
  locale: any;
  ranges: any;
  selected: any;
  responseGroup: any;
  @ViewChild('dt', { static: false }) table: Table;


  @Output() local: any;
  itemList = [];
  selectedItems = [];
  settings = {};
  tagss: any[];
  defaultPreference = true;
  checkPerefernces: boolean = false;
  detectChange = false;
  @ViewChild(DaterangepickerDirective) picker: DaterangepickerDirective;

  constructor(private aptService: AptService, private caledarSettings: CalendarSettingService,
              private spinner: NgxSpinnerService, private toastr: ToastrService,
              private formBuilder: FormBuilder, private datepipe: DatePipe,
              private route: ActivatedRoute,
              private ngZone: NgZone, private confirmationService: ConfirmationService, private router: Router) {
    this.sectorResult = [];
    this.aptService.saveGitem(true);

    this.checkRole();
    this.initReactiveForm();
    this.locale = this.caledarSettings.locale;
    this.ranges = this.caledarSettings.aptRanges;
    this.selected = this.caledarSettings.selected;
    this.from = this.caledarSettings.transformDate(this.selected.startDate);
    this.to = this.caledarSettings.transformDate(this.selected.endDate);

  }

  initReactiveForm() {
    this.registerForm = this.formBuilder.group({
      sectors: [''],
      countries: [''],
      values: [''],
    });
  }
  openDatepicker() {
    this.picker.open();
  }

  ngOnInit() {
    this.itemList = [];
    this.sectorResult = [];
    this.selectedCountries1 = [];
    this.selectedItems = [];
    this.getLookupServices();

  }

  filterCountry(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    let query = event.query;
    for (let countryIndex = 0; countryIndex < this.countries.length; countryIndex++) {
      let country = this.countries[countryIndex];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }

    this.filteredCountries = filtered;
  }

  ngAfterViewInit() {
    this.tagss = [];
  }

  search() {
    this.data = {};
    let body = this.registerForm.value;
    this.tagss = this.registerForm.value.values;

    if (this.tagss.length > 0 || body.from !== undefined || body.to !== undefined ||
        this.selectedCountries1.length > 0 || this.selectedItems.length > 0) {
      this.registerForm.disable();
      this.defaultPreference = false;
      if (body.from !== undefined) {
        this.data['from'] = body.from;
      }
      if (body.to !== undefined) {
        this.data['to'] = body.to;
      }
      if (this.tagss.length > 0) {
        this.data['values'] = [];
        for (let tag of this.tagss) {
          this.data['values'].push(tag.name);
        }
      }
      if (this.selectedCountries1.length > 0) {
        this.data['sectorIds'] = [];
        this.data['sectorIds'] = this.selectedCountries1;
      }
      if (this.selectedItems.length > 0) {
        this.data['countryIds'] = [];
        this.data['regions'] = [];
        this.data['regions'] = this.selectedItems;
      }
    }
    this.getGroupsList(this.data);

  }

  onKeyUp(event: KeyboardEvent) {
    if (event.key == "Enter") {
      let tokenInput = event.srcElement as any;
      if (tokenInput.value) {
        tokenInput.value = tokenInput.value.trim();
        if (this.selectedCountries.find((test) => test.name.toLowerCase() === tokenInput.value.toLowerCase()) === undefined) {
          this.selectedCountries.push({name: tokenInput.value});
        }
        tokenInput.value = "";
      }
    }
  }

  getGroupsList(body) {
    this.spin = true;
    this.spinner.show('listGroup-spinner');
    if (body === 'clear') {
      this.tagss = [];
      this.selectedCountries1 = [];
      this.selectedCountries = [];
      this.selectedItems = [];
      for (let itemObj of this.itemList) {
        itemObj.selected = false;
      }
      for (let itemObj of this.sectorResult) {
        itemObj.selected = false;
      }
      body = {};
    }
    this.aptService.getGroupsList(body, this.defaultPreference).subscribe(
        res => {
          this.spin = false;
          this.spinner.hide('listGroup-spinner');
          this.registerForm.enable();
          this.responseGroup = res;
          if (this.responseGroup) {
            this.requestIncident = [...this.responseGroup];
          } else {
            this.requestIncident = [];
          }

          this.table.first = 0;
          this.registerForm.enable();
        }, err => {
          this.spin = false;
          this.spinner.hide('listGroup-spinner');

          this.showTable = false;
          this.registerForm.enable();

          this.toastr.error('Some Error occured. Please try later');

        }
    );
  }

  handleRegions(event, category) {
    if (event.checked === true) {
      this.selectedItems.push(category);
    } else if (event.checked === false) {
      const index = this.selectedItems.findIndex(x => x === category);
      if (index !== -1) {
        this.selectedItems.splice(index, 1);
      }
    }
    this.search();

  }

  handleSectors(event, id) {
    event.selected = !event.selected;
    if (event.selected === true) {
      const index = this.selectedCountries1.findIndex(x => x === id);
      if (index === -1) {
        this.selectedCountries1.push(id);
      }
    } else if (event.selected === false) {
      const index = this.selectedCountries1.findIndex(x => x === id);
      if (index !== -1) {
        this.selectedCountries1.splice(index, 1);
      }
    }
    this.search();
  }

  viewGroups(id) {
    this.router.navigate(['apt/viewGroup'], {queryParams: {id: id, send: false, lastPage: 'Groups Listing'}});

  }

  createGroups() {
    this.router.navigate(['apt/groups']);
  }

  checkRole() {
    const role = sessionStorage.getItem('role');
    if (role === 'ROLE_SOC_ADMIN' || role === 'ROLE_L1_ANALYST' || role === 'ROLE_L2_ANALYST') {
      this.isSoc = true;
    } else {
      this.isSoc = false;
    }
  }

  getLookupServices() {
    this.itemList = [];
    this.sectorResult = [];
    this.selectedCountries = [];

    this.settings = {
      text: "Select Regions & Countries"
    };
    this.settingsS = {
      text: "Select Sectors"
    };
    this.aptService.getpreferences().subscribe(
        res => {
          this.preferences = res;
          if (this.preferences && (this.preferences.responseDto.responseCode === 200 ||
              this.preferences.responseDto.responseCode === '200')) {
            if ((this.preferences.sectorPreferences && this.preferences.sectorPreferences.length > 0)
                || (this.preferences.regionPreferences && this.preferences.regionPreferences.length > 0)) {
              this.defaultPreference = true;
              this.checkPerefernces = true;
            } else {
              this.checkPerefernces = false;
              this.defaultPreference = false;
            }
          } else {
            this.checkPerefernces = false;

            this.defaultPreference = false;

          }
          this.aptService.getSectors().subscribe(
              res => {
                const result: any = res;
                const sectorArray = [];
                this.sectorResult = [];
                if (result && result.length > 0) {
                  for (let sectorObj of result) {
                    const data = {
                      id: sectorObj.id,
                      itemName: sectorObj.value,
                      selected: false,
                    };
                    sectorArray.push(data);
                  }
                  this.sectorResult = sectorArray;
                  if (this.preferences && (this.preferences.responseDto.responseCode === 200 ||
                      this.preferences.responseDto.responseCode === '200')) {
                    if (this.preferences.sectorPreferences && this.preferences.sectorPreferences.length > 0) {
                      let result = this.sectorResult.filter(sectorFilter =>
                          !this.preferences.sectorPreferences.some(sectorFilterArray => (sectorFilter.id === sectorFilterArray.sector && sectorFilterArray.enabled === false)));
                      this.sectorResult = result;
                    }
                  }
                } else {
                  this.sectorResult = [];
                }
              },
              err => {
              }
          );
          this.aptService.suggestionGListing().subscribe(
              res => {
                let suggestionArray = [];
                const result: any = res;
                if (result && result.length > 0) {
                  this.countries = result;
                  for (let country of this.countries) {
                    const data = {
                      name: country
                    };
                    if (data.name === null || data.name == '') {
                    } else {
                      suggestionArray.push(data);

                    }
                  }
                  this.countries = suggestionArray;
                } else {
                  this.countries = [];
                }
              },
              err => {
                this.countries = [];
              }
          );
          this.aptService.getRegions().subscribe(
              res => {
                const result: any = res;
                this.selectedItems = [];
                this.settings = {
                  singleSelection: false,
                  text: "Select Fields",
                  selectAllText: 'Select All',
                  unSelectAllText: 'UnSelect All',
                  searchPlaceholderText: 'Search Fields',
                  enableSearchFilter: true,
                  badgeShowLimit: 5,
                  groupBy: "category"
                };
                if (result && result.length > 0) {
                  this.regionResult = result;
                  for (let region of this.regionResult) {
                    const data = {
                      id: region.id,
                      itemName: region.countryName,
                      category: region.region,
                      selected: false,

                    };
                    if (this.itemList.find((test) => test.category === data.category) === undefined) {
                      this.itemList.push(data);
                    }
                  }

                  if (this.preferences && (this.preferences.responseDto.responseCode === 200 ||
                      this.preferences.responseDto.responseCode === '200')) {
                    if (this.preferences.regionPreferences && this.preferences.regionPreferences.length > 0) {
                      let result = this.itemList.filter(sectorFilter =>
                          !this.preferences.regionPreferences.some(sectorFilterArray => (sectorFilter.category === sectorFilterArray.value && sectorFilterArray.enabled === false)));
                      this.itemList = result;
                    }
                  }
                } else {
                  this.itemList = [];
                }
              },
              err => {
              }
          );
        },
        err => {

        });
  }

  public selectedDate() {
    if (this.detectChange) {
      this.from = this.caledarSettings.transformDate(this.selected.startDate);
    this.to = this.caledarSettings.transformDate(this.selected.endDate);
    this.registerForm.value['from'] = this.from;
    this.registerForm.value['to'] = this.to;
    this.search();
  } else {
      this.detectChange = true;
      this.getGroupsList('clear');
    }
  }
  pagination() {
    window.scroll(0, 0);
  }
}
