import {Component, OnDestroy, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import 'd3';
declare var colors: any;
declare var Datamap: any;
declare var d3: any;
import {DatePipe} from "@angular/common";
import {AptService} from '../../../core-services/apt.service';
@Component({
  selector: 'app-datamapI',
  templateUrl: './datamapI.component.html',
  styleUrls: ['./datamap.component.css'],
})
export class DatamapIComponent implements OnInit {
  latlong: any;
  mapData: any;
  option: any ;
  spin: any = true;
  users: any;
  username: any;
  getTargetSectorCountriesByIOCRes: any;
  name: any;
  email: any;
  emailId: any;
  role: any;
  displayName: any;
  createdBy: any;
  createdDate: any;
  error: any;
options: any = {};
  elections: any ;
  election: any;
  showTable = false;
  hideTable = false;
  from: any;
  to: any;
  constructor(private aptService: AptService, private datepipe: DatePipe, private router: Router) {
    var map = new Datamap({
      element: document.getElementById('container2'), fills: {defaultFill: "fa0af0"},
      height: 500, width: 500});

  }
  ngOnInit() {
    this.mapData = [];
    this.elections = [];
    this.spin = true;
    this.showTable = false;
    this.hideTable = false;
    this.to = new Date();
    this.from = new Date();
    this.from  = this.from.setMonth(new Date().getMonth() - 1);
    this.from = this.datepipe.transform(this.from, 'yyyy-MM-dd');
    this.to = this.datepipe.transform(this.to, 'yyyy-MM-dd');
    this.aptService.getTargetSectorCountriesByIOC(this.from, this.to).subscribe(
      getTargetSectorCountriesByIOCRes =>{
        this.getTargetSectorCountriesByIOCRes = getTargetSectorCountriesByIOCRes;
        this.spin = false;
          this.showTable = true;
          this.hideTable = false;
          const colors = [
            "#0d552c",
            "#17753f",
            "#289b59",
            "#499b59",
            "#37ba6f",
            "#45cc7f",
            "#58e092",
            "#6ceda3",
            "#93ffc1",
            "#bdffd9",
            "#e7fff1",
          ];
        let previousHits = 0, colorIndex= 0, indexOfSameHits = 0, color, hits;
          if (this.getTargetSectorCountriesByIOCRes) {

            this.getTargetSectorCountriesByIOCRes = this.getTargetSectorCountriesByIOCRes.sort(
                (getTargetSectorCountriesByIOCResObjA,getTargetSectorCountriesByIOCResObjb) => getTargetSectorCountriesByIOCResObjb.count - getTargetSectorCountriesByIOCResObjA.count);
            for (let countryIndex = 0; countryIndex < this.getTargetSectorCountriesByIOCRes.length; countryIndex++) {
              hits = this.getTargetSectorCountriesByIOCRes[countryIndex].count;
              if (colorIndex <= 9) {
                if (hits !== previousHits) {
                  color = colors[colorIndex];
                  previousHits = hits;
                  indexOfSameHits = colorIndex;
                  colorIndex ++;
                } else {
                  color = colors[indexOfSameHits];
                  previousHits = hits;
                  indexOfSameHits = colorIndex;
                }
              } else {
                color = colors[10];
              }

              var obj = {
                fillColor: color,
                numberOfThings: hits,
                country: this.getTargetSectorCountriesByIOCRes[countryIndex].value,
                id: this.getTargetSectorCountriesByIOCRes[countryIndex].id,
                countryCode: this.getTargetSectorCountriesByIOCRes[countryIndex].countryCode
              };
              this.mapData[this.getTargetSectorCountriesByIOCRes[countryIndex].countryCode] = obj;
              if (obj.countryCode === 'KOR' || obj.countryCode === 'kor') {
                obj.country = 'South Korea'
              }
              if (obj.countryCode === 'PRK'|| obj.countryCode === 'prk') {
                obj.country = 'North Korea'

              }
              this.elections.push(obj);
            }
          }
          this.electionsfunt(this.mapData);
          this.election =  new Datamap({
            element: document.getElementById('container2'),
            projection: 'mercator',
            scope: 'world',
             width: 850,
             height: 630,
             options: {
              staticGeoData: false,
              legendHeight: 200,
              legendwidth: 300 // optionally set the padding for the legend
            },
            data: this.mapData,
            geographyConfig: {
              borderColor: '#DEDEDE',
              highlightBorderWidth: 2,
              highlightOnHover: false,
              // only change border
              highlightBorderColor: '#B7B7B7',
              popupTemplate: function (geo, data) {
                if (!data) {
                  return ['<div class="hoverinfo">',
                    '<strong>', geo.properties.name, '</strong>',
                    '<br>Count: <strong>', 0, '</strong>',
                    '</div>'].join('');
                  ;
                }
                let count = data.numberOfThings;
                if (count >= 1000 && count <= 99999) {
                  count = count.toString().replace(/\B(?=(\d{3})+(?!\d))/g,
                    ',');
                } else if (count >= 100000 && count <= 999999999) {
                  count = (count / 1000000);
                  count = Math.round(count * 100) / 100 + ' M';
                } else if (count >= 1000000000 && count <= 999999999999) {
                  count = (count / 1000000000);
                  count = Math.round(count * 10) / 10 + ' B';
                }
                // tooltip content
                return ['<div class="hoverinfo">',
                  '<strong>', geo.properties.name, '</strong>',
                  '<br>Count: <strong>', count, '</strong>',
                  '</div>'].join('');
              }
            }
          });
         this.election.addPlugin("mylegen", this.addLegend2);
          this.election.mylegen(this.elections)

      }, err => {
        this.spin = false;
        this.showTable = false;
        this.hideTable = true;
        var election =  new Datamap({
          element: document.getElementById('container2'),
          projection: 'mercator',

          fills: {defaultFill: '#e7fff1'},
          geographyConfig: {
            borderColor: '#DEDEDE',
            highlightBorderWidth: 2,
            highlightFillColor: function (geo) {
              return geo['fillColor'] || '#0d552c';
            },
            // only change border
            highlightBorderColor: '#B7B7B7',
            popupTemplate: function (geo, data) {
              if (!data) {
                return ['<div class="hoverinfo">',
                  '<strong>', geo.properties.name, '</strong>',
                  '<br>Count: <strong>', 0, '</strong>',
                  '</div>'].join('');
                ;
              }
              // tooltip content
              return ['<div class="hoverinfo">',
                '<strong>', geo.properties.name, '</strong>',
                '<br>Count: <strong>', data.numberOfThings, '</strong>',
                '</div>'].join('');
            }
          }
        });
      });


  }
  electionsfunt(election) {
    this.mapData = election;
  }

   addLegend2(layer, data, options) {
     var html = [],
       label = '';
     var allObj = data;
     this.mapData = data;
     var items = {};
     var count = 0;
     this.mapData.forEach(item => {
       if (count > 9)
         return
       var hits = item.numberOfThings;
       var countryname = item.countryCode;

       if (items.hasOwnProperty(hits)) {
         items[hits].push(item)
       } else {
         items[hits] = [item]
         count++;
       }
     });
     var size = Object.keys(items).length;
     for (var key in items) {
       var elem = items[key];
       var color = elem[0].fillColor;
       var width = 100 / size;
       let keys = +key;
       let keyss;
       if (keys >= 1000 && keys <= 99999) {
         keyss = keys.toString().replace(/\B(?=(\d{3})+(?!\d))/g,
           ',');
       } else if (keys >= 100000 && keys <= 999999999) {
         keys = (keys / 1000000);
         keyss = Math.round(keys * 100) / 100 + ' M';
       } else if (keys >= 1000000000 && keys <= 999999999999) {
         keys = (keys / 1000000000);
         keyss = Math.round(keys * 10) / 10 + ' B';
       }else if (keys < 1000) {
         keyss = keys;
       }
       var htmlStr = '<li class="legend-key" style=" cursor:pointer; width:' + width + '%; text-align: center;padding: 2px; border-top:10px solid ' + color + '; color:#000">';
       htmlStr += keyss;
       let htmler = "";
       htmlStr += htmler;

       htmlStr += '</li>';
       html[key] = htmlStr;
     }
     d3.select(this.options.element).selectAll(".datamaps-legend").remove();
     d3.select(this.options.element).append('div')
       .attr('class', 'datamaps-legend')
       .html('<ul class="list-inline list-inline-b">' + html.join('') + "</ul>");
  }
  clicked(event) {
    event  = JSON.parse(event.target.dataset.info);
    event  = event.id;
    this.router.navigate(['apt/attacksCampaignList'], {queryParams: {
        defaultPreferences: false, countries: event, from: this.from, to: this.to}});

  }
}
