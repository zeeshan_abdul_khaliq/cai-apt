import {Component, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from "rxjs";
import {Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import {EChartsOption} from "echarts";
import {NgxSpinnerService} from "ngx-spinner";
import {ToastrService} from "ngx-toastr";
import {AptService} from '../../core-services/apt.service';
import {CalendarSettingService} from '../../core-services/calendarSetting.service';

@ Component({
  selector: 'app-targetSector',
  templateUrl: './targetSector.component.html',
  styleUrls: ['./dashboard.component.scss'],
})


export class TargetSectorComponent implements OnInit, OnDestroy {
  showRA = true;
  showIA = false;
  hideRA = false;
  hideIA = false;
  private sectorResponse: any;
  sectors: any;
  error: any;
  public chartOption: EChartsOption = {};
  theme = 'macarons';
  spin: any;
  unsub: any = new Subject();
  from: any;
  to: any;
  locale: any;
  ranges: any;
  selected: any;
  @Output() local: any;
  constructor(private threatFeedService: AptService, private spinner: NgxSpinnerService,
              private caledarSettings: CalendarSettingService,
              private datepipe: DatePipe,  private toastr: ToastrService,
              private router: Router) {
    this.locale = this.caledarSettings.locale;
    this.ranges = this.caledarSettings.aptRanges;
    this.selected = {
      startDate: '2019-01-01T00:00:00',
      endDate: new Date()
    };
    this.from  = this.caledarSettings.transformAptDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformAptDate(this.selected.endDate);

  }
  public selectedDateRA() {
    this.from  = this.caledarSettings.transformAptDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformAptDate(this.selected.endDate);
    if (this.showRA === true || this.hideRA === true) {
      this.onSubmit();
    }
  }
  public selectedDateIA() {
    this.from  = this.caledarSettings.transformAptDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformAptDate(this.selected.endDate);
    if (this.showIA === true || this.hideIA === true) {
      this.onSubmitI();
    }
  }
  changeTab(event) {
    if (event.index === 0) {
      this.showRA = true;
      this.showIA = false;
      this.hideRA = false;
      this.hideIA = false;
    } else if (event.index === 1) {
      this.showRA = false;
      this.showIA = true;
      this.hideRA = false;
      this.hideIA = false;
    }

  }
  onSubmit() {
    this.sectors = [];
    this.spinner.show('targetSector-spinner');
    this.showRA = true;
    this.showIA = false;
    this.hideRA = false;
    this.hideIA = false;
    this.threatFeedService.getTargetSectorAttack(this.from, this.to).subscribe(
      res => {
        this.sectorResponse = res;
        this.spinner.hide('targetSector-spinner');
        if (this.sectorResponse && this.sectorResponse.length > 0) {
          this.showRA = true;
          this.showIA = false;
          this.hideRA = false;
          this.hideIA = false;

          this.chartOption = {
            tooltip: {
              trigger: 'item',
              formatter: function (info: any) {
                const value = info.value;
                const name = info.name;
                return  "Sector Name: " + name + '<br/>' + "Count: " + value;

              }
            },
            series: [
              {
                breadcrumb: {
                  show: false,
                  emptyItemWidth: 0
                },
                type: 'treemap',
                selectedMode: false,
                roam: false,                         // true, false, 'scale' or 'zoom', 'move'
                data: [],
                levels: [],

              }
            ]
          };
          this.chartOption.series[0].data = [];
          for (let sec of this.sectorResponse) {
            const data = {
              name: sec.value,
              id: sec.id,
              value: sec.count
            };
            this.sectors.push(data);
          }
          setTimeout(()=> {
            this.chartOption.series[0].data = [...this.sectors];
            this.chartOption.series[0].levels = [...this.caledarSettings.treemapSetting()];
            this.chartOption = {...this.chartOption};
          });
        } else {
          this.showRA = false;
          this.showIA = false;
          this.hideRA = true;
          this.hideIA = false;
        }
      },
      err => {
        this.error = err;
        this.spinner.hide('targetSector-spinner');
        this.showRA = false;
        this.showIA = false;
        this.hideRA = true;
        this.hideIA = false;
        this.toastr.error('Some error occurred. Please refresh the page.');
      }
    );
  }
  onSubmitI() {
    this.sectors = [];
    this.spinner.show('targetSector-spinner');
    this.showRA = false;
    this.showIA = true;
    this.hideRA = false;
    this.hideIA = false;
    this.threatFeedService.getTargetSectorAttackByIOC(this.from, this.to).subscribe(
      res => {
        this.sectorResponse = res;
        this.spinner.hide('targetSector-spinner');
        if (this.sectorResponse && this.sectorResponse.length > 0) {
          this.showRA = false;
          this.showIA = true;
          this.hideRA = false;
          this.hideIA = false;
          this.chartOption = {
            tooltip: {
              trigger: 'item',
              formatter: function (info: any) {
                const value = info.value;
                const name = info.name;
                return  "Sector Name: " + name + '<br/>' + "Count: " + value;

              }
            },
            series: [
              {
                breadcrumb: {
                  show: false,
                  emptyItemWidth: 0
                },
                type: 'treemap',
                selectedMode: false,
                roam: false,
                data: [],
                levels: []

              }
            ]
          };
          this.chartOption.series[0].data = [];
          for (let sec of this.sectorResponse) {
            const data = {
              name: sec.value,
              id: sec.id,
              value: sec.count
            };
            this.sectors.push(data);
          }
          setTimeout(() => {
            this.chartOption.series[0].data = [...this.sectors];
            this.chartOption.series[0].levels = [...this.caledarSettings.treemapSetting()];
            this.chartOption = {...this.chartOption};
          });
        } else {
          this.showRA = false;
          this.showIA = false;
          this.hideRA = false;
          this.hideIA = true;
        }
      },
      err => {
        this.error = err;
        this.spinner.hide('targetSector-spinner');
        this.showRA = false;
        this.showIA = false;
        this.hideRA = false;
        this.hideIA = true;
        this.toastr.error('Some error occurred. Please refresh the page.');
      }
    );
  }

  ngOnInit() {
  }

  routeToListing(event) {
    const country = event.data.id;
    this.router.navigate(['apt/attacksCampaignList'], {queryParams: {sectors: country,
        defaultPreferences: false, from: this.from, to: this.to}});
  }
  routeToListingAttackCampaign(event) {
    const country = event.data.id;
    this.router.navigate(['apt/attacksCampaignList'], {queryParams: {sectors: country,
        defaultPreferences: false,   from: this.from, to: this.to, attackCampaign: true}});
  }
  ngOnDestroy(): void {

    this.unsub.unsubscribe();
  }
}
