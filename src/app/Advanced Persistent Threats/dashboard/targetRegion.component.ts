import {Component, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from "rxjs";
import {Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import {EChartsOption} from "echarts";
import {NgxSpinnerService} from "ngx-spinner";
import {ToastrService} from "ngx-toastr";
import {AptService} from '../../core-services/apt.service';
import {CalendarSettingService} from '../../core-services/calendarSetting.service';
@ Component({
  selector: 'app-targetRegion',
  templateUrl: './targetRegion.component.html',
  styleUrls: ['./dashboard.component.scss'],
})


export class TargetRegionComponent implements OnInit , OnDestroy{
  showRA = true;
  showRC = false;
  showIA = false;
  showIC = false;
  hideRA = false;
  hideRC = false;
  hideIA = false;
  hideIC = false;
  private regionsReponse: any;
  sectors: any;
  countries: any;
  countriesR: any;
  error: any;
  region: any;
  public chartOption: EChartsOption = {};
  public chartOptionCountries: EChartsOption = {};
  spin: any;
  from: any;
  to: any;
  locale: any;
  ranges: any;
  selected: any;
  @Output() local: any;
  constructor(private threatFeedService: AptService,
              private spinner: NgxSpinnerService, private taostr: ToastrService,
              private datepipe: DatePipe,  private caledarSettings: CalendarSettingService,
              private router: Router) {
    this.locale = this.caledarSettings.locale;
    this.ranges = this.caledarSettings.aptRanges;
    this.selected = {
      startDate: '2019-01-01T00:00:00',
      endDate: new Date()
    };
    this.from  = this.caledarSettings.transformAptDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformAptDate(this.selected.endDate);
    this.getRegions();
  }
  public selectedDateRA(event) {
    this.from  = this.caledarSettings.transformAptDate(event.dates[0]);
    this.to  = this.caledarSettings.transformAptDate(event.dates[1]);
    if (this.showRA === true || this.hideRA === true) {
      this.getRegions();
    }
  }
  public selectedDateIA(event) {
    this.from  = this.caledarSettings.transformAptDate(event.dates[0]);
    this.to  = this.caledarSettings.transformAptDate(event.dates[1]);
    if (this.showIA === true || this.hideIA === true) {
      this.getRegionsIndicator();
    }
  }
  public selectedDateRC(event) {
    this.from  = this.caledarSettings.transformAptDate(event.dates[0]);
    this.to  = this.caledarSettings.transformAptDate(event.dates[1]);
    if (this.showRC === true || this.hideRC === true) {
      this.getCountries(this.region);
    }
  }
  public selectedDateIC(event) {
    this.from  = this.caledarSettings.transformAptDate(event.dates[0]);
    this.to  = this.caledarSettings.transformAptDate(event.dates[1]);
    if (this.showIC === true || this.hideIC === true) {
      this.getCountriesIndicator(this.region);
    }
  }
  tabIndex(event) {
    if (event.index === 0) {
      if (this.region === null || this.region === undefined || this.region === '') {
        this.showRA = true;
        this.showRC = false;
        this.showIA = false;
        this.showIC = false;
        this.hideRA = false;
        this.hideRC = false;
        this.hideIA = false;
        this.hideIC = false;
      } else {
        this.showRA = false;
        this.showRC = true;
        this.showIA = false;
        this.showIC = false;
        this.hideRA = false;
        this.hideRC = false;
        this.hideIA = false;
        this.hideIC = false;
      }
    } else  if (event.index === 1) {
      if (this.region === null || this.region === undefined || this.region === '') {
        this.showRA = false;
        this.showRC = false;
        this.showIA = true;
        this.showIC = false;
        this.hideRA = false;
        this.hideRC = false;
        this.hideIA = false;
        this.hideIC = false;
      } else {
        this.showRA = false;
        this.showRC = false;
        this.showIA = false;
        this.showIC = true;
        this.hideRA = false;
        this.hideRC = false;
        this.hideIA = false;
        this.hideIC = false;
      }
    }
  }
  getRegions() {
    this.spinner.show('targetRegion-spinner');
    this.sectors = [];
    this.showRA = true;
    this.showRC = false;
    this.showIA = false;
    this.showIC = false;
    this.hideRA = false;
    this.hideRC = false;
    this.hideIA = false;
    this.hideIC = false;
    this.region = null;
    this.threatFeedService.getTargetRegionAttack(this.from, this.to) .subscribe(
      res => {
        this.regionsReponse = res;
        this.spinner.hide('targetRegion-spinner');
        if (this.regionsReponse && this.regionsReponse.length > 0) {
          this.showRA = true;
          this.showRC = false;
          this.showIA = false;
          this.showIC = false;
          this.hideRA = false;
          this.hideRC = false;
          this.hideIA = false;
          this.hideIC = false;
          this.chartOption = {
            tooltip: {
              trigger: 'item',
              formatter: function (info: any) {
                const value = info.value;
                const name = info.name;
                return  "Region Name: " + name + '<br/>' + "Count: " + value;
              }
            },

            series: [
              {
                breadcrumb: {
                  show: false,
                  emptyItemWidth: 0
                },
                type: 'treemap',
                selectedMode: false,
                roam: false,
                data: [],
                levels: [],
              }
            ]
          };
          for (let sec of this.regionsReponse) {
            const data = {
              name: sec.value,
              value: sec.count
            };
            this.sectors.push(data);
          }
          setTimeout(()=> {
            this.chartOption.series[0].data = [];

            this.chartOption.series[0].data = [...this.sectors];
            this.chartOption.series[0].levels = [...this.caledarSettings.treemapSetting()];
            this.chartOption = {...this.chartOption};
          });
        } else {
          this.showRA = false;
          this.showRC = false;
          this.showIA = false;
          this.showIC = false;
          this.hideRA = true;
          this.hideRC = false;
          this.hideIA = false;
          this.hideIC = false;
        }
      },
      err => {
        this.error = err;
        this.spinner.hide('targetRegion-spinner');
        this.showRA = false;
        this.showRC = false;
        this.showIA = false;
        this.showIC = false;
        this.hideRA = true;
        this.hideRC = false;
        this.hideIA = false;
        this.hideIC = false;
        this.taostr.error('Some error occurred. Please refresh the page.');
      }
    );
  }

  getCountries(event) {
    this.spinner.show('targetRegion-spinner');
    if (this.region === null || this.region === undefined || this.region === '') {
      this.region = event.data.name;
    }
    this.countries = [];
    this.threatFeedService.getCountriesChart(this.region, this.from, this.to).subscribe(
      res => {
        this.countriesR = res;
        this.spinner.hide('targetRegion-spinner');
        if (this.countriesR && this.countriesR.length > 0) {
          this.showRA = false;
          this.showRC = true;
          this.showIA = false;
          this.showIC = false;
          this.hideRA = false;
          this.hideRC = false;
          this.hideIA = false;
          this.hideIC = false;
          this.chartOptionCountries = {
            tooltip: {
              trigger: 'item',
              formatter: function (info: any) {
                const value = info.value;
                const name = info.name;
                return  "Country Name: " + name + '<br/>' + "Count: " + value;

              }
            },

            series: [
              {
                breadcrumb: {
                  show: false,
                  emptyItemWidth: 0
                },
                type: 'treemap',
                selectedMode: false,
                roam: false,
                data: [],
                levels: [],

              }
            ]
          };
          this.chartOptionCountries.series[0].data = [];
          for (let sec of this.countriesR) {
            const data = {
              name: sec.value,
              id: sec.id,
              value: sec.count
            };
            this.countries.push(data);
          }
          setTimeout(()=> {
            this.chartOptionCountries.series[0].data = [...this.countries];
            this.chartOptionCountries.series[0].levels = [...this.caledarSettings.treemapSetting()];
            this.chartOptionCountries = {...this.chartOptionCountries};
          });
        } else {
          this.showRA = false;
          this.showRC = false;
          this.showIA = false;
          this.showIC = false;
          this.hideRA = false;
          this.hideRC = true;
          this.hideIA = false;
          this.hideIC = false;
        }
      },
      err => {
        this.error = err;
        this.spinner.hide('targetRegion-spinner');
        this.showRA = false;
        this.showRC = false;
        this.showIA = false;
        this.showIC = false;
        this.hideRA = false;
        this.hideRC = true;
        this.hideIA = false;
        this.hideIC = false;
        this.taostr.error('Some error occurred. Please refresh the page.');
      }
    );
  }
  getRegionsIndicator() {
    this.spinner.show('targetRegion-spinner');
    this.sectors = [];
    this.showRA = false;
    this.showRC = false;
    this.showIA = true;
    this.showIC = false;
    this.hideRA = false;
    this.hideRC = false;
    this.hideIA = false;
    this.hideIC = false;
    this.region = null;
    this.threatFeedService.getTargetRegionAttackByIOC(this.from, this.to) .subscribe(
      res => {
        this.regionsReponse = res;
        this.spinner.hide('targetRegion-spinner');
        if (this.regionsReponse && this.regionsReponse.length > 0) {
          this.showRA = false;
          this.showRC = false;
          this.showIA = true;
          this.showIC = false;
          this.hideRA = false;
          this.hideRC = false;
          this.hideIA = false;
          this.hideIC = false;
          this.chartOption = {
            tooltip: {
              trigger: 'item',
              formatter: function (info: any) {
                const value = info.value;
                const name = info.name;
                return  "Region Name: " + name + '<br/>' + "Count: " + value;

              }
            },
            series: [
              {
                breadcrumb: {
                  show: false,
                  emptyItemWidth: 0
                },
                type: 'treemap',
                selectedMode: false,
                roam: false,
                data: [],
                levels: [],

              }
            ]
          };
          this.chartOption.series[0].data = [];
          for (let sec of this.regionsReponse) {
            const data = {
              name: sec.value,
              value: sec.count
            };
            this.sectors.push(data);
          }
          setTimeout(()=> {
            this.chartOption.series[0].data = [...this.sectors];
            this.chartOption.series[0].levels = [...this.caledarSettings.treemapSetting()];

            this.chartOption = {...this.chartOption};
          });
        } else {
          this.showRA = false;
          this.showRC = false;
          this.showIA = false;
          this.showIC = false;
          this.hideRA = false;
          this.hideRC = false;
          this.hideIA = true;
          this.hideIC = false;
        }
      },
      err => {
        this.error = err;
        this.spinner.hide('targetRegion-spinner');
        this.showRA = false;
        this.showRC = false;
        this.showIA = false;
        this.showIC = false;
        this.hideRA = false;
        this.hideRC = false;
        this.hideIA = true;
        this.hideIC = false;
        this.taostr.error('Some error occurred. Please refresh the page.');
      }
    );
  }
  getCountriesIndicator(event) {
    this.spinner.show('targetRegion-spinner');
    if (this.region === null || this.region === undefined || this.region === '') {
      this.region = event.data.name;
    }
    this.countries = [];
    this.threatFeedService.getTargetSectorCountriesByIOCR(this.region, this.from, this.to).subscribe(
      res => {
        this.countriesR = res;
        this.spinner.hide('targetRegion-spinner');
        if (this.countriesR && this.countriesR.length > 0) {
          this.showRA = false;
          this.showRC = false;
          this.showIA = false;
          this.showIC = true;
          this.hideRA = false;
          this.hideRC = false;
          this.hideIA = false;
          this.hideIC = false;
          this.chartOptionCountries = {
            tooltip: {
              trigger: 'item',
              formatter: function (info: any) {
                const value = info.value;
                const name = info.name;
                return  "Country Name: " + name + '<br/>' + "Count: " + value;

              }
            },

            series: [
              {
                breadcrumb: {
                  show: false,
                  emptyItemWidth: 0
                },
                type: 'treemap',
                selectedMode: false,
                roam: false,
                data: [],
                levels: [],

              }
            ]
          };
          this.chartOptionCountries.series[0].data = [];
          for (let sec of this.countriesR) {
            const data = {
              name: sec.value,
              id: sec.id,
              value: sec.count
            };
            this.countries.push(data);
          }
          setTimeout(()=> {
            this.chartOptionCountries.series[0].data = [...this.countries];
            this.chartOptionCountries.series[0].levels = [...this.caledarSettings.treemapSetting()];

            this.chartOptionCountries = {...this.chartOptionCountries};
          });
        } else {
          this.showRA = false;
          this.showRC = false;
          this.showIA = false;
          this.showIC = false;
          this.hideRA = false;
          this.hideRC = false;
          this.hideIA = false;
          this.hideIC = true;
        }
      },
      err => {
        this.error = err;
        this.spinner.hide('targetRegion-spinner');
        this.showRA = false;
        this.showRC = false;
        this.showIA = false;
        this.showIC = false;
        this.hideRA = false;
        this.hideRC = false;
        this.hideIA = false;
        this.hideIC = true;
        this.taostr.error('Some error occurred. Please refresh the page.');
      }
    );
  }
  routeToListing(event) {
    const country = event.data.id;
    this.router.navigate(['apt/attacksCampaignList'], {queryParams: {countries: country
        ,defaultPreferences: false,  from: this.from, to: this.to}});
  }
  routeToListingAttackCampaign(event) {
    const country = event.data.id;
    this.router.navigate(['apt/attacksCampaignList'], {queryParams: {countries: country,
        defaultPreferences: false, from: this.from, to: this.to, attackCampaign: true}});
  }
  ngOnInit() {
  }

  ngOnDestroy(): void {
  }
}
