import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {NgxSpinnerService} from "ngx-spinner";
import {DatePipe} from "@angular/common";
import {AptService} from '../../core-services/apt.service';

@Component({
    selector: 'app-aptdashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
    response: any;
    requestIncident: any;
    requestGroups: any;
    showTable: any = true;
    showTopGroups: any = true;
    showTopAttacks: any = true;
    totalGroups = 0;
    totalAttacks = 0;
    totalIndicators = 0;
    targetSectorAttackCampaign: any;
    showtargetSectorAttackCampaign: any = true;
    hidetargetSectorAttackCampaign: any = false;
    showtargetSectorIndicator: any = true;
    hidetargetSectorIndicator: any = false;
    targetSectorIndicator: any;
    from: any;
    to: any;
    sh: any = false;

    constructor(private aptDashboard: AptService, private router: Router, private toastr: ToastrService,
                private datepipe: DatePipe, private spinner: NgxSpinnerService) {
        this.getttotalAttacks();
        this.gettLastMonthTargetSector();
        this.getGroupsList();
        this.getAttacksList();
    }



    ngOnInit(): void {
        this.sh = false;
    }

    changeDatamapTab(event) {
        if (event.index === 1) {
            this.sh = true;
        }
    }

    getttotalAttacks() {
        this.spinner.show('totalAttack-spinner');
        this.totalGroups = 0;
        this.totalAttacks = 0;
        this.totalIndicators = 0;
        this.aptDashboard.getTotalGAI().subscribe(
            getTotalGAIResponse => {
                this.response = getTotalGAIResponse;
                this.spinner.hide('totalAttack-spinner');
                this.totalGroups = this.response.group;
                this.totalAttacks = this.response.attackCampaign;
                this.totalIndicators = this.response.indicator;
            },
            err => {
                this.spinner.hide('totalAttack-spinner');
                this.totalGroups = 0;
                this.totalAttacks = 0;
                this.totalIndicators = 0;
            }
        );
    }

    gettLastMonthTargetSector() {
        this.targetSectorAttackCampaign = [];
        this.targetSectorIndicator = [];
        this.spinner.show('sector-spinner');
        this.showtargetSectorAttackCampaign = true;
        this.hidetargetSectorAttackCampaign = false;
        this.showtargetSectorIndicator = true;
        this.hidetargetSectorIndicator = false;
        this.to = new Date();
        this.from = new Date();
        this.from = this.from.setMonth(new Date().getMonth() - 1);
        this.from = this.datepipe.transform(this.from, 'yyyy-MM-dd');
        this.to = this.datepipe.transform(this.to, 'yyyy-MM-dd');

        this.aptDashboard.getTargetSectorAttack(this.from, this.to).subscribe(
            getTargetSectorResponse => {
                this.targetSectorAttackCampaign = getTargetSectorResponse;
                this.spinner.hide('sector-spinner');
                if (this.targetSectorAttackCampaign && this.targetSectorAttackCampaign.length > 0) {
                    this.showtargetSectorAttackCampaign = true;
                    this.hidetargetSectorAttackCampaign = false;
                } else {
                    this.showtargetSectorAttackCampaign = false;
                    this.hidetargetSectorAttackCampaign = true;
                }

            },
            err => {
                this.spinner.hide('sector-spinner');
                this.showtargetSectorAttackCampaign = false;
                this.hidetargetSectorAttackCampaign = true;
            }
        );
        this.aptDashboard.getTargetSectorAttackByIOC(this.from, this.to).subscribe(
            getTargetSectorAttackByIOCResponse => {
                this.targetSectorIndicator = getTargetSectorAttackByIOCResponse;
                this.spinner.hide('sector-spinner');
                if (this.targetSectorIndicator && this.targetSectorIndicator.length > 0) {
                    this.showtargetSectorIndicator = true;
                    this.hidetargetSectorIndicator = false;
                } else {
                    this.showtargetSectorIndicator = false;
                    this.hidetargetSectorIndicator = true;
                }

            },
            err => {
                this.spinner.hide('sector-spinner');
                this.showtargetSectorIndicator = false;
                this.hidetargetSectorIndicator = true;
            }
        );
    }

    getGroupsList() {
        this.spinner.show('topGroups-spinner');
        this.aptDashboard.gettopGroups().subscribe(
            gettopGroupsResponse => {
                this.spinner.hide('topGroups-spinner');
                this.requestGroups = gettopGroupsResponse;
                if (this.requestGroups) {
                    this.showTopGroups = true;
                } else {
                    this.showTopGroups = false;
                }


            }, err => {
                this.spinner.hide('topGroups-spinner');
                this.showTopGroups = false;
                this.toastr.error('Some error occured. Please try again');

            }
        );
    }

    getAttacksList() {
        this.spinner.show('topAttacks-spinner');
        this.aptDashboard.gettopAttacks().subscribe(
            gettopAttacksResponse => {
                this.spinner.hide('topAttacks-spinner');
                this.requestIncident = gettopAttacksResponse;
                if (this.requestIncident) {
                    this.showTopAttacks = true;
                } else {
                    this.showTopAttacks = false;
                }


            }, err => {
                this.spinner.hide('topAttacks-spinner');
                this.showTopAttacks = false;
                this.toastr.error('Some error occured. Please try again');

            }
        );
    }

    viewAttackCampaign(id) {
        this.router.navigate(['apt/viewCampaign'], {queryParams: {id: id, send: true, lastPage: 'Dashboard'}});

    }
    viewGroups(id) {
        this.router.navigate(['apt/viewGroup'], {queryParams: {id: id, send: true, lastPage: 'Dashboard'}});

    }
    routetoCam(value) {
        this.router.navigate(['apt/attacksCampaignList'], {
            queryParams: {
                sectors: value,
                defaultPreferences: false,
                from: this.from,
                to: this.to
            }
        });

    }

    routetoCamA(value) {
        this.router.navigate(['apt/attacksCampaignList'], {
            queryParams: {
                sectors: value,
                defaultPreferences: false,
                from: this.from,
                to: this.to,
                attackCampaign: true
            }
        });

    }
}
