import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import {MultiSelect} from "primeng/multiselect";
import {ConfirmationService, MenuItem} from "primeng/api";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {ToastrService} from "ngx-toastr";
import {NgxSpinnerService} from "ngx-spinner";
import {AptService} from '../../core-services/apt.service';

@Component({
  selector: 'app-apt-group-create-form',
  templateUrl: './apt-group-create-form.component.html',
  styleUrls: ['./apt-group-create-form.component.scss'],
  providers: [ConfirmationService]
})
export class AptGroupCreateFormComponent implements OnInit {
  breadCrumbs: MenuItem[];

  createGroupForm: FormGroup;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  loading = false;
  hide = true;
  caseName: any = [];
  caseNameResult: any = [];
  name: string;
  types: any = [];
  value: any;
  values = [];
  submitNextOne = false;
  submitNextThree = false;
  regionResult: any = [];
  sectorResult: any = [];
  regions: any = [];
  sectors: any = [];
  techniqueName: any = [];
  techniqueId: any = [];
  techniqueNameResult: any = [];
  mitreInitialAccesstid: any = [];
  mitreInitialAccesstname: any = [];
  mitreExecutiontid: any = [];
  mitreExecutiontname: any = [];
  mitrePersistencetid: any = [];
  mitrePersistencetname: any = [];
  mitrePrivilegeEscalationtid: any = [];
  mitrePrivilegeEscalationtname: any = [];
  mitreDefenseEvasiontid: any = [];
  mitreDefenseEvasiontname: any = [];
  mitreCredentialAccesstid: any = [];
  mitreCredentialAccesstname: any = [];
  mitreDiscoverytid: any = [];
  mitreDiscoverytname: any = [];
  mitreLateralMovementtid: any = [];
  mitreLateralMovementname: any = [];
  mitreCollectiontid: any = [];
  mitreCollectiontname: any = [];
  mitreCommandAndControltid: any = [];
  mitreCommandAndControltname: any = [];
  mitreExfiltrationtid: any = [];
  mitreExfiltrationtname: any = [];
  mitreImpacttid: any = [];
  mitreImpacttname: any = [];
  mitreResourcetname: any = [];
  mitreResourcettid: any = [];
  mitreRecontid: any = [];
  mitreRecontname: any = [];

  techniqueIdResult: any = [];
  techniqueDetails: any = [];
  aliases: any = [];
  mitreIdArr: any = [];
  mitreValueArr: any = [];
  @ViewChild('aliasInput') aliasInput: ElementRef<HTMLInputElement>;
  groupData: any = [];
  caseId: any;
  isTrue = false;
  countryList = [];
  selectedItems = [];
  selectedItemsS = [];
  settings : any;
  settingsSector: any;

  @ViewChild('file') uploadElRef: ElementRef;
  display = 'none';
  fileList: any;
  @ViewChild('file') file;
  filesToUpload: Set<File> = new Set();
  filee: File;
  file_loading = false;
  spin = false;
  respose: any;
  groupDetails: any;

  reconopen = false;
  resopen = false;
  iaopen = false;
  exeopen = false;
  persopen = false;
  privopen = false;
  defEVAopen = false;
  credAccopen = false;
  discopen = false;
  lateralopen = false;
  collecopen = false;
  cacopen = false;
  exifopen = false;
  impopen = false;

  constructor(private formBuilder: FormBuilder, private ngZone: NgZone, private aptService: AptService,
              private confirmationService: ConfirmationService,
              private router: Router, private route: ActivatedRoute, private spinner: NgxSpinnerService,
              private toastr: ToastrService) {
    this.breadCrumbs = [
      {label: 'Group Listing', routerLink: '/apt/groupList', routerLinkActiveOptions: true},
      {label: 'Create Group'},
    ];

    this.mitreInitialAccesstid = [];
    this.mitreInitialAccesstname = [];
    this.mitreExecutiontid = [];
    this.mitreExecutiontname = [];
    this.mitrePersistencetid = [];
    this.mitrePersistencetname = [];
    this.mitrePrivilegeEscalationtid = [];
    this.mitrePrivilegeEscalationtname = [];
    this.mitreDefenseEvasiontid = [];
    this.mitreDefenseEvasiontname = [];
    this.mitreCredentialAccesstid = [];
    this.mitreCredentialAccesstname = [];
    this.mitreDiscoverytid = [];
    this.mitreDiscoverytname = [];
    this.mitreLateralMovementtid = [];
    this.mitreLateralMovementname = [];
    this.mitreCollectiontid = [];
    this.mitreCollectiontname = [];
    this.mitreCommandAndControltid = [];
    this.mitreCommandAndControltname = [];
    this.mitreExfiltrationtid = [];
    this.mitreExfiltrationtname = [];
    this.mitreImpacttid = [];
    this.mitreImpacttname = [];
    this.mitreResourcetname = [];
    this.mitreResourcettid = [];
    this.mitreRecontid = [];
    this.mitreRecontname = [];
    this.techniqueDetails = [];
    this.getLookupServices();
    this.initReactiveForm();
    // redirect to home if already logged in

  }

  ngOnInit(): void {
  }

  initReactiveForm() {
    this.createGroupForm = this.formBuilder.group({
      groupName: ['', Validators.required],
      mitreName: [''],
      alias: [''],
      summary: [''],
      sectors: [''],
      regions: [''],
      filess: [''],
      mitreReconnaissance: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreResourceDevelopment: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreInitialAccess: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreExecution: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitrePersistence: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitrePrivilegeEscalation: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreDefenseEvasion: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreCredentialAccess: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreDiscovery: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreLateralMovement: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreCollection: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreCommandAndControl: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreExfiltration: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      mitreImpact: this.formBuilder.array([this.mitreInitial('', '', '', '')]),
      attackCampaigns: this.formBuilder.array([this.attackCampaigns(0)])
    });
    this.reconopen = false;
    this.resopen = false;
    this.iaopen = false;
    this.exeopen = false;
    this.persopen = false;
    this.privopen = false;
    this.defEVAopen = false;
    this.credAccopen = false;
    this.discopen = false;
    this.lateralopen = false;
    this.collecopen = false;
    this.cacopen = false;
    this.exifopen = false;
    this.impopen = false;
  }

  onFilesAdded() {
    // for malware search
    this.spin = true;
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (const key in files) {
      if (!isNaN(parseInt(key, 10))) {
        this.filee = files[key];
        this.filesToUpload.add(files[key]);
      }
    }
    if (this.filesToUpload.size === 1) {
      this.uploadFile(this.filesToUpload);
    } else if (this.filesToUpload.size < 1) {
      this.toastr.error('You must select a file');
    } else {
      this.toastr.error('You can only select 1 file');
    }
  }

  public uploadFile(files: Set<File>): any {
    files.forEach(file => {
      const formData: FormData = new FormData();
      formData.append('file', file, file.name);
      this.file_loading = false;
      this.spinner.show('mitre-spinner');

      this.aptService.uploadGroupCV(0, formData).subscribe(
          uploadGroupCVRes => {
            this.spin = false;
            this.spinner.hide('mitre-spinner');

            files.clear();
            this.filesToUpload.clear();
            this.respose = uploadGroupCVRes;

            if (this.respose) {
              this.toastr.success(this.respose.responseDto.response);
              if (this.respose.rejectedTechniques && this.respose.rejectedTechniques.length > 0) {
                let rejectedTechniquesError = '';
                for (let tech of this.respose.rejectedTechniques) {
                  rejectedTechniquesError = rejectedTechniquesError + ' ' + tech;
                }
                this.toastr.error('Rejected Techniques: \n' + rejectedTechniquesError);
              }
              if (this.respose.techniques && this.respose.techniques.length > 0) {
                this.file_loading = true;
                this.getGroupDetails(this.respose.techniques);
              } else {
                this.file_loading = false;
                this.toastr.error('No MITRES in file.');
                this.uploadElRef.nativeElement.value = '';
                files.clear();
                this.filesToUpload.clear();
              }
            } else {
              this.toastr.error('Only CSV file is accepted Kindly Try again');
              this.uploadElRef.nativeElement.value = '';
              files.clear();
              this.filesToUpload.clear();
            }

          },
          error => {
            this.spin = false;
            this.spinner.hide('mitre-spinner');
            let err = error.error;
            err = err.responseDto;
            this.file_loading = false;
            this.uploadElRef.nativeElement.value = '';
            files.clear();
            this.filesToUpload.clear();
            this.toastr.error(err.response);
          });
    });
  }

  getGroupDetails(iocs) {
    this.groupDetails = iocs;
    let mitreInitialAccess: any = [];
    let mitreExecution: any = [];
    let mitrePersistence: any = [];
    let mitrePrivilegeEscalation: any = [];
    let mitreDefenseEvasion: any = [];
    let mitreCredentialAccess: any = [];
    let mitreDiscovery: any = [];
    let mitreLateralMovement: any = [];
    let mitreCollection: any = [];
    let mitreCommandAndControl: any = [];
    let mitreExfiltration: any = [];
    let mitreImpact: any = [];
    let mitreReconnaissance: any = [];
    let mitreResourceDevelopment: any = [];
    if (this.groupDetails && this.groupDetails.length > 0) {
      this.groupDetails.map(item => {
        if (item.technique.techniqueTypes && item.technique.techniqueTypes.length > 0) {
          if (this.techniqueDetails.find((test) => test.techniqueId === item.technique.techniqueId) === undefined) {
            this.techniqueDetails.push(item.technique);
          }
          item.technique.techniqueTypes.map(techniqueIndex => {
            if (techniqueIndex.id === 1) {
              mitreInitialAccess.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 1
                }
              });
            } else if (techniqueIndex.id === 2) {
              mitreExecution.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 2
                }
              });
            } else if (techniqueIndex.id === 3) {
              mitrePersistence.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 3
                }
              });
            } else if (techniqueIndex.id === 4) {
              mitrePrivilegeEscalation.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 4
                }
              });
            } else if (techniqueIndex.id === 5) {
              mitreDefenseEvasion.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 5
                }
              });
            } else if (techniqueIndex.id === 6) {
              mitreCredentialAccess.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 6
                }
              });
            } else if (techniqueIndex.id === 7) {
              mitreDiscovery.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 7
                }
              });
            } else if (techniqueIndex.id === 8) {
              mitreLateralMovement.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 8
                }
              });
            } else if (techniqueIndex.id === 9) {
              mitreCollection.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 9
                }
              });
            } else if (techniqueIndex.id === 10) {
              mitreCommandAndControl.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 10
                }
              });
            } else if (techniqueIndex.id === 11) {
              mitreExfiltration.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 11
                }
              });
            } else if (techniqueIndex.id === 12) {
              mitreImpact.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 12
                }
              });
            } else if (techniqueIndex.id === 13) {
              mitreReconnaissance.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 13
                }
              });
            } else if (techniqueIndex.id === 14) {
              mitreResourceDevelopment.push({
                description: item.description,
                name: item.technique.name,
                techniqueId: item.technique.techniqueId,
                id: item.technique.id,
                techniqueTypes: {
                  id: 14
                }
              });
            }
          });
        }
      });
    }

    if (mitreInitialAccess && mitreInitialAccess.length > 0) {
      this.iaopen = true;
      this.getTechniqueByindx(2);

      const control = this.createGroupForm.controls.mitreInitialAccess as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreInitialAccess.length; mitreIndex++) {
        const edd = mitreInitialAccess[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreExecution && mitreExecution.length > 0) {
      this.exeopen = true;
      this.getTechniqueByindx(3);

      const control = this.createGroupForm.controls.mitreExecution as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreExecution.length; mitreIndex++) {
        const edd = mitreExecution[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitrePersistence && mitrePersistence.length > 0) {
      this.persopen = true;
      this.getTechniqueByindx(4);

      const control = this.createGroupForm.controls.mitrePersistence as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitrePersistence.length; mitreIndex++) {
        const edd = mitrePersistence[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitrePrivilegeEscalation && mitrePrivilegeEscalation.length > 0) {
      this.privopen = true;
      this.getTechniqueByindx(5);

      const control = this.createGroupForm.controls.mitrePrivilegeEscalation as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitrePrivilegeEscalation.length; mitreIndex++) {
        const edd = mitrePrivilegeEscalation[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreDefenseEvasion && mitreDefenseEvasion.length > 0) {
      this.defEVAopen = true;
      this.getTechniqueByindx(6);

      const control = this.createGroupForm.controls.mitreDefenseEvasion as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreDefenseEvasion.length; mitreIndex++) {
        const edd = mitreDefenseEvasion[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreCredentialAccess && mitreCredentialAccess.length > 0) {
      this.credAccopen = true;
      this.getTechniqueByindx(7);

      const control = this.createGroupForm.controls.mitreCredentialAccess as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreCredentialAccess.length; mitreIndex++) {
        const edd = mitreCredentialAccess[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreDiscovery && mitreDiscovery.length > 0) {
      this.discopen = true;
      this.getTechniqueByindx(8);

      const control = this.createGroupForm.controls.mitreDiscovery as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreDiscovery.length; mitreIndex++) {
        const edd = mitreDiscovery[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreLateralMovement && mitreLateralMovement.length > 0) {
      this.lateralopen = true;
      this.getTechniqueByindx(9);

      const control = this.createGroupForm.controls.mitreLateralMovement as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreLateralMovement.length; mitreIndex++) {
        const edd = mitreLateralMovement[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreCollection && mitreCollection.length > 0) {
      this.collecopen = true;
      this.getTechniqueByindx(10);

      const control = this.createGroupForm.controls.mitreCollection as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreCollection.length; mitreIndex++) {
        const edd = mitreCollection[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreCommandAndControl && mitreCommandAndControl.length > 0) {
      this.cacopen = true;
      this.getTechniqueByindx(11);

      const control = this.createGroupForm.controls.mitreCommandAndControl as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreCommandAndControl.length; mitreIndex++) {
        const edd = mitreCommandAndControl[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreExfiltration && mitreExfiltration.length > 0) {
      this.exifopen = true;
      this.getTechniqueByindx(12);

      const control = this.createGroupForm.controls.mitreExfiltration as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreExfiltration.length; mitreIndex++) {
        const edd = mitreExfiltration[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreReconnaissance && mitreReconnaissance.length > 0) {
      this.reconopen = true;
      this.getTechniqueByindx(0);

      const control = this.createGroupForm.controls.mitreReconnaissance as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreReconnaissance.length; mitreIndex++) {
        const edd = mitreReconnaissance[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreResourceDevelopment && mitreResourceDevelopment.length > 0) {
      this.resopen = true;
      this.getTechniqueByindx(1);

      const control = this.createGroupForm.controls.mitreResourceDevelopment as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreResourceDevelopment.length; mitreIndex++) {
        const edd = mitreResourceDevelopment[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
    if (mitreImpact && mitreImpact.length > 0) {
      this.impopen = true;
      this.getTechniqueByindx(13);

      const control = this.createGroupForm.controls.mitreImpact as FormArray;
      control.removeAt(0);
      control[0] = [];
      for (let mitreIndex = 0; mitreIndex < mitreImpact.length; mitreIndex++) {
        const edd = mitreImpact[mitreIndex];
        control.push(this.patchMitreInitial(edd.techniqueId, edd.name, edd.id, edd.description));
      }
    }
  }

  patchMitreInitial(techniqueId, name, id, description): FormGroup {
    return this.formBuilder.group({
      techniqueId: [techniqueId],
      name: [name, Validators.required],
      id: [id],
      description: [description]
    });
  }

  processString(name) {
    const str = name.replace(/([A-Z])/g, ' $1');
    const first = str.substr(0, 1).toUpperCase();
    return first + str.substr(1);
  }

  viewGroups(id) {
    this.router.navigate(['apt/viewGroup'], {queryParams: {id: id, send: false, lastPage: 'Create Group'}});

  }


  add(event): void {
    const input = event.input;
    let value = event.value;

    if ((value || '').trim()) {
      value = value.trim();
      if (this.aliases.find((test) => test.value.toLowerCase() === value.toLowerCase()) === undefined) {
        this.aliases.push({value: value.trim()});
        this.createGroupForm.value.alias = this.aliases;
      }
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(fruit: any): void {
    const index = this.aliases.indexOf(fruit);

    if (index >= 0) {
      this.aliases.splice(index, 1);
      this.createGroupForm.value.alias = this.aliases;
    }
  }

  // convenience getter for easy access to form fields
  get errorControl() {
    return this.createGroupForm.controls;
  }


  /*--------------Submit form------------------*/
  onSubmit() {
    let mitreReconnaissance = this.createGroupForm.value.mitreReconnaissance.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 13
          }
        };
      }
    });
    let mitreResourceDevelopment = this.createGroupForm.value.mitreResourceDevelopment.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 14
          }
        };
      }
    });
    let mitreInitial = this.createGroupForm.value.mitreInitialAccess.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 1
          }
        };
      }
    });
    let mitreExecution = this.createGroupForm.value.mitreExecution.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 2
          }
        };
      }
    });

    let mitrePersistence = this.createGroupForm.value.mitrePersistence.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 3
          }
        };
      }
    });
    let mitrePrivilegeEscalation = this.createGroupForm.value.mitrePrivilegeEscalation.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 4
          }
        };
      }
    });
    let mitreDefenseEvasion = this.createGroupForm.value.mitreDefenseEvasion.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 5
          }
        };
      }
    });
    let mitreCredentialAccess = this.createGroupForm.value.mitreCredentialAccess.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 6
          }
        };
      }
    });
    let mitreDiscovery = this.createGroupForm.value.mitreDiscovery.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 7
          }
        };
      }
    });
    let mitreLateralMovement = this.createGroupForm.value.mitreLateralMovement.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 8
          }
        };
      }
    });
    let mitreCollection = this.createGroupForm.value.mitreCollection.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 12
          }
        };
      }
    });
    let mitreCommandAndControl = this.createGroupForm.value.mitreCommandAndControl.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 9
          }
        };
      }
    });
    let mitreExfiltration: any = this.createGroupForm.value.mitreExfiltration.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 10
          }
        };
      }
    });
    let mitreImpact: any = this.createGroupForm.value.mitreImpact.map(item => {
      if (item.name !== "" && item.techniqueId !== "") {
        return {
          description: item.description,
          name: item.name,
          techniqueId: item.techniqueId,
          id: item.id,
          techniqueTypes: {
            id: 11
          }
        };
      }
    });
    let newArray = [];
    this.mitreIdArr = [];
    this.mitreValueArr = [];
    if (mitreImpact.length === 1 && (mitreImpact[0] === null || mitreImpact[0] === undefined)) {
      mitreImpact = [];
    } else {
      newArray.push(...mitreImpact);
    }
    if (mitreExfiltration.length === 1 && (mitreExfiltration[0] === null || mitreExfiltration[0] === undefined)) {
      mitreExfiltration = [];
    } else {
      newArray.push(...mitreExfiltration);
    }
    if (mitreCommandAndControl.length === 1 && (mitreCommandAndControl[0] === null || mitreCommandAndControl[0] === undefined)) {
      mitreCommandAndControl = [];
    } else {
      newArray.push(...mitreCommandAndControl);
    }
    if (mitreCollection.length === 1 && (mitreCollection[0] === null || mitreCollection[0] === undefined)) {
      mitreCollection = [];
    } else {
      newArray.push(...mitreCollection);
    }
    if (mitreExecution.length === 1 && (mitreExecution[0] === null || mitreExecution[0] === undefined)) {
      mitreExecution = [];
    } else {
      newArray.push(...mitreExecution);
    }
    if (mitreLateralMovement.length === 1 && (mitreLateralMovement[0] === null || mitreLateralMovement[0] === undefined)) {
      mitreLateralMovement = [];
    } else {
      newArray.push(...mitreLateralMovement);
    }
    if (mitreDiscovery.length === 1 && (mitreDiscovery[0] === null || mitreDiscovery[0] === undefined)) {
      mitreDiscovery = [];
    } else {
      newArray.push(...mitreDiscovery);
    }
    if (mitreCredentialAccess.length === 1 && (mitreCredentialAccess[0] === null || mitreCredentialAccess[0] === undefined)) {
      mitreCredentialAccess = [];
    } else {
      newArray.push(...mitreCredentialAccess);
    }
    if (mitreDefenseEvasion.length === 1 && (mitreDefenseEvasion[0] === null || mitreDefenseEvasion[0] === undefined)) {
      mitreDefenseEvasion = [];
    } else {
      newArray.push(...mitreDefenseEvasion);
    }
    if (mitrePrivilegeEscalation.length === 1 && (mitrePrivilegeEscalation[0] === null || mitrePrivilegeEscalation[0] === undefined)) {
      mitrePrivilegeEscalation = [];
    } else {
      newArray.push(...mitrePrivilegeEscalation);
    }
    if (mitrePersistence.length === 1 && (mitrePersistence[0] === null || mitrePersistence[0] === undefined)) {
      mitrePersistence = [];
    } else {
      newArray.push(...mitrePersistence);
    }
    if (mitreReconnaissance.length === 1 && (mitreReconnaissance[0] === null || mitreReconnaissance[0] === undefined)) {
      mitreReconnaissance = [];
    } else {
      newArray.push(...mitreReconnaissance);
    }
    if (mitreResourceDevelopment.length === 1 && (mitreResourceDevelopment[0] === null || mitreResourceDevelopment[0] === undefined)) {
      mitreResourceDevelopment = [];
    } else {
      newArray.push(...mitreResourceDevelopment);
    }
    if (mitreInitial.length === 1 && (mitreInitial[0] === null || mitreInitial[0] === undefined)) {
      mitreInitial = [];
    } else {
      newArray.push(...mitreInitial);
    }
    this.mitreValueArr = [];
    this.mitreIdArr = [];
    if (this.techniqueDetails && this.techniqueDetails.length > 0) {
      this.techniqueDetails.map((groupFormObj, tecniqueIndex) => {
        const idArr: any = [];
        newArray.map(x => {
          if (groupFormObj && groupFormObj.id !== '' && x.id !== '' && groupFormObj.id === x.id && groupFormObj.id !== undefined && x.id !== undefined) {
            idArr.push({id: x.techniqueTypes.id});
            this.mitreIdArr.push({
              description: x.description,
              name: x.name,
              id: x.id,
              techniqueId: x.techniqueId,
              techniqueTypes: idArr
            });
          } else if (x.id === '') {
            this.mitreValueArr.push({
              description: x.description,
              name: x.name,
              techniqueId: x.techniqueId,
              techniqueTypes: [{id: x.techniqueTypes.id}]
            });
          }
        });
      });

    } else {
      newArray.map(x => {
        this.mitreValueArr.push({
          description: x.description,
          name: x.name,
          techniqueId: x.techniqueId,
          techniqueTypes: [{id: x.techniqueTypes.id}]
        });
      });
    }
    const techniquesArr = [];
    if (this.mitreValueArr && this.mitreValueArr.length > 0) {
      const dataArray = this.mitreValueArr.filter((item, tecniqueIndex, arr) =>
          arr.findIndex((x) => (x.techniqueId === item.techniqueId)) === tecniqueIndex);
      techniquesArr.push(...dataArray);
    }
    if (this.mitreIdArr && this.mitreIdArr.length > 0) {
      const newd = [];
      const dataArray = this.mitreIdArr.filter((item, tecniqueIndex, arr) =>
          arr.findIndex((x) => (x.techniqueId === item.techniqueId)) === tecniqueIndex);
      dataArray.map(item => item).filter(tecniqueIndex => {
        const updateArr = tecniqueIndex.techniqueTypes.map(tecniqueIndex => tecniqueIndex.id).filter((value, index, self) => self.indexOf(value) === index);
        const data = {
          description: tecniqueIndex.description,
          name: tecniqueIndex.name,
          id: tecniqueIndex.id,
          techniqueId: tecniqueIndex.techniqueId,
          techniqueTypes: updateArr.map(tecniqueIndex => {
            return {id: tecniqueIndex}
          })
        };
        newd.push(data);
      });
      techniquesArr.push(...newd);
    }
    let attackCampaigns: any = this.createGroupForm.value.attackCampaigns.map(item => {
      this.isTrue = false;
      if (item.associatedIndicators !== "" && item.caseName !== "" && (item.regions && item.regions.length > 0) && (item.sectors && item.sectors.length > 0)) {
        this.groupData.map(tecniqueIndex => {
          if (item.caseName === tecniqueIndex.caseName) {
            if (tecniqueIndex.caseName) {
              this.isTrue = true;
              this.caseId = tecniqueIndex.id;
            }
          }
        });
        if (this.isTrue) {
          return {
            associatedIndicators: item.associatedIndicators,
            caseName: item.caseName,
            id: this.caseId,
            countries: item.regions,
            sectors: item.sectors
          };
        } else {
          return {
            associatedIndicators: item.associatedIndicators,
            caseName: item.caseName,
            countries: item.regions,
            sectors: item.sectors
          };
        }
      }
    });
    if (attackCampaigns.length === 1 && (attackCampaigns[0] === null || attackCampaigns[0] === undefined)) {
      attackCampaigns = [];
    }
    if (this.createGroupForm.value.regions === '') {
      this.createGroupForm.value.regions = [];
    }
    if (this.createGroupForm.value.sectors === '') {
      this.createGroupForm.value.sectors = [];
    }
    const dataJson = {
      aliases: this.aliases,
      groupName: this.createGroupForm.value.groupName,
      mitreName: this.createGroupForm.value.mitreName,
      summary: this.createGroupForm.value.summary,
      attackCampaigns: attackCampaigns,
      techniques: techniquesArr,
      countries: this.createGroupForm.value.regions,
      sectors: this.createGroupForm.value.sectors
    };
    if (dataJson.groupName !== '') {
      this.spinner.show('creategroup-spinner');
      this.aptService.postGroups(dataJson).subscribe(
          res => {
            const response: any = res;
            this.spinner.hide('creategroup-spinner');

            if (response.responseDto.responseCode === 201 || response.responseDto.responseCode === 200) {
              this.toastr.success(response.responseDto.response);
              this.viewGroups(response.id);
            } else {
              this.toastr.error('Some error occurred while creating');
            }
          },
          error => {
            this.spinner.hide('creategroup-spinner');
            this.toastr.error('Some error occurred while creating');
          });
    } else {
      if (dataJson.groupName === '') {
        this.toastr.error('Group Name is required');

      }
    }

  }

  /*-----------------------------------------------------------------------------*/

  /*--------------Form Array Fields - Initialize Form Array - Add item to form array - Remove item from form array------------------*/
  mitreInitial(techId, name, description, id): FormGroup {
    return this.formBuilder.group({
      techniqueId: [techId],
      name: [name, Validators.required],
      description: [description],
      id: [id]
    });
  }

  attackCampaigns(ind): FormGroup {
    return this.formBuilder.group({
      associatedIndicators: [ind],
      caseName: ['', Validators.required],
      caseId: [''],
      regions: [''],
      sectors: ['']
    });
  }

  pattackCampaigns(ind, caseName, countries, sectors): FormGroup {
    return this.formBuilder.group({
      associatedIndicators: [ind],
      caseName: [caseName, Validators.required],
      caseId: [''],
      regions: [countries],
      sectors: [sectors],
    });
  }

  addAttackCampaigns() {
    const control = this.createGroupForm.controls.attackCampaigns as FormArray;
    control.push(this.attackCampaigns(0));
  }

  patchIndiactorDetails(event, type, tecniqueIndex) {
    const sector = [];
    const countries = [];
    if (event.caseName.value) {
      this.groupData.filter((groupFormObj, j) => {
        if (event.caseName.value === groupFormObj.caseName) {
          const control = this.createGroupForm.get(type)['controls'];
          if (control[tecniqueIndex].controls) {
            if (groupFormObj.sectors && groupFormObj.sectors.length > 0) {
              for (let sec of groupFormObj.sectors) {
                const data = {
                  id: sec.id,
                  itemName: sec.value,
                };
                sector.push(data);

              }
              groupFormObj.sectors = sector;

            }
            if (groupFormObj.countries && groupFormObj.countries.length > 0) {
              for (let region of groupFormObj.countries) {
                const data = {
                  id: region.id,
                  itemName: region.countryName,
                  category: region.region
                };
                countries.push(data);
              }
              groupFormObj.countries = countries;
            }
            control[tecniqueIndex] = this.pattackCampaigns(groupFormObj.indicatorCount, groupFormObj.caseName, countries, sector);
          }
        }
      });
    }
  }
  addMitreInitial(value) {
    if (value === 'Reconnaissance') {
      const control = this.createGroupForm.controls.mitreReconnaissance as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Resource Development') {
      const control = this.createGroupForm.controls.mitreResourceDevelopment as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Initial Access') {
      const control = this.createGroupForm.controls.mitreInitialAccess as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Execution') {
      const control = this.createGroupForm.controls.mitreExecution as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Persistence') {
      const control = this.createGroupForm.controls.mitrePersistence as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Privilege Escalation') {
      const control = this.createGroupForm.controls.mitrePrivilegeEscalation as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Defense Evasion') {
      const control = this.createGroupForm.controls.mitreDefenseEvasion as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Credential Access') {
      const control = this.createGroupForm.controls.mitreCredentialAccess as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Discovery') {
      const control = this.createGroupForm.controls.mitreDiscovery as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Lateral Movement') {
      const control = this.createGroupForm.controls.mitreLateralMovement as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Collection') {
      const control = this.createGroupForm.controls.mitreCollection as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Command and Control') {
      const control = this.createGroupForm.controls.mitreCommandAndControl as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Exfiltration') {
      const control = this.createGroupForm.controls.mitreExfiltration as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    } else if (value === 'Impact') {
      const control = this.createGroupForm.controls.mitreImpact as FormArray;
      control.push(this.mitreInitial('', '', '', ''));
    }
  }

  removeMitreInitial(tecniqueIndex: number, value) {
    if (value === 'Reconnaissance') {
      const control = this.createGroupForm.controls.mitreReconnaissance as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Resource Development') {
      const control = this.createGroupForm.controls.mitreResourceDevelopment as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Initial Access') {
      const control = this.createGroupForm.controls.mitreInitialAccess as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Execution') {
      const control = this.createGroupForm.controls.mitreExecution as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Persistence') {
      const control = this.createGroupForm.controls.mitrePersistence as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Privilege Escalation') {
      const control = this.createGroupForm.controls.mitrePrivilegeEscalation as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Defense Evasion') {
      const control = this.createGroupForm.controls.mitreDefenseEvasion as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Credential Access') {
      const control = this.createGroupForm.controls.mitreCredentialAccess as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Discovery') {
      const control = this.createGroupForm.controls.mitreDiscovery as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Lateral Movement') {
      const control = this.createGroupForm.controls.mitreLateralMovement as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Collection') {
      const control = this.createGroupForm.controls.mitreCollection as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Command and Control') {
      const control = this.createGroupForm.controls.mitreCommandAndControl as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Exfiltration') {
      const control = this.createGroupForm.controls.mitreExfiltration as FormArray;
      control.removeAt(tecniqueIndex);
    } else if (value === 'Impact') {
      const control = this.createGroupForm.controls.mitreImpact as FormArray;
      control.removeAt(tecniqueIndex);
    }
  }

  removeAttackCampaigns(tecniqueIndex: number) {
    const control = this.createGroupForm.controls.attackCampaigns as FormArray;
    control.removeAt(tecniqueIndex);
  }

  // in case of zero
  removeAttackCampaignsZ(tecniqueIndex: number) {
    const control = this.createGroupForm.controls.attackCampaigns as FormArray;
    if (control.length <= 1) {
      const control = this.createGroupForm.get('attackCampaigns')['controls'];
      control[tecniqueIndex]['controls'].associatedIndicators.patchValue('');
      control[tecniqueIndex]['controls'].caseName.patchValue('');
      control[tecniqueIndex]['controls'].caseId.patchValue('');
      control[tecniqueIndex]['controls'].regions.patchValue('');
      control[tecniqueIndex]['controls'].sectors.patchValue('');
      control[tecniqueIndex]['controls'].associatedIndicators.updateValueAndValidity();
      control[tecniqueIndex]['controls'].caseName.updateValueAndValidity();
      control[tecniqueIndex]['controls'].caseId.updateValueAndValidity();
      control[tecniqueIndex]['controls'].regions.updateValueAndValidity();
      control[tecniqueIndex]['controls'].sectors.updateValueAndValidity();
    } else if (control.length > 1) {
      control.removeAt(tecniqueIndex);

    }
  }

  removeMitreInitialZ(tecniqueIndex: number, type) {
    const control = this.createGroupForm.controls[type] as FormArray;
    if (control.length <= 1) {
      const control = this.createGroupForm.get(type)['controls'];
      control[tecniqueIndex]['controls'].techniqueId.patchValue('');
      control[tecniqueIndex]['controls'].name.patchValue('');
      control[tecniqueIndex]['controls'].description.patchValue('');
      control[tecniqueIndex]['controls'].id.patchValue('');
      control[tecniqueIndex]['controls'].techniqueId.updateValueAndValidity();
      control[tecniqueIndex]['controls'].name.updateValueAndValidity();
      control[tecniqueIndex]['controls'].description.updateValueAndValidity();
      control[tecniqueIndex]['controls'].id.updateValueAndValidity();

    } else if (control.length > 1) {
      control.removeAt(tecniqueIndex);

    }
  }
  searchTechniqueById(event, index): void {
    index = Number(index);
    if (index === 0) {
      this.techniqueIdResult = this.mitreRecontid.filter(groupFormObj => groupFormObj.startsWith(event.query));

    } else if (index === 1) {
      this.techniqueIdResult = this.mitreResourcettid.filter(groupFormObj => groupFormObj.startsWith(event.query));

    } else if (index === 2) {
      this.techniqueIdResult = this.mitreInitialAccesstid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 3) {
      this.techniqueIdResult = this.mitreExecutiontid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 4) {
      this.techniqueIdResult = this.mitrePersistencetid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 5) {
      this.techniqueIdResult = this.mitrePrivilegeEscalationtid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 6) {
      this.techniqueIdResult = this.mitreDefenseEvasiontid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 7) {
      this.mitreCredentialAccesstid = this.techniqueId;
      this.techniqueIdResult = this.techniqueId.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 8) {
      this.techniqueIdResult = this.mitreDiscoverytid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 9) {
      this.techniqueIdResult = this.mitreLateralMovementtid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 10) {
      this.techniqueIdResult = this.mitreCollectiontid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 11) {
      this.techniqueIdResult = this.mitreCommandAndControltid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 12) {
      this.techniqueIdResult = this.mitreExfiltrationtid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    } else if (index === 13) {
      this.techniqueIdResult = this.mitreImpacttid.filter(groupFormObj => groupFormObj.startsWith(event.query));
    }
  }

  patchMitreDetails(event, type, tecniqueIndex, category) {
    let tech;
    if (category === 'id') {
      tech = event.techniqueId.value;
    } else {
      tech = event.name.value;
    }
    this.techniqueDetails.filter((groupFormObj, j) => {
      if (tech && tech !== null && tech !== undefined && tech !== '') {
        if (event.techniqueId.value === groupFormObj.techniqueId || event.name.value === groupFormObj.name) {
          const control = this.createGroupForm.get(type)['controls'];
          if (control[tecniqueIndex].controls) {
            control[tecniqueIndex]['controls'].name.patchValue(groupFormObj.name);
            control[tecniqueIndex]['controls'].id.patchValue(groupFormObj.id);
            control[tecniqueIndex]['controls'].id.updateValueAndValidity();
            control[tecniqueIndex]['controls'].name.updateValueAndValidity();
          }
        }
      } else {
        const control = this.createGroupForm.get(type)['controls'];
        if (control[tecniqueIndex].controls) {
          control[tecniqueIndex]['controls'].name.patchValue('');
          control[tecniqueIndex]['controls'].id.patchValue('');
          control[tecniqueIndex]['controls'].id.updateValueAndValidity();
          control[tecniqueIndex]['controls'].name.updateValueAndValidity();
        }
      }
    });
  }

  clear(type, tecniqueIndex) {
    const control = this.createGroupForm.get(type)['controls'];
    if (control[tecniqueIndex].controls) {
      control[tecniqueIndex]['controls'].name.patchValue('');
      control[tecniqueIndex]['controls'].id.patchValue('');
      control[tecniqueIndex]['controls'].id.updateValueAndValidity();
      control[tecniqueIndex]['controls'].name.updateValueAndValidity();
    }
  }

  getTechnique(e) {
    const index = e.index;
    let id;
    if (index === 0) {
      id = 13;
    } else if (index === 1) {
      id = 14;

    } else if (index === 2) {
      id = 1;

    } else if (index === 3) {
      id = 2;

    } else if (index === 4) {
      id = 3;

    } else if (index === 5) {
      id = 4;

    } else if (index === 6) {
      id = 5;

    } else if (index === 7) {
      id = 6;

    } else if (index === 8) {
      id = 7;

    } else if (index === 9) {
      id = 8;

    } else if (index === 10) {
      id = 12;

    } else if (index === 11) {
      id = 9;

    } else if (index === 12) {
      id = 10;

    } else if (index === 13) {
      id = 11;

    }

    this.aptService.getTechniquesById(id).subscribe(
        res => {
          const result: any = res;
          this.techniqueName = [];
          this.techniqueId = [];
          if (result && result.length > 0) {
            result.map(item => {

              if ((item.name && item.name !== undefined)
                  && (item.techniqueId && item.techniqueId !== undefined)) {

                if (this.techniqueDetails.find((test) => test.techniqueId === item.techniqueId) === undefined) {
                  this.techniqueDetails.push(item);
                }
                this.techniqueName.push(item.name);
                this.techniqueId.push(item.techniqueId);

              }
            });
            if (index === 0) {
              this.mitreRecontid = this.techniqueId;
              this.mitreRecontname = this.techniqueName;
            } else if (index === 1) {
              this.mitreResourcettid = this.techniqueId;
              this.mitreResourcetname = this.techniqueName;
            } else if (index === 2) {
              this.mitreInitialAccesstid = this.techniqueId;
              this.mitreInitialAccesstname = this.techniqueName;
            } else if (index === 3) {
              this.mitreExecutiontid = this.techniqueId;
              this.mitreExecutiontname = this.techniqueName;
            } else if (index === 4) {
              this.mitrePersistencetid = this.techniqueId;
              this.mitrePersistencetname = this.techniqueName;
            } else if (index === 5) {
              this.mitrePrivilegeEscalationtid = this.techniqueId;
              this.mitrePrivilegeEscalationtname = this.techniqueName;
            } else if (index === 6) {
              this.mitreDefenseEvasiontid = this.techniqueId;
              this.mitreDefenseEvasiontname = this.techniqueName;
            } else if (index === 7) {
              this.mitreCredentialAccesstid = this.techniqueId;
              this.mitreCredentialAccesstname = this.techniqueName;
            } else if (index === 8) {
              this.mitreDiscoverytid = this.techniqueId;
              this.mitreDiscoverytname = this.techniqueName;
            } else if (index === 9) {
              this.mitreLateralMovementtid = this.techniqueId;
              this.mitreLateralMovementname = this.techniqueName;
            } else if (index === 10) {
              this.mitreCollectiontid = this.techniqueId;
              this.mitreCollectiontname = this.techniqueName;
            } else if (index === 11) {
              this.mitreCommandAndControltid = this.techniqueId;
              this.mitreCommandAndControltname = this.techniqueName;
            } else if (index === 12) {
              this.mitreExfiltrationtid = this.techniqueId;
              this.mitreExfiltrationtname = this.techniqueName;
            } else if (index === 13) {
              this.mitreImpacttid = this.techniqueId;
              this.mitreImpacttname = this.techniqueName;
            }
          } else {
            this.techniqueDetails = [];
            this.techniqueName = [];
            this.techniqueId = [];
          }
        },
        err => {
        }
    );
  }

  getTechniqueByindx(e) {
    const index = e;
    let id;
    if (index === 0) {
      id = 13;
    } else if (index === 1) {
      id = 14;

    } else if (index === 2) {
      id = 1;

    } else if (index === 3) {
      id = 2;

    } else if (index === 4) {
      id = 3;

    } else if (index === 5) {
      id = 4;

    } else if (index === 6) {
      id = 5;

    } else if (index === 7) {
      id = 6;

    } else if (index === 8) {
      id = 7;

    } else if (index === 9) {
      id = 8;

    } else if (index === 10) {
      id = 12;

    } else if (index === 11) {
      id = 9;

    } else if (index === 12) {
      id = 10;

    } else if (index === 13) {
      id = 11;

    }

    this.aptService.getTechniquesById(id).subscribe(
        res => {
          const result: any = res;
          this.techniqueName = [];
          this.techniqueId = [];
          if (result && result.length > 0) {
            result.map(item => {

              if ((item.name && item.name !== undefined)
                  && (item.techniqueId && item.techniqueId !== undefined)) {

                if (this.techniqueDetails.find((test) => test.techniqueId === item.techniqueId) === undefined) {
                  this.techniqueDetails.push(item);
                }
                this.techniqueName.push(item.name);
                this.techniqueId.push(item.techniqueId);

              }
            });
            if (index === 0) {
              this.mitreRecontid = this.techniqueId;
              this.mitreRecontname = this.techniqueName;
            } else if (index === 1) {
              this.mitreResourcettid = this.techniqueId;
              this.mitreResourcetname = this.techniqueName;
            } else if (index === 2) {
              this.mitreInitialAccesstid = this.techniqueId;
              this.mitreInitialAccesstname = this.techniqueName;
            } else if (index === 3) {
              this.mitreExecutiontid = this.techniqueId;
              this.mitreExecutiontname = this.techniqueName;
            } else if (index === 4) {
              this.mitrePersistencetid = this.techniqueId;
              this.mitrePersistencetname = this.techniqueName;
            } else if (index === 5) {
              this.mitrePrivilegeEscalationtid = this.techniqueId;
              this.mitrePrivilegeEscalationtname = this.techniqueName;
            } else if (index === 6) {
              this.mitreDefenseEvasiontid = this.techniqueId;
              this.mitreDefenseEvasiontname = this.techniqueName;
            } else if (index === 7) {
              this.mitreCredentialAccesstid = this.techniqueId;
              this.mitreCredentialAccesstname = this.techniqueName;
            } else if (index === 8) {
              this.mitreDiscoverytid = this.techniqueId;
              this.mitreDiscoverytname = this.techniqueName;
            } else if (index === 9) {
              this.mitreLateralMovementtid = this.techniqueId;
              this.mitreLateralMovementname = this.techniqueName;
            } else if (index === 10) {
              this.mitreCollectiontid = this.techniqueId;
              this.mitreCollectiontname = this.techniqueName;
            } else if (index === 11) {
              this.mitreCommandAndControltid = this.techniqueId;
              this.mitreCommandAndControltname = this.techniqueName;
            } else if (index === 12) {
              this.mitreExfiltrationtid = this.techniqueId;
              this.mitreExfiltrationtname = this.techniqueName;
            } else if (index === 13) {
              this.mitreImpacttid = this.techniqueId;
              this.mitreImpacttname = this.techniqueName;
            }
          } else {
            this.techniqueDetails = [];
            this.techniqueName = [];
            this.techniqueId = [];
          }
        },
        err => {
        }
    );
  }

  getLookupServices() {
    this.countryList = [];
    this.regionResult = [];
    this.settingsSector = {
      singleSelection: false,
      text: "Select Sectors",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Sectors',
      enableSearchFilter: true,
      badgeShowLimit: 5,
    };
    this.settings = {
      singleSelection: false,
      text: "Select Countries",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Regions and Countries',
      enableSearchFilter: true,
      badgeShowLimit: 5,
      groupBy: "category"
    };
    this.aptService.getCaseNames().subscribe(
        res => {
          const result: any = res;
          this.caseName = [];
          if (result && result.length > 0) {
            this.groupData = result;
            if (result && result.length > 0) {
              result.map(item => {
                if (item.caseName && item.caseName !== undefined) {
                  this.caseName.push(item.caseName);
                  return item.caseName;
                }
              });
            }
          } else {
            this.caseName = [];
          }
        },
        err => {
        }
    );
    this.aptService.getSectors().subscribe(
        res => {
          const result: any = res;
          this.selectedItemsS = [];
          this.sectorResult = [];
          if (result && result.length > 0) {
            for (let sec of result) {
              const data = {
                id: sec.id,
                itemName: sec.value,
              };
              this.sectorResult.push(data);

            }

          } else {
            this.sectorResult = [];
          }
        },
        err => {
        }
    );

    this.aptService.getRegions().subscribe(
        res => {
          const result: any = res;
          this.selectedItems = [];
          if (result && result.length > 0) {
            for (let region of result) {
              const data = {
                id: region.id,
                itemName: region.countryName,
                category: region.region
              };
              this.selectedItems.push(data);

            }
            this.regionResult = this.selectedItems;
            this.countryList = this.selectedItems;

          } else {
            this.regionResult = [];
            this.countryList = [];
          }
        },
        err => {
        }
    );

  }
  searchCasename(event): void {
    this.caseNameResult = this.caseName.filter(groupFormObj => groupFormObj.toLowerCase().startsWith(event.query.toLowerCase()));
  }

  onCancel() {
    this.router.navigate(['apt/groupList']);
  }
}
