const webpack = require("webpack");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

module.exports = {
  output: {
    publicPath: "https://cai-stage.cydea.tech/",
    uniqueName: "apt",
  },
  optimization: {
    runtimeChunk: false,
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "apt",
      library: { type: "var", name: "apt" },
      filename: "remoteEntry.js",
      exposes: {
        AptModule: "./src/app/Advanced Persistent Threats/apt.module",
      },
      shared: {
        "@angular/core": { singleton: true, requiredVersion:'12.0.1'  },
        "@angular/common": { singleton: true, requiredVersion:'12.0.1'  },
        "@angular/router": { singleton: true, requiredVersion:'12.0.1'  },
      },
    }),
  ],
};
